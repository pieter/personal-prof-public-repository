# Personal Prof

An automatic code review tool for student Java assignments. See also the
accompanying paper
https://gitlab.science.ru.nl/objectorientatie/personal-prof-paper

Personal Prof consists of two parts: The rule checker itself and the
Brightspace daemon. The rule checker is called **Personal Prof**, the
Brightspace integration is called **pp-bs-daemon**. The rule checker is written
in Rascal, which runs on the Java Virtual Machine (tested with openjdk 11,
newer probably also works). The Brightspace daemon is written in Python 3.

Running Personal Prof directly is only useful for development and debugging.
For checking assignments you always want pp-bs-daemon.



## Running Personal Prof

To run Personal Prof, you need a rascal jar and the Personal Prof source code.
Both are included in this repository. The rascal jar can be found in `src/lib`
and the source code in `src`. To run Personal Prof, you have to call java as
follows.

```
$ cd src
$ java -Xmx1G -Xss32m -jar lib/rascal-0.15.2-SNAPSHOT.jar Main.rsc <ruleset-name> <absolute-path-to-project-under-test>
```

- Rascal documentation recommends running java with `-Xmx1G -Xss32m`
- You have to run it from inside the directory where `Main.rsc` is. This is due
  to the way Rascal module loading works.
- Rascal needs absolute paths to open files. The path to the project under test
  must be absolute
- To see which rulesets are available, look at the big switch statement in
  `Main.rsc`

There is a script `run.sh` which simplifies running Personal Prof. This script
can handle relative paths to the project under test.

```
$ ./run.sh <ruleset-name> <path-to-student-project>
```

For example

```
$ ./run.sh hangman ~/tmp/oo-grading-2020/assignment02-hangman/grading/TA_Name/128863\ -\ 34172\ -\ AsgnA\ 142\ -\ Student\ Name\ -\ 2020-02-16T09_23_57.733Z/submission/
Gallows should use StringBuilder
at |java+class:///hangman/Gallows|
```


# Running The Brightspace Daemon

pp-bs-daemon is a Python 3 program that periodically polls Brightspace for new
student submissions. When a new student submission is detected, the daemon
downloads it, runs Personal Prof, and uploads the resulting feedback to
Brightspace.

For all this to work, the daemon needs two dependencies: Personal Prof and
no-bs. no-bs is a Python library and command line program to interact with
Brightspace. Everything you need is included in this repository.

no-bs is a component that can run independently of the daemon. You have to set
up no-bs first, before you can run the daemon. no-bs has its own configuration
file, and you have to use no-bs to log in to Brightspace. This needs to be done
only once. Logging in with no-bs stores a long-term authentication cookie on
your computer.

The basic steps to set up no-bs are described as follows. For more
information about no-bs see
https://gitlab.science.ru.nl/objectorientatie/brightspace-grading-tool

## To run the pp-bs-daemon, follow these steps.

1. Clone repository

```
$ git clone --recurse-submodules https://gitlab.science.ru.nl/objectorientatie/personal-prof.git
$ cd personal-prof
```

3. Optional: Run tests

```
$ ./test.sh
```

4. Create config directories for no-bs and pp-bs-daemon

```
$ mkdir -p ~/.config/grading-tools
$ mkdir -p ~/.config/personal-prof
```

5. Copy config files to config direcotries

```
$ cd brightspace-integration
$ cp app.json.config ~/.config/personal-prof
$ cp brightspace-grading-tool/grading-tool-python/app.json.config.default ~/.config/grading-tools/app.json.config
```

6. Edit config files. For no-bs you have to set `lms_url` and `courses`.

```
$ vim ~/.config/personal-prof/app.json.config
```

For pp-bs-daemon you have to set `rascalJar` and `personalProfInstallPath`

```
$ vim ~/.config/grading-tools/app.json.config
```

7. Install d2lvalence python module. This command will install it into your
   local python package store.

```
$ pip3 install D2LValence
```

8. Log in with no-bs

```
$ cd brightspace-grading-tool/grading-tool-python/
$ ./no-bs login-manual
```

9. See if it works

```
$ ./no-bs list-courses
$ ./no-bs list-folders <course-id>
```

10. Start the daemon

```
$ export PYTHONPATH=`readlink -f brightspace-grading-tool/grading-tool-python`
$ ./personal-prof run <course-name> <folder-id> <target-directory> <ruleset-name>
```

11. Keep the daemon running

It is recommended to run the daemon on a server, not your personal laptop. All
the above setup procedure needs to be repeated on the server. We always used
lilo, but an AWS instance or something similar should also work (not tested).
To keep the daemon running when you are logged out, use `screen`.


```
$ ssh <science-login>@lilo.science.ru.nl
$ cd path/to/personal-prof/brightspace-integration
$ export PYTHONPATH=`readlink -f brightspace-grading-tool/grading-tool-python`
$ screen
```

From inside screen, open a new tab per pp-bs-daemon instance you need. In each
week we typically need four instances. Two for the current assignment, two for
the resit. That's because of the stupid Brightspace restriction that a group
category can only have 200 groups.

screen quick-reference:

- `Ctrl-a c` create new tab
- `Ctrl-a n` switch to next tab
- `Ctrl-a p` switch to previous tab
- `Ctrl-a d` detach screen to backgrond. You can log out now.
- `$ screen -r` re-attach to a running screen

This is an example session for how to run pp-bs-daemon for the course
oo1920 for assignment 03 groups A and B, and resit 01 groups A and B.

```
$ ssh <science-login>@lilo.science.ru.nl
$ cd path/to/personal-prof/brightspace-integration
$ export PYTHONPATH=`readlink -f brightspace-grading-tool/grading-tool-python`
$ screen
$ ./pp-bs-daemon help
$ ./pp-bs-daemon list-folders oo1920
33813 Assignment 01 A
33814 Assignment 01 B
33913 Assignment 02 A
33914 Assignment 02 B
34013 Assignment 03 A
34014 Assignment 03 B
34015 Resit 01 A
34016 Resit 01 B
$ ./pp-bs-daemon oo1920 34013 ./pp-work/assignment03a geometric
Ctrl-a c # open new screen tab
$ ./pp-bs-daemon oo1920 34014 ./pp-work/assignment03b geometric
Ctrl-a c # open new screen tab
$ ./pp-bs-daemon oo1920 34015 ./pp-work/resit01a student
Ctrl-a c # open new screen tab
$ ./pp-bs-daemon oo1920 34016 ./pp-work/resit01b student
Ctrl-a d # detach screen
```

Both no-bs and pp-bs-daemon work incrementally. You can stop `Ctrl-c` and
re-start pp-bs-daemon by using the exact same command again. It will continue
where it left off by looking at the work directory.



# Contributing

To develop rascal code, use eclipse-rcp. Follow the instructions here:
https://www.rascal-mpl.org/start/


## Running the test cases

There are some test projects and test cases to check that the rules give
correct error messages. To run the tests, use the test script. The test script
looks for all files `*Spec.rsc` and executes them

```
$ ./test.sh
```

To test only specific files, specify them on the command line.

```
$ ./test.sh Assignment02RulesSpec.rsc Assignment03RulesSpec.rsc
```




# References

- Rascal official website https://www.rascal-mpl.org/
- Rascal github https://github.com/usethesource/rascal
- Rascal documentation http://tutor.rascal-mpl.org/Rascal/Rascal.html
