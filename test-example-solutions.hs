-- run this script with
--   $ stack runghc test-example-solutions.hs

import System.IO.Error
import System.Process
import Control.Exception

-- Run coding rules on all start projects
-- Run coding rules + assignment rules on all example solutions

-- Rascal needs absolute paths
baseDir = "/home/mkl/radboud/teaching/object-orientation/NWI-IPI005-Object-Orientatie/OO-opgaven/"

startProjects =
  [ "assignment01-student/assignment01-student-start"
  , "assignment02-hangman/assignment02-hangman-start"
  , "assignment03-geometric-shapes/assignment03-geometric-start"
  , "assignment04-quiz/assignment04-quiz-start"
  , "assignment05-expressions/assignment05-expressions-start"
  , "assignment06-sliding-game/assignment06-sliding-game-start"
  , "assignment07-quadtrees/assignment07-quadtrees-start"
  , "assignment08-polynomials/assignment08-polynomials-start"
  , "assignment09-javafx-pie-charts/assignment09-javafx-pie-charts-start"
  , "assignment09-javafx-pie-charts/assignment09-javafx-pie-charts-start"
  , "assignment10-snake/assignment10-snake-start"
  , "assignment11-visitor-pattern/assignment11-visitor-start"
  , "assignment12-streams/assignment12-streams-start"
  , "assignment13-file-finder-merge-sort/assignment13-finder-sort-start"
  , "assignment14-taxi/assignment14-taxi-start"
  , "assignment15-supermarket/assignment15-supermarket-start"
  ]

exampleSolutions =
  [ ("student", "assignment01-student/assignment01-student-example-solution")
  , ("hangman", "assignment02-hangman/assignment02-hangman-example-solution")
  , ("geometric", "assignment03-geometric-shapes/assignment03-geometric-example-solution")
  , ("quiz", "assignment04-quiz/assignment04-quiz-example-solution")
  , ("expressions", "assignment05-expressions/assignment05-expressions-example-solution")
  , ("sliding-game", "assignment06-sliding-game/assignment06-sliding-game-example-solution")
  , ("quadtrees", "assignment07-quadtrees/assignment07-quadtrees-example-solution")
  , ("polynomials", "assignment08-polynomials/assignment08-polynomials-example-solution")
  , ("pie-charts", "assignment09-javafx-pie-charts/assignment09-javafx-pie-charts-simple-example-solution")
  , ("pie-charts", "assignment09-javafx-pie-charts/assignment09-javafx-pie-charts-extra-example-solution")
  , ("snake", "assignment10-snake/assignment10-snake-example-solution")
  , ("propositional", "assignment11-visitor-pattern/assignment11-visitor-example-solution")
  , ("streams", "assignment12-streams/assignment12-streams-example-solution")
  , ("file-finder-merge-sort", "assignment13-file-finder-merge-sort/assignment13-finder-sort-example-solution")
  , ("taxi", "assignment14-taxi/assignment14-taxi-example-solution")
  , ("supermarket", "assignment15-supermarket/assignment15-supermarket-example-solution")
  ]

runPersonalProf :: String -> FilePath -> IO ()
runPersonalProf ruleset project = do
  putStrLn project
  catch
    (callProcess "./run.sh" [ruleset, baseDir++project])
    -- This also handles Ctrl-C, which I don't want, but callProcess is very
    -- unspecific about what exception it throws.
    (\ex -> do
      print (ex :: IOError)
      return ()
    )
  putStrLn "-----"
  
main = do
  sequence_ [ runPersonalProf "coding" project | project <- startProjects ]
  sequence_ [ runPersonalProf ruleset project | (ruleset,project) <- exampleSolutions ]
