#!/bin/bash

# $1 is a two-digit number of the assignment to check
# $2 is the directory of the student submission to check


## BEGIN configuration; adapt as needed

# Download latest rascal shell jar from https://update.rascal-mpl.org/console/
#RASCAL_JAR=$(readlink -f lib/rascal-shell-unstable.jar)
RASCAL_JAR=$(readlink -f src/lib/rascal-0.15.2-SNAPSHOT.jar)

## END configuration; don't touch anything else

JAVA_ARGS="-Xmx1G -Xss32m"

# make directory name absolute
INPUT_DIR=$(readlink -f "$2")

if [ $# -lt 2 ]
then
  echo "usage: run.sh <ruleset> <submissionDir>"
  exit 1
fi

if [ ! -e "$INPUT_DIR" ]
then
  echo "input directory does not exist: $2"
  exit 1
fi

cd src
java $JAVA_ARGS -jar $RASCAL_JAR Main.rsc $1 "$INPUT_DIR" 2>/dev/null
