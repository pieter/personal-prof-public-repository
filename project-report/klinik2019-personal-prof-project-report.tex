\documentclass[a4paper]{article}
% The following \documentclass options may be useful:

% preprint      Remove this option only once the paper is in final form.
% 10pt          To set in 10-point type instead of 9-point.
% 11pt          To set in 11-point type instead of 9-point.
% authoryear    To obtain author/year citation style instead of numeric.

% the option a4paper of documentclass article doesn't always work
\usepackage[a4paper]{geometry}

\usepackage{mathptmx}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{enumitem}
\usepackage{xspace}
\usepackage{color}

\usepackage{hyperref}

\input{javalist.tex}

% for generating timestamps in draft versions of the paper
\usepackage[yyyymmdd,24hr]{datetime}
\renewcommand{\dateseparator}{-}


%BEGIN user-defined commands ===========================

\newcommand{\todo}[1]{\textcolor{red}{\emph{Todo: #1}}}

%END user-defined commands ===========================


% version.tex needs to define the command \version
\IfFileExists{version.tex}
  {\input{version.tex}}
  {\newcommand{\version}{unknown}}

\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}
\fancyfoot[L]{v.\version\ \today\ \currenttime}


\title{Personal Prof: Automatic Early Feedback for Java Assignments}
\author{
       Markus Klinik
  \and Pieter Koopman
}

\begin{document}

\maketitle
\begin{abstract}
Evaluating students' Java assignments involves many criteria, some of which can be checked automatically.
In this document we study which criteria can be checked automatically, to what extent, and how assignment texts should be changed to facilitate automation.
\end{abstract}
\thispagestyle{fancy}





\section{Problem Statement}

Evaluating assignments is about the interaction between \emph{students} and \emph{graders}.
Our regular workflow for giving feedback for assignments is as follows.
Students hand in their submissions before an assignment's deadline.
Graders check the submissions and prepare feedback.
The feedback is then sent to the students.

When students get the feedback, they are already busy with working on the next assignment.
It would benefit students to get feedback as early as possible, maybe even before they submit the assignment.
This feedback doesn't need to be very sophisticated.
Some simple rules that guide students towards the intended solution would already help a lot.

The goal of this article is not to fully automate grading.
The final assessment must still be made by a human looking at the solution.
However, the work of graders would be reduced if all submissions undergo basic sanity checks beforehand.

We would like to have a system that takes a student submission as input, evaluates it according to a set of criteria, and returns a list of human readable improvement suggestions.



\subsection{Automatic Feedback}

To facilitate automatic feedback, we sometimes need to change our existing exercise texts.
For example, the assignment text should always specify names of classes, so that the feedback system can find and analyze them.
Some of our assignment texts currently do not specify class names.

Furthermore, if we want to provide test cases so that students themselves can check correctness of their implementation, we might have to modify the assignments so that test cases are possible.
For example, some of our assignments currently ask users to implement an interactive application with reads and writes, for which it is very hard to provide unit tests.
These assignments could be changed either to require an interface that the test cases can use, or to specify the input and output format of the user interface such that it can be automatically tested.






\section{Criteria}

In this section we study the criteria we would like to automatically check.
We start by summarizing all relevant criteria, and then isolate the ones suitable for automation.
The basis for this chapter are two documents, the programming guidelines, developed over several years for our Java programming course, and the grading manual, developed summer 18/19.
The grading manual was written by teachers for teaching assistants to help them grade assignments in a uniform way.

Every evaluation criteria falls into one of the following categories.
\begin{itemize}[noitemsep]
\item Code style
\item Correctness
\item Software architecture
\end{itemize}

\paragraph{Code style}
Does the source code adhere to the recommended code style?
Code style is purely syntactical.
This includes indentation, placement of parentheses, and naming conventions.

\paragraph{Correctness}
Does the program compute the correct results?

\paragraph{Software architecture}
Does the source code adhere to the required software architecture?
There are general criteria for Java software architecture, for example that attributes should be private.
Other criteria are specific to an assignment, for example that the model and the view must be separated.

The rest of this section lists all rules and criteria from the programming guidelines and the grading manual.
Every criteria gets a unique identifier.
These are all the rules we would like the students to observe.
Which rules can be checked automatically remains to be seen.



\subsection{Programming Style}

All programming style rules are implemented in CodingRules.rsc

\subsubsection{Naming}

\begin{description}

\item[ps-class-names-uppercase]
Class names always start with an uppercase letter.

\item[ps-lowercase-attribute-names]
Names of attributes and methods start with a lowercase letter.

\item[ps-all-caps-constant-names]
Constant attribute names are written with only capital letters.
If there would have been whitespace, use an underscore, for example \prog{MAX_SIZE} or \prog{PI_SQUARED}.

\end{description}




\subsubsection{Using Java}

\begin{description}

\item[ps-public-attributes]
Never make attributes \prog{public}. Only \prog{final} values may be \prog{public}.

\item[ps-explicit-access-modifiers]
Always explicitly use an access modifier for you attributes and methods: \prog{public}, \prog{private}, or \prog{protected}.

\end{description}



\subsection{Grading Manual}


We have run each set of static checks on at least 10 random student submissions from the course of 2018, and adjusted the checked criteria so that there are no or very few false positives.



\subsubsection{Criteria Assignment 1 - Student}

\paragraph{Architecture}
These criteria are implemented as rules in Assignment01Rules.rsc
\begin{description}[noitemsep]
\item [a01-student] There should be a class \prog{Student} for holding student data.
\item [a01-group] There should be a class \prog{Group} for managing the student group.
\item [a01-io] The main class can have code for I/O.
  Separate classes for views and controllers will be part of assignment 2.
  No other class should have I/O.
\end{description}

\paragraph{Correctness}
These criteria are made available to students as test cases in the project template.
We do not want to give away the classes and methods in the test cases.
The test cases come with a class Assignment01Tester, which acts as a model for the application.
The students have to implement the methods, which should be a one-liner for each method.
The tester acts as an automated user interface for testing, in place of the interactive user interface.
\begin{description}[noitemsep]
\item [a01-add-student] It should be possible to create a group and add students.
\item [a01-rename-student] Changing the name of a student should work.
\end{description}




\subsubsection{Criteria Assignment 2 - Hangman}

\paragraph{Architecture}
These criteria are implemented as rules in Assignment02Rules.rsc
Performing I/O is defined as using PrintStream.
\begin{description}[noitemsep]
\item [a02-all-io-in-view] All I/O should happen in the view.
  The main function should not have I/O.
  The model should also not have I/O.
\item [a02-use-stringbuilder] StringBuilder should be used for the guessed word.
\end{description}

\paragraph{Correctness}
There are test cases for students in the project template.
Because the game is stateful, there is a class HangmanTester which the students have to fill in.
Students have to implement each method with a one-liner that just forwards the call to the student's code.
We do this to not give away the interface of the application in the test cases.
\begin{description}[noitemsep]
\item [a02-game] The game should work as specified.
\end{description}




\subsubsection{Criteria Assignment 3 - Geometric Shapes}

\paragraph{Architecture}
These criteria are implemented as rules in Assignment03Rules.rsc
\begin{description}[noitemsep]
\item [a03-geometric-is-comparable] Interface Geometric should extend Comparable.
\item [a03-shapes-implement-comparable] Circle and Rectangle should implement \prog{Comparable.compareTo}.
\item [a03-separation-of-concerns] The parser, the model, and the command interpreter should be separated cleanly.
\end{description}

\paragraph{Correctness}
There are test cases for students in the project template.
There is a class Assignment03Tester that encapsulates the statefulness of the program.
Students have to implement the methods of the tester with one-liners that just forward the method calls to the student's code.
\begin{description}[noitemsep]
\item [a03-sorting-correctness] Creating, moving, and sorting should work correctly.
\end{description}




\subsubsection{Criteria Assignment 4 - Quiz}

\paragraph{Architecture}
These criteria are implemented as rules in Assignment04Rules.rsc
\begin{description}[noitemsep]
\item [a04-question-exists] There should be an abstract class Question.
\item [a04-question-types] There should be three classes OpenQuestion, MultipleChoiceQuestion, and ThisThatQuestion that extend Question.
\item [a04-implement-abstract-methods] Every question class should implement the three functions toString, isCorrect and correctAnswer.
\end{description}

\paragraph{Correctness}
This project requires students to write a stateful, interactive game, where new questions can be added, and correct answers are recorded.
We do not provide a tester class like in the assignments before, because this behaviour is not easily testable.
To test the application's interaction logic, the students would have to abstract away the user interface so that the tests can swap it out with a non-interactive one.
An abstraction of the user interface is too advanced for this stage of the course.
Instead, we just do some unit tests on the test classes directly.

This has the downside that test cases don't compile until students have implemented the question classes.
\begin{description}[noitemsep]
\item [a04-getters-setters] The getters and setters of the different question classes should behave according to the specification.
\item [a04-setscore-validate-argument] Question.setScore should validate its parameter.
\end{description}





\subsubsection{Criteria Assignment 5 - Expressions}

\paragraph{Architecture}
These criteria are implemented as rules in Assignment05Rules.rsc
\begin{description}[noitemsep]
\item [a05-base-classes-exist] The following classes should be abstract: Expression, NoArgExpr, OneArgExpr, TwoArgExpr
\item [a05-concrete-classes-exist] The following classes should be concrete: Constant, Variable, Negate, Add, Multiply.
\end{description}

\paragraph{Correctness}
There are test cases performing various tests about the abstract methods of Expression.
The project template includes the classes Expression, and ExpressionFactory.
This allows us to provide compiling but failing test cases.
\begin{description}[noitemsep]
\item [a05-implement-methods] The concrete classes should implement toString, eval, and partialEval as specified in the assignment text.
\end{description}





\subsubsection{Criteria Assignment 6 - Sliding Game}

\paragraph{Architecture}
These criteria are implemented as rules in Assignment06Rules.rsc
\begin{description}[noitemsep]
\item [a06-breadth-solver-uses-collection] The solver should use the Collection interface for the observed states, Queue for the to examine states, and HashSet.
\item [a06-bestfit-solver-uses-priorityqueue] The best-first solver should use Collection, Queue, HashSet, and PriorityQueue.
\end{description}

\paragraph{Correctness}
There are test cases in the start project that test these criteria.
The test cases use the Collection and SlidingGame interface that is given to the students in the project start template.
\begin{description}[noitemsep]
\item [a06-identify-solutions] SlidingGame.isSolution should correctly identify solved configurations
\item [a06-traverse-parents] Configuration.pathFromRoot should return the list configurations from the start configuration to the solution.
\item [a06-calculate-manhattan-distance] The Manhattan distance of a configuration should be calculated according to the formula, for all tiles.
\item [a06-use-manhattan-distance] Configuration.compareTo should use the Manhattan distance to determine if a configuration is better.
\item [a06-equals] Configuration.equals should be implemented correctly.
\item [a06-hashCode] Configuration.hashCode should be implemented correctly.
\end{description}





\subsubsection{Criteria Assignment 7 - Quad Trees}

\paragraph{Architecture}
\begin{description}[noitemsep]
\item [a06-class-hierarchy] There should be an interface QuadTreeNode, and three implementations: grey, white and black nodes
\item [a06-main-uses-qtree] The main function should use QTree, not the nodes directly
\end{description}

\paragraph{Correctness}
\begin{description}[noitemsep]
\item [a06-example-serialization] The serialization given in the assignment and in the template project should result in the example bitmap.
\item [a06-qtree-exists] There should be a class QTree that manages quad trees.
  This class is given in the project template, its methods should be implemented correctly.
\end{description}





\subsubsection{Criteria Assignment 8 - Polynomials}

\paragraph{Architecture}
These checks are implemented as rules in Assignment08Rules.rsc
\begin{description}[noitemsep]
\item [a08-no-additional-classes] The classes Polynomial and Term are given in the project template.
  No additional classes should be implemented.
\item [a08-use-iterators] The methods that traverse the term list should use ListIterators.
  They should not use indices, and not use enhanced for loops.
  The methods are: toString, equals, plus, times.
\item [a08-test-cases] There should be a minimal number of test cases.
  A test case is a method annotated with \jav{@Test}.
\end{description}

\paragraph{Correctness}
The existence of the different test cases must be checked by hand by the teaching assistants.
\begin{description}[noitemsep]
\item [a08-test-each-function] Each of the following methods should have at least one regular test case.
  toString, plus, minus, times, evaluate.
\item [a08-test-vanishing-terms] There should be a test case that tests for vanishing terms in plus.
\item [a08-test-subtraction-is-negative-addition] There should be a test case for $a - b = a + (-1 \times b)$.
\item [a08-test-square] There should be a test case for multiplying a polynomial with itself.
\item [a08-test-commut-assoc-distrib] There should be at least five test cases testing the mathematical properties of plus and times.
  \begin{itemize}[noitemsep]
  \item 2 test cases: Commutativity of plus and times.
  \item 2 test cases: Associativity of plus and times.
  \item 1 test case: Distributivity of times over plus.
  \end{itemize}
\end{description}





\subsubsection{Criteria Assignment 9 - JavaFX Pie Chart Generator}

The students have to do this exercise in a week where there are no lab sessions.
The exercise is not very difficult.
The biggest difficulty is probably getting JavaFX running.
Therefore, the evaluation criteria are very simple.

There are no test cases, because this assignment is about a GUI program.

JavaFX is an external dependency, I don't know how to make Rascal aware of it.
For now there are no criteria that can be checked automatically.






\subsubsection{Criteria Assignment 10 - JavaFX Snake}

There are no test cases or criteria for this assignment.




\subsubsection{Criteria Assignment 11 - Visitor Pattern}

\paragraph{Architecture}
These criteria are implemented as checks in Assignment11Rules.
\begin{description}[noitemsep]
\item [a11-ast-hierarchy]
  There should be an interface Formula.
  There should be classes implementing this interface for all the syntax elements: Constant, Atomic, Not, BinaryOperator.
\item [a11-visitor-hierarchy]
  There should be an interface FormulaVisitor for the visitors.
  There should be two visitors: a PrintVisitor and an EvaluateVisitor.
\item [a11-binop-uses-strategy] There should only be one class BinaryOperator, which is parameterized with a strategy BinOp to print and evaluate them.
\end{description}

\paragraph{Correctness}
There are many test cases provided in the project start template.
The test cases use a class FormulaFactory, which is provided with empty methods.
The test cases do not use the formula and visitor classes directly.
\begin{description}[noitemsep]
\item [a11-test-cases] All given test cases should pass.
\end{description}






\subsubsection{Criteria Assignment 12 - Streams}

In this assignment the students have to write stream pipelines to solve small puzzles.
The puzzles are given as failing test cases.

\begin{description}[noitemsep]
\item [a12-no-loops] No loops should be used at all.
  This includes foreach-, for-, and while loops.
\item [a12-use-streams] All test cases should be implemented with stream pipelines.
  Some use Stream, some use IntStream.
\item [a12-correctness] All test cases should pass
\end{description}






\subsubsection{Criteria Assignment 13 - File Finder and Merge Sort}

There are rules implemented in Assignment13Rules that check these criteria.

There are no test cases for this assignment.
FileFinder can not be tested, because it does not join threads.
It just creates new threads which print the file name if found.
There is no way to check the result without joining threads.

For ParallelMergeSort we provide a main function that does a performance comparison between the parallel and the non-parallel sort.

\paragraph{File Finder}
\begin{description}[noitemsep]
\item [a13-ff-use-runnable] FileFinder should implement Runnable.
\item [a13-ff-run-creates-threads] Instead of the recursive call to find, a new thread should be started.
\item [a13-ff-not-call-join] FileFinder should not join threads.
\end{description}

\paragraph{Merge Sort}
\begin{description}[noitemsep]
\item [a13-parallel-merge-creates-threads] There should be an additional class ParallelMergeSort.
  \begin{itemize}[noitemsep]
  \item It should then create a new thread to sort one of the smaller arrays.
  \item It should then join with the other thread.
  \end{itemize}
\item [a13-parallel-merge-creates-only-one-thread] It should then create \emph{one} new thread to sort one of the smaller arrays, and sort the other one \emph{itself}.
\end{description}






\subsubsection{Criteria Assignment 14 - Train and Taxi}

This assignment is about concurrency, which is always a nightmare to get right and to check.
We do not expect you to actually check that every solution is correct, because that is nearly impossible.
If the students get the following points right, there is a high chance that their solution works.

\paragraph{Architecture}
These criteria are implemented in Assignment14Rules.
\begin{description}[noitemsep]
\item [a14-use-runnable] Train and Taxi should implement Runnable.
\item [a14-simulation-exists] There should be a class Simulation that creates the train and taxis and starts them using an Executor.
\item [a14-synchronization-exists] There should be exactly one lock and two Conditions in the whole program.
\item [a14-synchronization-only-in-station] This lock and the conditions should be in the class Station.
  No other class should have any locks or synchronization.
\item [a14-no-synchronized] Only the lock may be used for synchronization.
  There should be no synchronized blocks or methods.
\end{description}

\paragraph{Correctness}
There is one test case that just runs the simulation and checks the following simple consistency property.
\begin{description}
\item [a14-no-last-passengers] At the end of the simulation, the number of passengers arrived by train and the number of passengers departed by taxi should be the same.
\end{description}





\subsubsection{Criteria Assignment 15 - Supermarket}

Most of the code is given in the template project.
The students have to fill in the synchronization code.
As the architecture is given, only correctness criteria are of interest.

\paragraph{Correctness}
\begin{description}[noitemsep]
\item [a15-synchronization-exists] The classes ConveyorBelt and Register should use Lock and Condition.
\item [a15-synchronization-only-in-converyorbelt-register] Only the classes ConveyorBelt and Register should have synchronization code.
\item [a15-no-synchronized] Only Locks may be used for synchronization.
  There should be no synchronized blocks or methods.
\item [a15-sync-register] The class Register should a Lock for synchronization.
\end{description}




\end{document}
