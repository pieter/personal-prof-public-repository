import unittest
import shutil
from pathlib import Path

import personal_prof

testDataOutDir = 'test-data-out'
testDataDir = 'test-data'

class TestPP(unittest.TestCase):
    def test_delete_junk(self):
        shutil.rmtree(testDataOutDir, ignore_errors=True)
        shutil.copytree(testDataDir, testDataOutDir)

        macOsDirs = [d for d in Path(testDataOutDir).rglob('__MACOSX')]
        javaFiles = [d for d in Path(testDataOutDir).rglob('*.java')]
        self.assertNotEqual(macOsDirs, [])
        self.assertEqual(len(javaFiles), 6)

        personal_prof.delete_junk(testDataOutDir)

        macOsDirs = [d for d in Path(testDataOutDir).rglob('__MACOSX')]
        javaFiles = [d for d in Path(testDataOutDir).rglob('*.java')]
        self.assertEqual(macOsDirs, [])
        self.assertEqual(len(javaFiles), 5)

if __name__ == '__main__':
    unittest.main()
