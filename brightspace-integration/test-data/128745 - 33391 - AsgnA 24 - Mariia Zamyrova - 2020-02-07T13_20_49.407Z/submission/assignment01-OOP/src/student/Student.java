package student;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author me myself and I
 */
public class Student {
    private String givenName;
    private String familyName;
    private int studentNumber;
    
    public Student(String given, String family,int number){
        this.givenName = given;
        this.familyName = family;
        this.studentNumber = number;
    }
    
    public String toString(Student student){
        return ""+student.givenName + " " +student.familyName + ", s"+ student.studentNumber;
    }
    
    public void setName(String givenName, String familyName){
        this.givenName = givenName;
        this.familyName = familyName;
    }
    
    public int getStudentNumber(){
        return this.studentNumber;
    }
}
