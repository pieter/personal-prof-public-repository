package student;

import java.util.Scanner;
/**
 * 
 * @author mariia zamyrova s1038789 and jonathan bauermeister s1037112
 */
public class Main {

    public static void main(String[] args) {
    	Scanner scanner = new Scanner(System.in);
        System.out.println("please Enter the size of the group you want to create:");
        int size = scanner.nextInt();
        Group studentGroup = new Group(size);
        addStudent(studentGroup, scanner);
        printGroup(studentGroup);
        changeNames(scanner, studentGroup);
        
    }
    
    private static void changeNames(Scanner scanner, Group studentGroup){
        Boolean stop = false;
        String newGivenName;
        String newFamilyName;
        while(!stop){
            System.out.println("student number and new given and family Name?");
            int s = scanner.nextInt();
            if (s < 0){
               
                System.out.println("Bye!");
                stop = true;
            }
            else{
            newGivenName = scanner.next();
            newFamilyName = scanner.next();

            
            System.out.println(studentGroup.changingName(newGivenName, newFamilyName, s));
            printGroup(studentGroup);
            }
        }
    }
    private static void addStudent(Group studentGroup, Scanner scanner){
        String givenName;
        String familyName;
        int studentNumber;
        for(int i = 0; i < studentGroup.getSize(); i++){
            System.out.println("please enter a student:");
            studentNumber = scanner.nextInt();
            givenName = scanner.next();
            familyName = scanner.next();
            Student student = new Student(givenName, familyName, studentNumber);
            studentGroup.fillGroup(student, i);
            }
    }
    private static void printGroup(Group studentGroup){
        System.out.println("The group now contains: ");
        for(int i = 0; i < studentGroup.getSize(); i++){
            System.out.println(studentGroup.printGroup(i));
        }
    }
}
