package student;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jonathan
 */
public class Group {
    private Student[] group;
    private int size;
    
    
    public Group(int size){
        this.group = new Student[size];
        this.size = size;
    }
    
    public void fillGroup(Student student, int i){
        this.group[i] = student;
    }
    
    public String printGroup(int i){
        
         return group[i].toString(group[i]);
        
    }
    
    public int getSize(){
        return size;
    }
    
    public String changingName(String given, String family,int number){
        for(int i = 0; i < this.size; i++){
           if(group[i].getStudentNumber()==number){
               group[i].setName(given, family);
               return "you changed the names succesfully!";
           }
           
        }
        return "The student Id could not be found.";
    }
}
