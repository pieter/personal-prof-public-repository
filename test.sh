#!/bin/bash

# Rascal currently has no test runner. This script implements a test runner by
# piping commands into the interactive Rascal shell.

# To run all tests, invoke the script without arguments
#    ./test.sh
# To run an individual test, run like this:
#    ./test.sh test/Assignment01RulesSpec.rsc

# make filename absolute
#RASCAL_JAR=$(readlink -f rascal-shell-unstable.jar)
RASCAL_JAR=$(readlink -f src/lib/rascal-0.15.2-SNAPSHOT.jar)

# all sources and tests must be in the same directory, because the Rascal shell
# does not support include paths.
cd src

# if no command line arguments are given, execute all *Spec.rsc
# if command line arguments are given, test only those files
if [ $# -eq 0 ]
then
  SPEC_FILES=test/*Spec.rsc
else
  SPEC_FILES=$@
fi

# generate import statements for all files named *Spec.rsc
( ls $SPEC_FILES |\
    # replace direcotry separators by module separators
    sed 's#/#::#' |\
    # escape keyword "test"
    sed 's/test/\\test/' |\
    # generate import statements
    sed 's/\(.*\).rsc/import \1;/'
  echo ":test"
) |\
# pipe the import statements to the rascal shell
  java -jar $RASCAL_JAR
