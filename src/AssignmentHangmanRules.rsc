module AssignmentHangmanRules

import Set;
import Message;
import lang::java::m3::Core;

import ClassHierarchy;
import Util;
import StandardLocations;

set[Message] allAssignmentHangmanRules(M3 model)
  = aHangman_all_io_in_view(model)
  + aHangman_use_stringbuilder(model)
  ;

// There should be one class that is not the main class and not the Gallows
// class, and this class is the only one that uses System.io, which is a PrintStream
set[Message] aHangman_all_io_in_view(M3 model)
{
  set[Message] result = {};
  try
  {
    loc mainClass = getMainClass(model);
    loc gallows = findClass(model, "Gallows");
    loc wordReader = findClass(model, "WordReader");

    dirtyClasses = ioClasses(model);

    // Generate error messages if Main or Gallows performs I/O
    result += { error("All I/O should happen in the view class", c) | c <- { mainClass, gallows }, c in dirtyClasses };

    // If there is more than one class that performs I/O, generate error messages
    set[loc] ioClasses2 = dirtyClasses - { mainClass, gallows, wordReader };
    if( size(ioClasses2) > 1 )
    {
      result += { error("Only the view class should perform I/O", c) | c <- ioClasses2 };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return result;
}

// The gallows class should use StringBuilder
set[Message] aHangman_use_stringbuilder(M3 model)
{
  try
  {
    loc gallows = findClass(model, "Gallows");
    set[loc] gallowsMethods = methods(model, gallows);
    set[loc] gallowsDependencies = model.typeDependency[gallowsMethods] + model.typeDependency[gallows];
    if( !(javaLangStringBuilder in gallowsDependencies) )
    {
      return { error("Gallows should use StringBuilder", gallows) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }
  return {};
}
