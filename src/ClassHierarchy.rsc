module ClassHierarchy

import lang::java::m3::Core;
import Message;
import Set;
import Relation;

import Util;

// The functions in this module are all about direct relationships.
// We don't care about transitive relationships.

// We assume that names are unique, which means for example that the same class should not exist
// in different packages. For student projects this assumption is reasonable.


loc findInterface(M3 model, str interfaceName)
{
  candidateInterfaces = { i | i <- interfaces(model), /^<interfaceName>$/ := getName(model, i) };
  if(size(candidateInterfaces) > 1)
  {
    throw error("There should be only one interface called <interfaceName>", |file:///|);
  }
  if(size(candidateInterfaces) < 1)
  {
    throw error("There should be an interface called <interfaceName>", |file:///|);
  }

  return getOneFrom(candidateInterfaces);
}

loc findClass(M3 model, str className)
{
  candidates = { i | i <- classes(model), /^<className>$/ := getName(model, i) };
  if(size(candidates) > 1)
  {
    throw error("There should be only one class called <className>", |file:///|);
  }
  if(size(candidates) < 1)
  {
    throw error("There should be a class called <className>", |file:///|);
  }

  return getOneFrom(candidates);
}

loc findEnum(M3 model, str className)
{
  candidates = { i | i <- enums(model), /^<className>$/ := getName(model, i) };
  if(size(candidates) > 1)
  {
    throw error("There should be only one enum called <className>", |file:///|);
  }
  if(size(candidates) < 1)
  {
    throw error("There should be an enum called <className>", |file:///|);
  }

  return getOneFrom(candidates);
}

// get all methods with the given name
set[loc] findMethods(M3 model, str methodName)
{
  candidates = { i | i <- methods(model), /^<methodName>$/ := getName(model, i) };
  return candidates;
}

loc findMethod(M3 model, str className, str methodName)
{
  c = findClass(model, className);
  candidates = { m | m <- methods(model, c), /^<methodName>$/ := getName(model, m) };

  if(size(candidates) > 1)
  {
    throw error("There should be only one method <className>.<methodName>", |file:///|);
  }
  if(size(candidates) < 1)
  {
    throw error("There should be a method <className>.<methodName>", |file:///|);
  }

  return getOneFrom(candidates);
}

// Wrap one of the find fuctions in an exception handler
set[Message] assertExists(loc(M3,str) findFunction, M3 model, str name)
{
  try
  {
    findFunction(model, name);
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

// Checks that a given interface extends another interface
set[Message] extendsInterface(M3 model, str interfaceNameA, str interfaceNameB)
{
  try
  {
    interfaceA = findInterface(model, interfaceNameA);
    interfaceB = findInterface(model, interfaceNameB);
    if( ! (interfaceB in model.extends[interfaceA]) )
    {
      return { error("<interfaceNameA> should extend <interfaceNameB>", interfaceA) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

// Checks that a given class extends another class
set[Message] extendsClass(M3 model, str classNameA, str classNameB)
{
  try
  {
    loc classA = findClass(model, classNameA);
    loc classB = findClass(model, classNameB);
    if( ! (classB in model.extends[classA]) )
    {
      return { error("<classNameA> should extend <classNameB>", classA) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

// Checks that a given class implements an interface
set[Message] implementsInterface(M3 model, str className, str interfaceName)
  = implementsInterfaceGeneric
      ( model
      , loc (M3 model){return findClass(model,className);}
      , loc (M3 model){return findInterface(model,interfaceName);}
      );

// Checks that a given thing implements an interface. The thing is given by the findFunction, which can either be findClass or findEnum
set[Message] implementsInterfaceGeneric(M3 model, loc (M3) findFunction, loc (M3) interfaceFindFunction)
{
  try
  {
    // class and interface seem to be keywords or otherwise already defined. Rascal error messages are not very helpful.
    loc classs = findFunction(model);
    loc interfacee = interfaceFindFunction(model);
    if( ! (interfacee in model.implements[classs]) )
    {
      return { error("<classs> should implement <interfacee>", classs) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}


// Given a class location c, return all types that c depends on
set[loc] allTypeDependenciesLoc(M3 model, loc dependerClass) =
  ({} | it + model.typeDependency[thing] | thing <- (model.containment+)[dependerClass]);

// Given a class name c, return all types that c depends on
set[loc] allTypeDependencies(M3 model, str depender)
{
  loc dependerClass = findClass(model, depender);
  return allTypeDependenciesLoc(model, dependerClass);
}

// Assert that class depender must depend on dependency
set[Message] classDependsOn(M3 model, str depender, loc dependency)
{
  try
  {
    loc dependerClass = findClass(model, depender);
    set[loc] allDependencies = allTypeDependenciesLoc(model, dependerClass);
    if( !(dependency in allDependencies) )
    {
      return { error("<depender> should use <dependency>", dependerClass) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }
  return {};
}

// Assert that method of class depends on dependency
set[Message] methodDependsOn(M3 model, str dependerClassName, str dependerMethodName, loc dependency)
{
  try
  {
    loc dependerMethod = findMethod(model, dependerClassName, dependerMethodName);
    if( !(dependency in model.typeDependency[dependerMethod]) )
    {
      return { error("<dependerMethodName> should use <dependency>", dependerMethod) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }
  return {};
}



// Given an M3 model, a class and an abstract function, determine whether the
// class implements the function.
bool implementsAbstractMethod(M3 model, loc class, loc function)
{

  // transitive closure of extends and implements
  // TODO: should this be (model.extends*) + (model.implements*) ?
  // see https://stackoverflow.com/questions/57525714/m3-java-how-to-check-that-a-class-implements-a-function-from-an-interface
  rel[loc,loc] super = (model.extends + model.implements)*;

  // All methods that the class overrides
  // TODO: this can be achieved easier using domainR
  rel[loc,loc] allOverrides = ident(union({methods(model,s) | s <- super[class]})) o model.methodOverrides;

  // All methods that override the function
  functionOverrides = invert(allOverrides)[function];

  return size(functionOverrides) > 0;
}
