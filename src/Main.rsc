module Main

import IO;
import lang::java::m3::Core;
import lang::java::m3::AST;
import Message;
import List;
import Set;

import Util;
import Configuration;
import CodingRules;
import AssignmentStudentRules;
import AssignmentHangmanRules;
import AssignmentGeometricRules;
import AssignmentQuizRules;
import AssignmentExpressionsRules;
import AssignmentSlidingGameRules;
import AssignmentQuadtreesRules;
import AssignmentPolynomialsRules;
import AssignmentSnakeRules;
import AssignmentPropositionalRules;
import AssignmentStreamsRules;
import AssignmentFileFinderMergeSortRules;
import AssignmentTaxiRules;
import AssignmentSupermarketRules;

// args[0] should be the two-digit assignment number like 01 or 14
// args[1] should be the absolute path to the project directory
int main(list[str] args)
{
  if( size(args) != 2 )
  {
    printUsage();
    return 1;
  }

  if( args[1] == "" )
  {
    println("empty input directory");
    return 1;
  }

  str assignmentNr = args[0];
  loc projectDir = |file:///| + args[1];
  
  M3 m = createM3FromDirectory(projectDir, javaVersion=defaultJavaVersion, classPath=defaultClassPath);

  if( !isEmpty(modelErrors(m)) )
  {
    for( msg <- modelErrors(m) )
    {
      prettyPrintMessage(msg);
      println("");
    }
    println("The project contains errors. Not running rule checks.");
    return 1;
  }

  set[Message] messages = allCodingRules(m);

  switch(assignmentNr)
  {
    case "student": messages += allAssignmentStudentRules(m);
    case "hangman": messages += allAssignmentHangmanRules(m);
    case "geometric": messages += allAssignmentGeometricRules(m);
    case "quiz": messages += allAssignmentQuizRules(m);
    case "expressions": messages += allAssignmentExpressionsRules(m);
    case "sliding-game": messages += allAssignmentSlidingGameRules(m);
    case "quadtrees": messages += allAssignmentQuadtreesRules(m);
    case "polynomials": messages += allAssignmentPolynomialsRules(m);
    // pie-charts doesn't have rules, but we still want coding rules and syntax errors
    case "pie-charts": ;
    case "snake": messages += allAssignmentSnakeRules(m);
    case "propositional": messages += allAssignmentPropositionalRules(m);
    case "streams":
    {
      set[Declaration] ast = createAstsFromDirectory(projectDir, true, javaVersion=defaultJavaVersion);
      messages += allAssignmentStreamsRules(m, ast);
    }
    case "file-finder-merge-sort":
    {
      set[Declaration] ast = createAstsFromDirectory(projectDir, true, javaVersion=defaultJavaVersion);
      messages += allAssignmentFileFinderMergeSortRules(m, ast);
    }
    case "taxi":
    {
      set[Declaration] ast = createAstsFromDirectory(projectDir, true, javaVersion=defaultJavaVersion);
      messages += allAssignmentTaxiRules(m, ast);
    }
    case "supermarket":
    {
      set[Declaration] ast = createAstsFromDirectory(projectDir, true, javaVersion=defaultJavaVersion);
      messages += allAssignmentSupermarketRules(m, ast);
    }
    case "coding": messages += {}; // only the coding rules
    default: println("Rules for assignment " + assignmentNr + " not implemented");
  }

  for( msg <- messages )
  {
    prettyPrintMessage(msg);
    println("");
  }

  if( isEmpty(messages) )
  {
    println("Your solution looks good to me. Let\'s see what the TA has to say.");
  }

  return 0;
}

void printUsage()
{
  println("usage: 2 arguments required");
  println("First argument must be the ruleset name, like student or taxi");
  println("Second argument must be the absolute path to the project directory.");
}

void prettyPrintMessage(Message msg)
{
  switch(msg)
  {
    case error(m, at):
    {
      println(m);
      println("at <at>");
    }
    case error(m):
    {
      println(m);
    }
    case warning(m, at):
    {
      println(m);
      println("at <at>");
    }
    case info(m, at):
    {
      println(m);
      println("at <at>");
    }
  }

  return;
}
