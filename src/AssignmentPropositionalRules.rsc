module AssignmentPropositionalRules

import Message;
import lang::java::m3::Core;

import ClassHierarchy;

set[Message] allAssignmentPropositionalRules(M3 model)
  = aPropositional_ast_hierarchy(model)
  + aPropositional_visitor_hierarchy(model)
  + aPropositional_binop_uses_strategy(model)
  ;

set[Message] aPropositional_ast_hierarchy(M3 model)
  = implementsInterfaceGeneric
      ( model
      , loc (M3 model){return findEnum(model, "Constant");}
      , loc (M3 model){return findInterface(model, "Formula");}
      )
  + implementsInterface(model, "Atom", "Formula")
  + implementsInterface(model, "Not", "Formula")
  + implementsInterface(model, "BinaryOperator", "Formula")
  ;

set[Message] aPropositional_visitor_hierarchy(M3 model)
  = implementsInterface(model, "PrintVisitor", "FormulaVisitor")
  + implementsInterface(model, "EvaluateVisitor", "FormulaVisitor")
  ;

set[Message] aPropositional_binop_uses_strategy(M3 model)
{
  try
  {
    set[Message] result = {};
    binOp = findEnum(model, "BinOp");
    binaryOperator = findClass(model, "BinaryOperator");
    if( !(binOp in model.typeDependency[binaryOperator]) )
    {
      result += error("class BinaryOperator should use enum BinOp as strategy", binaryOperator);
    }
    return result;
  }
  catch e:error(_,_):
  {
    return { e };
  }
}