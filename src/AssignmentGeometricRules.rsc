module AssignmentGeometricRules

import Relation;
import lang::java::m3::Core;
import Message;

import ImplementsAbstractMethod;
import ClassHierarchy;
import Util;
import StandardLocations;

// Runs all checkers for assignment 3 on the given model
set[Message] allAssignmentGeometricRules(M3 model)
  = aGeometric_geometric_is_comparable(model)
  + aGeometric_shapes_implement_comparable(model)
  + aGeometric_separation_of_concerns(model)
  ;

set[Message] aGeometric_geometric_is_comparable(M3 model)
{
  set[Message] result = {};
  try
  {
    geometric = findInterface(model, "Geometric");
    if( ! (javaLangComparable in model.extends[geometric] ) )
    {
      result += { error("Geometric should extend Comparable", geometric) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return result;
}


set[Message] aGeometric_shapes_implement_comparable(M3 model)
{
  set[Message] result = {};
  try
  {
    geometric = findInterface(model, "Geometric");

    // There should be exactly two subclasses of Geometric
    shapes = invert(model.implements)[geometric];
    if( {"Rectangle", "Circle"} != { getName(model, shape) | shape <- shapes } )
    {
      return { error("There should be two implementations of Geometric: Rectangle and Circle", geometric) };
    }

    // Rectangle and Circle should only implement Geometric and nothing else
    result +=
      { error("<getName(model,shape)> should implement Geometric and nothing else", shape)
      | shape <- shapes
      , model.implements[shape] != { geometric }
      };

    // Rectangle and Circle should implement Comparable.compareTo

    // for every shape: one of the methods has to override compareTo
    for( shape <- shapes )
    {
      if( !implementsAbstractMethod(model,shape,compareTo) )
      {
        result += error("<shape.file> should override Comparable.compareTo", shape);
      }
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return result;
}



// Neither Circle nor Rectangle should perform I/O
set[Message] aGeometric_separation_of_concerns(M3 model)
{
  set[Message] result = {};

  try
  {
    circle = findClass(model, "Circle");
    rectangle = findClass(model, "Rectangle");
    dirtyClasses = ioClasses(model);
    result +=
      { error("<getName(model,shape)> should not perform I/O", shape)
      | shape <- {circle, rectangle}
      , shape in dirtyClasses
      };
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return result;
}
