module AssignmentFileFinderMergeSortRules

import Message;
import Set;
import Relation;
import lang::java::m3::Core;
import lang::java::m3::AST;

import ClassHierarchy;
import Util;
import StandardLocations;

set[Message] allAssignmentFileFinderMergeSortRules(M3 model, set[Declaration] ast)
  = aFileFinderMergeSort_ff_use_runnable(model)
  + aFileFinderMergeSort_ff_creates_thread(model)
  + aFileFinderMergeSort_ff_not_call_join(model)
  + aFileFinderMergeSort_parallel_merge_creates_threads(model)
  + aFileFinderMergeSort_parallel_merge_creates_only_one_thread(model, ast)
  ;

set[Message] aFileFinderMergeSort_ff_use_runnable(M3 model) =
  // FileFinder must implement Runnable
  implementsInterfaceGeneric
    ( model
    , loc (M3 model){return findClass(model, "FileFinder");}
    , loc (M3 _){return javaLangRunnable;}
    );

set[Message] aFileFinderMergeSort_ff_creates_thread(model)
{
  try
  {
    loc fileFinder = findClass(model, "FileFinder");
    if( !(javaLangThread in allTypeDependenciesLoc(model, fileFinder)) )
    {
      return { error("FileFinder should start new threads", fileFinder) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

set[Message] aFileFinderMergeSort_ff_not_call_join(M3 model)
{
  try
  {
    loc ff = findClass(model, "FileFinder");
    set[loc] allMethods =
      { m
      | m <- (model.containment+)[ff]
      , isMethod(m)
      };
    set[loc] allMethodFunctionCalls =
      union(
        { model.methodInvocation[m]
        | m <- allMethods
        }
      );
    if( threadJoin in allMethodFunctionCalls )
    {
      return { error("FileFinder should not call Thread.join", ff) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

set[Message] aFileFinderMergeSort_parallel_merge_creates_threads(M3 model)
{
  try
  {
    loc pms = findClass(model, "ParallelMergeSort");
    // The type dependencies of all methods transitively contained somewhere in
    // ParallelMergeSort. Works for methods of the class itself, but also for
    // methods of inner classes.
    set[loc] allMethods =
      { m
      | m <- (model.containment+)[pms]
      , isMethod(m)
      };
    set[loc] allMethodsDependencies  =
      union(
        { model.typeDependency[m]
        | m <- allMethods
        }
      );
    set[loc] allMethodFunctionCalls =
      union(
        { model.methodInvocation[m]
        | m <- allMethods
        }
      );

    // one of the methods has to create new Threads
    if( !(javaLangThread in allMethodsDependencies ) )
    {
      return { error("ParallelMergeSort should start new threads", pms) };
    }

    // one of the methods has to join Threads
    if( !(threadJoin in allMethodFunctionCalls ) )
    {
      return { error("ParallelMergeSort should call Thread.join", pms) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

set[Message] aFileFinderMergeSort_parallel_merge_creates_only_one_thread(M3 model, set[Declaration] ast)
{
  try
  {
    int count = 0;
    // Find all method calls to Thread::start somewhere inside ParallelMergeSort
    for( /class("ParallelMergeSort",_,_,/m:methodCall(_,_,_,_)) <- ast )
    {
      if( m.decl == threadStart )
      {
        count+=1;
      }
    }
    if( count > 1 )
    {
        return { error("ParallelMergeSort should start only one new thread", findClass(model, "ParallelMergeSort")) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}