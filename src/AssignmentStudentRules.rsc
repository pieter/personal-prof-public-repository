module AssignmentStudentRules

import Set;
import Message;
import Relation;
import lang::java::m3::Core;

import ClassHierarchy;
import Util;

set[Message] allAssignmentStudentRules(M3 model)
  = aStudent_student(model)
  + aStudent_group(model)
  + aStudent_io(model)
  ;

set[Message] aStudent_student(M3 model) = assertExists(findClass, model, "Student");

set[Message] aStudent_group(M3 model) = assertExists(findClass, model, "Group");

// TODO: we have a helper function for this now
set[Message] aStudent_io(M3 model)
{
  try
  {
    // we assume there is only one main method, which lets us identify the main class
    loc mainClass = getMainClass(model);

    // find all methods that use System
    set[loc] ioMethods =
      { m
      | m <- invert(model.typeDependency)[|java+class:///java/lang/System|]
      , isMethod(m)
      };
    // find the classes to which these methods belong
    set[loc] ioClasses = range(domainR(invert(model.containment), ioMethods));

    set[str] ioClassNames = { getName(model, c) | c <- ioClasses };

    // Some heuristics. Many students implemented a view class, which doesn't have
    // a main function. In this case we don't want to generate an error message.
    // Therefore, the rules:
    //   - If more than one class has I/O: generate messages for both of them
    //   - If only one class has I/O: check that it is NOT Student or Group.

    // More than one class has I/O
    if( size(ioClasses) > 1 )
    {
      return
        { error("Only the main or view class should contain I/O", c)
        | c <- ioClasses, c != mainClass
        };
    }
    // Only one class has I/O
    else if( "Student" in ioClassNames || "Group" in ioClassNames )
    {
      return
        { error("Only the main or view class should contain I/O", c)
        | c <- ioClasses
        };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}