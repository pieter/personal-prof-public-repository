module AssignmentPolynomialsRules

import Message;
import Set;
import Relation;
import lang::java::m3::Core;

import ClassHierarchy;
import Util;
import StandardLocations;

set[Message] allAssignmentPolynomialsRules(M3 model)
  = aPolynomials_no_additional_classes(model)
  + aPolynomials_use_iterators(model)
  + aPolynomials_test_cases(model)
  ;
  
set[Message] aPolynomials_no_additional_classes(M3 model)
{
  set[str] allowedClasses = {"Polynomial", "Main", "PolynomialTest", "Term" };
  return
    { error("You should not write additional classes in this assignment", c)
    | c <- classes(model)
    , ! (getName(model, c) in allowedClasses)
    };
}
  
set[Message] aPolynomials_use_iterators(M3 model)
{
  set[Message] errors = {};

  try
  {
    errors += classDependsOn(model, "Polynomial", javaUtilListIterator);

    set[str] methodsOfInterest = { "plus", "minus", "times" };
    set[loc] forbiddenMethods = { javaUtilListSize, javaUtilListGet };
    loc polynomial = findClass(model, "Polynomial");
    errors +=
      { error("In this assignment you are not allowed to use <invokedMethod>", m)
      | m <- methods(model, polynomial)
      , getName(model, m) in methodsOfInterest
      , invokedMethod <- model.methodInvocation[m]
      , invokedMethod in forbiddenMethods
      };
    return errors;
  }
  catch e:error(_,_):
  {
    return { e };
  }
}

set[Message] aPolynomials_test_cases(M3 model)
{
  int expectedNumTestCases = 11;
  int numTestCases = size(rangeR(model.annotations, {orgJunitTest}));
  if( numTestCases < expectedNumTestCases )
  {
    return { error("Please write at least <expectedNumTestCases> test cases, as described in the assignment text", |file:///|) };
  }
  return {};
}
