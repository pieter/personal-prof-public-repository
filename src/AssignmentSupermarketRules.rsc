module AssignmentSupermarketRules

import Message;
import Relation;
import lang::java::m3::AST;
import lang::java::m3::Core;

import Util;
import StandardLocations;
import ClassHierarchy;

set[Message] allAssignmentSupermarketRules(M3 model, set[Declaration] ast)
  = aSupermarket_no_synchronized_modifier(model)
  + aSupermarket_no_synchronized_block(ast)
  + aSupermarket_only_register_belt_synchronization(model)
  + aSupermarket_synchronization_exists(model)
  ;

set[Message] aSupermarket_no_synchronized_modifier(M3 model) =
  { error("synchronized should not be used in this assignment", synchronizedThing)
  | loc synchronizedThing <- invert(model.modifiers)[synchronized()]
  };

set[Message] aSupermarket_no_synchronized_block(set[Declaration] ast) =
  { error("synchronized should not be used in this assignment", m.decl)
  | /m:method(_,_,_,_,/synchronizedStatement(_,_)) <- ast
  };

// All classes except Register or ConveyorBelt, that depend on Lock
set[Message] aSupermarket_only_register_belt_synchronization(M3 model) =
  { error("class <getName(model,c)> should not have synchronization code", c)
  | c <- classes(model) - { findClass(model, "Register"), findClass(model, "ConveyorBelt") }
  ,    javaUtilLock      in allTypeDependenciesLoc(model, c)
    || javaUtilCondition in allTypeDependenciesLoc(model, c)
  };

set[Message] aSupermarket_synchronization_exists(M3 model)
  = classDependsOn(model, "ConveyorBelt", javaUtilLock)
  + classDependsOn(model, "ConveyorBelt", javaUtilCondition)
  + classDependsOn(model, "Register", javaUtilLock)
  ;