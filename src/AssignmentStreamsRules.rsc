module AssignmentStreamsRules

import Message;
import Set;
import Relation;
import lang::java::m3::Core;
import lang::java::m3::AST;

import ClassHierarchy;
import Util;
import StandardLocations;

// Note! This function takes an AST instead of a model!
set[Message] allAssignmentStreamsRules(M3 model, set[Declaration] ast)
  = aStreams_no_loops(ast)
  + aStreams_use_streams(model)
  ;

set[Message] aStreams_no_loops(set[Declaration] ast)
{
  set[Message] errors = {};
  visit(ast)
  {
    case loop:\foreach(_, _, _):
    {
      errors += error("No loops are allowed in this assignment", loop.src);
    }
    case loop:\for(_, _, _, _):
    {
      errors += error("No loops are allowed in this assignment", loop.src);
    }
    case loop:\for(_, _, _):
    {
      errors += error("No loops are allowed in this assignment", loop.src);
    }
    case loop:\while(_, _):
    {
      errors += error("No loops are allowed in this assignment", loop.src);
    }
  }
  return errors;
}

set[Message] aStreams_use_streams(M3 model)
  = methodDependsOn(model, "StreamsTest", "countEvenNumbers",   javaUtilStream)
  + methodDependsOn(model, "StreamsTest", "sumOddNumbers",      javaUtilStream)
  + methodDependsOn(model, "StreamsTest", "multiplyNumbers",    javaUtilStream)
  + methodDependsOn(model, "StreamsTest", "calculateTax",       javaUtilStream)
  + methodDependsOn(model, "StreamsTest", "sumStringIntegers",  javaUtilStream)
  + methodDependsOn(model, "StreamsTest", "streamOfStreams",    javaUtilStream)
  + methodDependsOn(model, "StreamsTest", "everySecondElement", javaUtilIntStream)
  + methodDependsOn(model, "StreamsTest", "philosophers",       javaUtilStream)
  ;