module AssignmentQuizRules

import Message;
import lang::java::m3::Core;
import lang::java::m3::AST;

import ClassHierarchy;
import Util;
import StandardLocations;

set[Message] allAssignmentQuizRules(M3 model)
  = aQuiz_question_exists(model)
  + aQuiz_question_types(model)
  + aQuiz_implement_abstract_methods(model)
  ;

// A class Question should exist, and it should be an abstract class.
set[Message] aQuiz_question_exists(M3 model)
{
  try
  {
    loc questionClass = findClass(model, "Question");
    if( ! (abstract() in model.modifiers[questionClass]) )
    {
      throw error("Class Question should be abstract.", questionClass);
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return {};
}

set[Message] aQuiz_question_types(M3 model)
{
  try
  {
    return
      extendsClass(model, "OpenQuestion", "Question") +
      extendsClass(model, "MultipleChoiceQuestion", "Question") +
      extendsClass(model, "ThisThatQuestion", "MultipleChoiceQuestion");
  }
  catch e:error(_,_):
  {
    return { e };
  }
}


// Each of the classes OpenQuestion, MultipleChoiceQuestion and ThisThatQuestion
// must implement the three functions toString, isCorrect, correctAnswer
set[Message] aQuiz_implement_abstract_methods(M3 model)
{
  set[Message] result = {};
  try
  {
    set[loc] questions =
      { findClass(model, "OpenQuestion")
      , findClass(model, "MultipleChoiceQuestion")
      , findClass(model, "ThisThatQuestion")
      };

    loc correctAnswer = findMethod(model, "Question", "correctAnswer");
    loc isCorrect = findMethod(model, "Question", "isCorrect");

    // every question must override the three functions correctAnswer, isCorrect, toString
    result +=
      { error("<getName(model,questionClass)> should implement <method>", questionClass)
      | questionClass <- questions
      , method <- { javaLangToString, correctAnswer, isCorrect }
      , !(method in model.methodOverrides[methods(model, questionClass)])
      };

  }
  catch e:error(_,_):
  {
    return { e };
  }
  return result;
}
