module AssignmentQuadtreesRules

import Message;
import lang::java::m3::Core;

import ClassHierarchy;
import Util;

set[Message] allAssignmentQuadtreesRules(M3 model)
  = aQuadtrees_class_hierarchy(model)
  + aQuadtrees_main_uses_qtree(model)
  ;
  
set[Message] aQuadtrees_class_hierarchy(M3 model)
  = implementsInterface(model, "GreyNode", "QuadTreeNode")
  + implementsInterface(model, "WhiteLeaf", "QuadTreeNode")
  + implementsInterface(model, "BlackLeaf", "QuadTreeNode")
  ;

// Main class should not depend on any of the node classes, nor the tree interface.
// It should only use QTree
set[Message] aQuadtrees_main_uses_qtree(M3 model)
{
  try
  {
    loc main = getMainClass(model);
    set[loc] treeClasses =
      { findInterface(model, "QuadTreeNode")
      , findClass(model, "GreyNode")
      , findClass(model, "WhiteLeaf")
      , findClass(model, "BlackLeaf")
      };
    return
      { error("<getName(model, main)> should not use <getName(model, treeClass)> directly", main)
      | treeClass <- treeClasses
      , treeClass in allTypeDependenciesLoc(model, main)
      };
  }
  catch e:error(_,_):
  {
    return { e };
  }
}