package oo.assignment4;
import java.util.*;

public class IO {

    Scanner scan = new Scanner(System.in);

    public IO() {

    }

    public void start() {
    	System.out.println("Let's start the quiz! \n");
    }
    
    public void end() {
    	System.out.println("Thanks for playing!");
    }
    
    public void printQuestion(Question question){
        System.out.println(question);
    }
    
    public void printCorrect(){
        System.out.println("Correct answer! \n");
    }
    
     public void printIncorrect(){
        System.out.println("Incorrect answer. \n");
    }
    
    public String askAnswer() {
        String answer = "";

        System.out.print("Answer: ");
        answer = scan.nextLine();

        return answer;
    }
    
    public void printScore1(int totalScore) {
    	System.out.println("End of first round, your score is: " + totalScore + ". \n");
    }
    
    public void printScore2(int totalScore) {
    	System.out.println("Your final score is: " + totalScore + ". \n");
    }
    

    public void invalid() {
        System.out.println("This is not a valid action.");
    }
}
