package oo.assignment4;

public class OOAssignment4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logic logic = new Logic();
        logic.startQuiz();
        logic.runQuiz();
        
        new IO().end();
    }
    
}
