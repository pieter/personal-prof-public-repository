
package oo.assignment4;

public class MultipleChoiceQuestion extends Question{
    private String[] answers;
    private String correctAnswer;
    String alphabet = "abcdefghijklmnopqrstuvwxyz";
	
    public MultipleChoiceQuestion(String question, String[] answers, int correctAnswer, int score){
    	super(question, score);
        this.answers = answers;
        this.correctAnswer = alphabet.substring(correctAnswer-1, correctAnswer);
    }
    
    public MultipleChoiceQuestion(String question, String[] answers, int correctAnswer){
    	super(question, 3);
        this.answers = answers;
        this.correctAnswer = alphabet.substring(correctAnswer-1, correctAnswer);
    }
    
    @Override
    public String correctAnswer() {
    	return correctAnswer;
    }
    
    @Override
        public String toString(){
            String text = question;
            int index = 0;
            
            
            for (String s : answers){
                text += "\n" + alphabet.charAt(index)+ ". " + s;
                index++;
            }
            return text;
        }
    
}
 