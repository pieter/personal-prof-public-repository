
package oo.assignment4;

public class Question {
    protected String question;
    protected int score;
    
    public String correctAnswer() { return null; }
    public String toString() { return null; }
    
    public Question(String question, int score) {
    	this.question = question;
        
        if (score < 1 || score > 5)
            this.score = 3;
        else 
            this.score = score;
    }


     public boolean isCorrect (String answer){
         return correctAnswer().equalsIgnoreCase(answer);
         
     }

     public Question duplicate(){
         return this;
     }
}
