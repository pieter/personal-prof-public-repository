/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package filefindermergesort;

import java.util.Arrays;

/**
 *
 * @author Wouter Damen (s1028002)
 */
public class MergeSortMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      long time1, time2;
      final int l = 20000000;
      int [] a1 = new int[l];
      int [] a2 = new int[l];
      for (int i = 0; i < l; i += 1) {
        a1[i] = a2[i] = (int) (Math.random() * l);
      }
      System.out.print("sequential sort: ");
      time1 = System.currentTimeMillis();
      MergeSort.sort(a1);
      time2 = System.currentTimeMillis();
      System.out.println((time2 - time1) + " millis. Array is sorted = " + MergeSort.isSorted(a1));
      
      System.out.print("parrallel sort (ParallelMergeSort): ");
      time1 = System.currentTimeMillis();
      ParallelMergeSort.sort(a2);
      time2 = System.currentTimeMillis();
      System.out.println((time2 - time1) + " millis. Array is sorted = " + MergeSort.isSorted(a2));
      System.out.println("sorter arrays equals = " + Arrays.equals(a1, a2));
      System.out.println("The number of processors is " + Runtime.getRuntime().availableProcessors());
      
      /*
        On my 4 core CPU, Intel Core i5-4460 @ 3.20 GHz, the sequential version always took between 3900 and 4100 ms.
        By correctly choosing the threshold at which to create new threads, I was able to achieve a 2 times faster result using the parallel method.
        At a threshold of 50000 (so at least 25000 per new thread) sorting took between 1700 and 1900 ms.
        Note that I set the L variable in the main class to 20 million instead of 10 million, for more accurate measurements.
      */
    }
}
