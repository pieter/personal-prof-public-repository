package filefindermergesort;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author Wouter Damen (s1028002)
 */
public class FileFinder implements Runnable {
    private final File rootDir;
    private boolean isFound = false;
    
    public FileFinder(String root) throws IOException {
        rootDir = new File(root);
        if (!(rootDir.exists() && rootDir.isDirectory())) {
            throw new IOException(root + " is not a directory");
        }
    }
    
    public void findFile(String file) {
        find(rootDir, file);
    }
    
    private void find (File rootDir, final String fileName) {
        File [] files = rootDir.listFiles();
        if (files != null) {
            for (File file: files) {
                if (file.getName().equals(fileName)) {
                    System.out.println("Found at: " + file.getAbsolutePath());
                    isFound = true;
                    return;
                } else if (file.isDirectory()) {
                    if(isFound) { //Stop creating new threads if we found the file.
                        return;
                    } else {
                        Thread t = new Thread(() -> {
                            find(file, fileName);
                        });
                        t.start();
                    }
                }
            }
        }
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}
