package ast;

import java.util.function.BinaryOperator;

public enum BinOp implements BinaryOperator<Boolean> {
	AND(2, "/\\") {
		public Boolean apply(Boolean a1, Boolean a2) {
			return a1 && a2;
		}
	},
	OR(3, "\\/") {
		public Boolean apply(Boolean a1, Boolean a2) {
			return a1 || a2;
		}
	},
	IMPLIES(4, "=>") {
		public Boolean apply(Boolean a1, Boolean a2) {
			return !a1 || a2;
		}
	};

	public static final int MAX_PRIO = 5;
	private int prio;
	private String string;

	BinOp(int prio, String string) {
		this.prio = prio;
		this.string = string;
	}

	public String toString() {
		return string;
	}

	public int getPriority() {
		return prio;
	}
}
