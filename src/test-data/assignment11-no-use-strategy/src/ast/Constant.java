package ast;

import visitor.FormulaVisitor;

public enum Constant implements Formula {
	True(true), False(false);

	private boolean value;

	Constant(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	@Override
	public <R, A> R accept(FormulaVisitor<R, A> v, A arg) {
		return v.visit(this, arg);
	}
}
