package ast;

import visitor.FormulaVisitor;

public class Atom implements Formula {
	private final String id;

	public Atom(String name) {
		this.id = name;
	}

	public String getId() {
		return id;
	}

	public <R, A> R accept(FormulaVisitor<R, A> v, A arg) {
		return v.visit(this, arg);
	}
}
