package ast;

import visitor.FormulaVisitor;

public class BinaryOperator implements Formula {
	private String op;
	private Formula leftOperand;
	private Formula rightOperand;

	public BinaryOperator(Formula left, Formula right, String op) {
		this.leftOperand = left;
		this.rightOperand = right;
		this.op = op;
	}

	public Formula getLeft() {
		return leftOperand;
	}

	public Formula getRight() {
		return rightOperand;
	}

	public String getOp() {
		return op;
	}

	public <R, A> R accept(FormulaVisitor<R, A> v, A arg) {
		return v.visit(this, arg);
	}
}
