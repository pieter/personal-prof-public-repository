package visitor;

import ast.BinaryOperator;
import ast.Atom;
import ast.BinOp;
import ast.Constant;
import ast.Not;
import java.util.Map;

public class EvaluateVisitor implements FormulaVisitor<Boolean, Void> {
	private Map<String, Boolean> environment;

	public EvaluateVisitor(Map<String, Boolean> environ) {
		this.environment = environ;
	}

	public Boolean visit(BinaryOperator form, Void arg) {
		switch (form.getOp()) {
		case "AND":
			return BinOp.AND.apply(form.getLeft().accept(this, arg), form.getRight().accept(this, arg));
		case "OR":
			return BinOp.OR.apply(form.getLeft().accept(this, arg), form.getRight().accept(this, arg));
		case "IMPLIES":
			return BinOp.IMPLIES.apply(form.getLeft().accept(this, arg), form.getRight().accept(this, arg));
		}
		throw (new IllegalArgumentException());
	}

	public Boolean visit(Not form, Void arg) {
		return !form.getOperand().accept(this, arg);
	}

	public Boolean visit(Atom form, Void arg) {
		return environment.get(form.getId());
	}

	public Boolean visit(Constant form, Void arg) {
		return form.getValue();
	}

}
