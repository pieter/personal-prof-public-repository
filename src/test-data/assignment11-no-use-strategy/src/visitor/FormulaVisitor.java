package visitor;

import ast.BinaryOperator;
import ast.Atom;
import ast.Not;
import ast.Constant;

public interface FormulaVisitor<Result, AdditionalArg> {
	Result visit(BinaryOperator form, AdditionalArg arg);

	Result visit(Not form, AdditionalArg arg);

	Result visit(Atom form, AdditionalArg arg);

	Result visit(Constant form, AdditionalArg arg);
}
