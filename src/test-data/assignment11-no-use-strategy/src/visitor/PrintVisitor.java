package visitor;

import ast.BinaryOperator;
import ast.Atom;
import ast.BinOp;
import ast.Constant;
import ast.Not;

public class PrintVisitor implements FormulaVisitor<Void, Integer> {

	private StringBuilder result = new StringBuilder();

	public String getResult() {
		return result.toString();
	}

	public Void visit(BinaryOperator form, Integer parentPrio) {
		BinOp op = null;

		switch (form.getOp()) {
		case "AND":
			op = BinOp.AND;
			break;
		case "OR":
			op = BinOp.OR;
			break;
		case "IMPLIES":
			op = BinOp.IMPLIES;
			break;
		default:
			throw new IllegalArgumentException();
		}

		int myPrio = op.getPriority();
		if (parentPrio <= myPrio) {
			result.append("(");
		}
		form.getLeft().accept(this, myPrio);
		result.append(op.toString());
		form.getRight().accept(this, myPrio);
		if (parentPrio <= myPrio) {
			result.append(")");
		}
		return null;
	}

	public Void visit(Not form, Integer parentPrio) {
		result.append("!");
		form.getOperand().accept(this, 1);
		return null;
	}

	public Void visit(Atom form, Integer parentPrio) {
		result.append(form.getId());
		return null;
	}

	public Void visit(Constant form, Integer parentPrio) {
		result.append(form.toString());
		return null;
	}

}
