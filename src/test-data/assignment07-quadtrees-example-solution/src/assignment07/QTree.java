package assignment07;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class QTree {

	private QuadTreeNode root;

	public QTree(Reader input) {
		root = readQTree(input);
	}

	public QTree(Bitmap bitmap) {
		root = bitmap2QTree(0, 0, bitmap.getWidth(), bitmap);
	}

	public void fillBitmap(Bitmap bitmap) {
		root.fillBitmap(0, 0, bitmap.getWidth(), bitmap);
	}

	public void writeQTree(Writer sb) {
		root.writeNode(sb);
	}

	private static QuadTreeNode readQTree(Reader input) {
		try {
			int next = input.read();
			if ((char) next == '1') {
				QuadTreeNode[] children = new QuadTreeNode[4];
				for (int i = 0; i < 4; i++) {
					children[i] = readQTree(input);
				}
				return new GreyNode(children);
			} else {
				next = input.read();
				return ((char) next == '0' ? new BlackLeaf() : new WhiteLeaf());

			}
		} catch (IOException ex) {
			System.err.println(ex);
		}
		return null;
	}

	public static QuadTreeNode bitmap2QTree(int x, int y, int width, Bitmap bitmap) {
		if (width > 1) {
			width /= 2;
			QuadTreeNode[] children = new QuadTreeNode[4];
			children[0] = bitmap2QTree(x, y, width, bitmap);
			children[1] = bitmap2QTree(x + width, y, width, bitmap);
			children[2] = bitmap2QTree(x + width, y + width, width, bitmap);
			children[3] = bitmap2QTree(x, y + width, width, bitmap);
			for (int i = 1; i < 4; i++) {
				if (!children[0].sameLeaf(children[i])) {
					return new GreyNode(children);
				}
			}
			return children[0];
		} else {
			return (bitmap.getBit(x, y) ? new WhiteLeaf() : new BlackLeaf());
		}
	}

}
