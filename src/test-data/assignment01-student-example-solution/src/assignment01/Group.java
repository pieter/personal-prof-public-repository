package assignment01;

/**
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Group {

    private Student[] allStudents;
    private int numStudents;

    public Group(int amountOfStudents) {
        allStudents = new Student[amountOfStudents];
        numStudents = 0;
    }

    public void addToGroup(Student student, int place) {
        allStudents[place] = student;
        numStudents++;
    }

    public void addToGroup(Student student) {
        allStudents[numStudents] = student;
        numStudents++;
    }

    public boolean findStudent(int number, String firstName, String lastName) {
        for (int i = 0; i < allStudents.length; i++) {
            if (number == allStudents[i].getNumber()) {
                allStudents[i].setFirstName(firstName);
                allStudents[i].setLastName(lastName);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < allStudents.length; i++) {
            str += allStudents[i] + "\n";
        }
        return str;
    }
}
