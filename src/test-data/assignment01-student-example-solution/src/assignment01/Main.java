package assignment01;

import java.util.*;

/**
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Main {

    private static Scanner scanner;
    private static Group group;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        welcomeMessage();
        allStudents();
        changeStudent();
    }

    private static void welcomeMessage() {
        System.out.println("Hello, welcome to the program made by Dylan and Misja");
        System.out.println("");
    }

    private static void allStudents() {
        System.out.println("How big is the group of students supposed to be?");
        System.out.println("Please provide the number: ");
        int amountStudents = scanner.nextInt();
        while (amountStudents <= 0) {
            System.out.println("Please give a number that is higher than 0!");
            System.out.println("How big is the group of students supposed to be?");
            System.out.println("Please provide the number: ");
            amountStudents = scanner.nextInt();
        }
        group = new Group(amountStudents);
        for (int addedPersons = 0; addedPersons < amountStudents; addedPersons++) {
            System.out.println("Please enter a student number, first name and"
                    + " last name: ");
            System.out.println("format: number first last");
            int studentNumber = scanner.nextInt();
            String firstName = scanner.next();
            String lastName = scanner.next();
            Student newStudent = new Student(studentNumber, firstName, lastName);
            group.addToGroup(newStudent, addedPersons);
        }
        System.out.println("");
        System.out.println("All students: ");
        System.out.println(group);
    }

    private static void changeStudent() {
        System.out.println("Enter a studentnumber of which you want to change "
                + "the first and lastname.");
        System.out.println("If you want to stop or do not want to change a"
                + " student enter a negative number.");
        System.out.println("format: number newfirst newlast");
        int number = scanner.nextInt();

        while (number >= 0) {
            String firstName = scanner.next();
            String lastName = scanner.next();
            if (group.findStudent(number, firstName, lastName)) {
                System.out.println("Student changed");
            } else {
                System.out.println("This number does not exist, please try again.");
            }
            System.out.println(group);
            System.out.println("Enter a studentnumber of which you want to "
                    + "change the first and lastname.");
            System.out.println("If you want to stop or do not want to change a "
                    + "student enter a negative number.");
            System.out.println("format: number newfirst newlast");
            number = scanner.nextInt();
        }
        System.out.println("Bye, have a good day!");
    }
}
