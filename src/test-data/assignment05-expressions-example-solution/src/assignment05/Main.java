package assignment05;

import java.util.HashMap;
import java.util.Map;
import static assignment05.ExpressionFactory.*;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Main {
	public static void main(String[] args) {
		Map<String, Double> store = new HashMap<>();
		store.put("pi", 3.1415);
		store.put("cactus", 34.);
		store.put("cons", 2.0);
		store.put("x", 4.0);
		store.put("y", -1.0);

		System.out.println("pi = " + store.get("pi"));
		System.out.println("");
		Expression e1 = add(mul(con(2.0), con(3.0)), var("x"));
		System.out.println(e1);
		System.out.println(e1.partialEval());
		System.out.println(e1.eval(store));
		System.out.println("");
		Expression e2 = add(add(con(-2.0), con(3.0)), var("x"));
		System.out.println(e2);
		System.out.println(e2.partialEval());
		System.out.println(e2.eval(store));
		System.out.println("");
		Expression e3 = mul(add(var("x"), var("y")), con(3.0));
		System.out.println(e3);
		System.out.println(e3.partialEval());
		System.out.println(e3.eval(store));
		System.out.println("");
		Expression e4 = add(neg(con(2.0)), var("x"));
		System.out.println(e4);
		System.out.println(e4.partialEval());
		System.out.println(e4.eval(store));
	}
}
