package assignment05;

import java.util.Map;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Variable extends NoArgExpr {

	private String name;

	public Variable(String var) {
		this.name = var;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public double eval(Map<String, Double> env) {
		return env.get(name);
	}

	public String getVar() {
		return name;
	}

	@Override
	public Expression partialEval() {
		return new Variable(name);
	}

}
