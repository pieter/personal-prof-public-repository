package assignment05;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public abstract interface Expression {

	abstract String toString();

	abstract double eval(Map<String, Double> env);

	abstract Expression partialEval();

	public static final Map<String,Double> EMPTY_ENV = new HashMap<>();
}
