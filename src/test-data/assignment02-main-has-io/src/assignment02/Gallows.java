package assignment02;

import java.util.LinkedList;
import java.util.List;

public class Gallows {

	private String wordToGuess;
	private List<Character> guessedLetters;
	private int guessesLeft;

	public int getGuessesLeft() {
		return guessesLeft;
	}

	public String getGuessedLetters() {
		StringBuilder result = new StringBuilder();
		for( char l : guessedLetters )
		{
			result.append(l);
		}
		return result.toString();
	}

	public String getWordToGuess() {
		return wordToGuess;
	}

	// When you input your own word.
	public Gallows(String s) {
		guessedLetters = new LinkedList<>();
		this.wordToGuess = s;
		guessesLeft = 10;
	}

//   When you want an automated word.
	public Gallows() {
		guessedLetters = new LinkedList<>();
		WordReader file = new WordReader("words.txt");
		this.wordToGuess = file.getWord();
		guessesLeft = 10;
	}

	public boolean isWordGuessed() {
		for (int i = 0; i < wordToGuess.length(); ++i) {
			if (!guessedLetters.contains(wordToGuess.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public void guessLetter(char c) {
		guessesLeft--;
		guessedLetters.add(c);
	}

	public String getWordSoFar() {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < wordToGuess.length(); ++i) {
			char currentLetter = wordToGuess.charAt(i);
			if (guessedLetters.contains(currentLetter)) {
				result.append(currentLetter);
			} else {
				result.append(".");
			}
		}
		return result.toString();
	}
}