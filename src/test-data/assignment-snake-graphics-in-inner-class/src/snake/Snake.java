package snake;

import java.util.LinkedList;
import java.util.List;

/**
 * Snake consists of segments, where this head segment keeps track of the other body segments
 */
public class Snake extends Segment {

    public interface SnakeSegmentListener {
        void onNewSegment(Segment segment);
    }

    private Direction direction = Direction.RIGHT;

    private final World world;

    private final List<Segment> body = new LinkedList<>();

    private final List<SnakeSegmentListener> listeners = new LinkedList<>();

    public Snake(int x, int y, World world) {
        super(x, y);
        this.world = world;
    }

    public void move() {
        int newX = getX() + direction.getDX();
        int newY = getY() + direction.getDY();

        if (isAt(newX, newY) || newX < 0 || newY < 0 || newX >= world.getSize() || newY >= world.getSize()) {
            // Bitten itself or the border, game over
            world.endGame();
        } else {
            Food food = world.getFood();

            if (food.getX() == newX && food.getY() == newY) {
                // Eating fruit, increment score and add new segment before the head
                world.setScore(world.getScore() + 1);
                world.moveFoodRandomly();

                Segment segment = new Segment(getX(), getY());

                for (SnakeSegmentListener listener : listeners) {
                    listener.onNewSegment(segment);
                }

                body.add(segment);
            } else {
                // Moving normally, recycle tail and move it before head
                if (!body.isEmpty()) {
                    Segment tail = body.remove(0);
                    body.add(tail);

                    tail.setPosition(getX(), getY());
                }
            }

            // Move head to new location
            setPosition(newX, newY);
        }
    }

    public void addListener(SnakeSegmentListener listener) {
        listeners.add(listener);
    }

    public void setDirection(Direction newDirection) {
        direction = newDirection;
    }

    public boolean isAt(int x, int y) {
        for (Segment segment : body) {
            if (segment.getX() == x && segment.getY() == y) {
                return true;
            }
        }

        return false;
    }

    public Direction getDirection() {
        return direction;
    }
}
