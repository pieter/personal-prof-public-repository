package snake;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import snake.Snake.SnakeSegmentListener;

/**
 * A JavaFX Pane that displays the snake game represented by the given world
 */
public class SnakeGame extends Pane {

    public static final int SCALE = 16;
    
    private class SegmentCallback implements SnakeSegmentListener
    {
		public void onNewSegment(Segment segment) {
            Rectangle body = new Rectangle(SCALE, SCALE, Color.GREEN);

            body.translateXProperty().bind(segment.getXProperty().multiply(SCALE));
            body.translateYProperty().bind(segment.getYProperty().multiply(SCALE));

            getChildren().add(body);
		}
    }

    public SnakeGame(World world) {
        setPrefSize(world.getSize() * SCALE, world.getSize() * SCALE);

        // Snake

        Snake snake = world.getSnake();

        Rectangle head = new Rectangle(SCALE, SCALE, Color.RED);

        head.translateXProperty().bind(snake.getXProperty().multiply(SCALE));
        head.translateYProperty().bind(snake.getYProperty().multiply(SCALE));

        getChildren().add(head);

        snake.addListener(new SegmentCallback());

        // Food

        Food food = world.getFood();

        Circle circle = new Circle(SCALE / 2F, Color.BLUE);

        circle.translateXProperty().bind(food.getXProperty().multiply(SCALE).add(SCALE / 2F));
        circle.translateYProperty().bind(food.getYProperty().multiply(SCALE).add(SCALE / 2F));

        getChildren().add(circle);
    }

    public static Pane createUserInterface(World world) {
        VBox ui = new VBox();

        Label scoreText = new Label();
        Label runningText = new Label("Press 's' to start");

        scoreText.textProperty().bind(world.getScoreProperty().asString("%d points"));
        world.getRunningProperty().addListener((observableValue, aBoolean, t1) -> {
            if (t1) {
                runningText.textProperty().set("");
            } else {
                runningText.textProperty().set("Paused");
            }
        });

        ui.getChildren().addAll(scoreText, runningText);

        return ui;
    }
}
