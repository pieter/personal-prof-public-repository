package exercise3oo;

import java.util.Comparator;

public class Ycomparator implements Comparator<Geometric> {

    @Override
    public int compare(Geometric o1, Geometric o2) {
        if (o1.bottomBorder() == o2.bottomBorder()) {
            return 0;
        }
        if (o1.bottomBorder() < o2.bottomBorder()) {
            return -1;
        } else {
            return 1;
        }
    }
}
