package exercise3oo;

import java.util.Comparator;

public class Xcomparator implements Comparator<Geometric> {

    @Override
    public int compare(Geometric o1, Geometric o2) {
        if (o1.leftBorder() == o2.leftBorder()) {
            return 0;
        }
        if (o1.leftBorder() < o2.leftBorder()) {
            return -1;
        } else {
            return 1;
        }
    }
}
