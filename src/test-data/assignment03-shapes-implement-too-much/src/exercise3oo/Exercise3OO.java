package exercise3oo;

import java.util.Collections;
import java.util.Scanner;

public class Exercise3OO {

    private final int maxObjects;
    private Scanner scan;
    private GeometryArray fillObjects;
    private double x, y, radius, height, width;

    public static void main(String[] args) {

        new Exercise3OO();
    }

    public Exercise3OO() {
        maxObjects = 3;
        fillObjects = new GeometryArray();
        scan = new Scanner(System.in);
        chosen();
    }

    public void chosen() {
        boolean stop = false;
        while (!stop) {
            instructions();
            String choice = scan.next();
            if (choice.equals("quit")) {
                System.out.println("");
                System.out.println("the program has been terminated");
                stop = true;
            } else if (choice.equals("show")) {
                doShow();
            } else if (choice.equals("circle")) {
                doCircle();
            } else if (choice.equals("rectangle")) {
                doRectangle();
            } else if (choice.equals("move")) {
                moveObject();
                System.out.println("");
            } else if (choice.equals("remove")) {
                doRemove();
            } else if (choice.equals("sort")) {
                doMove();
            } else {
                System.out.println("Not a valid input!");
            }
        }
    }

    public void doShow() {
        for (int i = 0; i < fillObjects.getSize(); i++) {
            System.out.println(fillObjects.getElement(i).toString());
        }
        System.out.println("");
    }

    public void doRectangle() {
        x = scan.nextDouble();
        y = scan.nextDouble();
        height = scan.nextDouble();
        width = scan.nextDouble();
        if (fillObjects.getSize() >= maxObjects) {
            System.out.println("The arrayList is full");
            System.out.println("");
        } else {
            makeRectangle();
            System.out.println("");
        }
    }

    public void doCircle() {
        x = scan.nextDouble();
        y = scan.nextDouble();
        radius = scan.nextDouble();
        if (fillObjects.getSize() >= maxObjects) {
            System.out.println("The arrayList is full");
        } else {
            makeCircle();
            System.out.println("");
        }
    }

    public void doRemove() {
        int index = scan.nextInt();
        if (index < fillObjects.getObjects().size()) {
            fillObjects.remove(index);
        } else {
            System.out.println("sorry, this index does not exist");
        }
        System.out.println("");
    }

    public void doMove() {
        String line = scan.nextLine();
        if (line.equals("X")) {
            Xcomparator xComp = new Xcomparator();
            Collections.sort(fillObjects.getObjects(), xComp);
            if (line.equals("Y")) {
                Ycomparator yComp = new Ycomparator();
                Collections.sort(fillObjects.getObjects(), yComp);
            }
        } else {
            Collections.sort(fillObjects.getObjects());
        }
        System.out.println("");
    }

    public void instructions() {
        System.out.println("Your options are;");
        System.out.println("quit - stops the program.");
        System.out.println("show - lists the geometric objects.");
        System.out.println("circle x y r - adds a circle at (x,y) with radius r.");
        System.out.println("rectangle x y h w - adds a rectangle at (x,y) with height h and width h.");
        System.out.println("move i dx dy - moves object i over the specified distance in x and y direction.");
        System.out.println("remove i -deletes object i");
        System.out.println("sort - sorts the object in the array according to criterio c (see next lines)");
        System.out.println("X: objects are sorted on their most left point from small to large");
        System.out.println("Y: objects are sorted by their bottom point from small to large");
        System.out.println("No arguments: objects are sorted on their area's from small to large");
        System.out.println("Enter your command: ");
    }

    public void makeCircle() {
        Circle circle = new Circle(x, y, radius);
        fillObjects.addCircle(circle);
    }

    public void makeRectangle() {
        Rectangle rectangle = new Rectangle(x, y, height, width);
        fillObjects.addRectangle(rectangle);
    }

    private void moveObject() {
        int index = scan.nextInt();
        double x = scan.nextDouble();
        double y = scan.nextDouble();
        fillObjects.move(x, y, index);
    }
}
