package exercise3oo;

public interface Geometric extends Comparable<Geometric> {

    double leftBorder();

    double rightBorder();

    double bottomBorder();

    double topBorder();

    double areaObject();

    void moveObject(double dx, double dy);

    @Override
    default int compareTo(Geometric x) {
        if (this.areaObject() < x.areaObject()) {
            return -1;
        }
        if (this.areaObject() == x.areaObject()) {
            return 0;
        } else {
            return 1;
        }
    }

}
