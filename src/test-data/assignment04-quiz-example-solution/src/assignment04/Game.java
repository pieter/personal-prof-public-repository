package assignment04;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Game {

	private Scanner scanner;
	private int firstRoundScore, secondRoundScore;
	private List<Question> questions;
	private List<Question> roundTwoQuestions;

	public Game() {
		scanner = new Scanner(System.in);
		questions = new LinkedList<>();
		roundTwoQuestions = new LinkedList<>();
		initializeQuestions();
	}

	private void initializeQuestions() {
		questions = ExampleQuestions.getExampleQuestions();
	}

	public void play() {
		round1();
		round2();
		printResult();
	}

	private void round1() {
		for (Question q : questions) {
			System.out.println(q);
			String answer = scanner.nextLine();
			if (q.isCorrect(answer)) {
				firstRoundScore += q.getScore();
				System.out.println("Correct!");
			} else {
				roundTwoQuestions.add(q);
				System.out.println("Not correct. The correct answer is:");
				System.out.println(q.correctAnswer());
			}
		}
	}

	private void round2() {
		for (Question q : roundTwoQuestions) {
			System.out.println(q);
			String answer = scanner.nextLine();
			if (q.isCorrect(answer)) {
				secondRoundScore += q.getScore();
				System.out.println("Correct!");
			} else {
				System.out.println("The correct answer would have been:");
				System.out.println(q.correctAnswer());
			}
		}
	}

	private void printResult() {
		System.out.println("Score first round: " + firstRoundScore);
		System.out.println("Score second round: " + secondRoundScore);
	}
}