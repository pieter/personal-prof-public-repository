package filefindermergesort;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Benjamin Robijn, s4822331
 */
public class FileFinder implements Runnable {

    private final File rootDir;

    //For use in temp solution
    private File tempDir;
    private String tempName;

    public FileFinder(String root) throws IOException {
        rootDir = new File(root);
        if (!(rootDir.exists() && rootDir.isDirectory())) {
            throw new IOException(root + " is not a directory");
        }
    }

    public void findFile(String file) throws IOException, InterruptedException {
        find(rootDir, file);
    }

    private void find(File rootDir, String fileName) throws IOException, InterruptedException {
        File[] files = rootDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().equals(fileName)) {
                    System.out.println("Found at: " + file.getAbsolutePath());
                    return;
                } else if (file.isDirectory()) {

                    tempDir = file;
                    tempName = fileName;
                    FileFinder newroute = new FileFinder(file.toString());
                    Thread newroute_2 = new Thread(newroute);
                    newroute_2.start();
                    newroute_2.join();
                }
            }
        }
    }

    @Override
    public void run() {
        try {
            if (tempDir != null) {
                find(tempDir, tempName);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileFinder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(FileFinder.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
