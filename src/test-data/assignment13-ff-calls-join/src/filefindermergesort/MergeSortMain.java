/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filefindermergesort;

import java.util.Arrays;

/**
 *
 * Run succesfully with -Xmx1024m applied in the 'run' section, system gives
 * error for values above ~1700 range. Given a threshold of 10.000, likely
 * because of the lack of proper memory allocation due to errors in the virtual
 * machine-
 *
 * Sequential: 1759 ms Parallel: 1063 ms Cores: 12
 *
 *
 * @author Benjamin Robijn, s4822331
 */
public class MergeSortMain {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        long time1, time2;

        //Randomly fill int array
        final int L = 10000000;

        int[] a1 = new int[L];
        int[] a2 = new int[L];

        for (int i = 0; i < L; i += 1) {
            a1[i] = a2[i] = (int) (Math.random() * L);
        }

        //Sequential sort
        System.out.print("sequential sort: ");
        time1 = System.currentTimeMillis();
        MergeSort.sort(a1);
        time2 = System.currentTimeMillis();
        System.out.println((time2 - time1) + " millis. Array is sorted = " + MergeSort.isSorted(a1));

        //Parallel sort
        System.out.print("parrallel sort: ");
        time1 = System.currentTimeMillis();
        ParallelMergeSort.sort(a2);
        time2 = System.currentTimeMillis();
        System.out.println((time2 - time1) + " millis. Array is sorted = " + MergeSort.isSorted(a2));
        System.out.println("sorter arrays equals = " + Arrays.equals(a1, a2));
        System.out.println("The number of processors is " + Runtime.getRuntime().availableProcessors());
    }
}
