package filefindermergesort;

import java.io.IOException;

/**
 * Run succesfully with -Xmx1024m applied in the 'run' section, system gives
 * error for values above ~1700 range.
 *
 * @author Benjamin Robijn, s4822331
 *
 */
public class FileFinderMain {

    public static void main(String[] args) throws InterruptedException {
        FileFinderTest();
    }

    public static void FileFinderTest() throws InterruptedException {
        try {
            String goal = "FileFinder.java";
            String root = "C:\\Users\\Benjamin (Chibita)\\Documents\\NetBeansProjects";
            FileFinder ff = new FileFinder(root);
            ff.findFile(goal);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
