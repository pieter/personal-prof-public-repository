package filefindermergesort;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Benjamin Robijn, s4822331
 *
 */
public class ParallelMergeSort implements Runnable {

    /**
     * sort the given array in O(N log N) time The array is split in two parts
     * of equal size. These parts are sort recursively and merged.
     *
     * @param array
     */
    private final int[] sub;

    public ParallelMergeSort(int[] arr) {
        this.sub = arr;
    }

    public static void sort(int[] array) throws InterruptedException {

        int[] firstHalf = Arrays.copyOf(array, array.length / 2);
        int[] secondHalf = Arrays.copyOfRange(array, array.length / 2, array.length);

        if (array.length > 10000) {

            ParallelMergeSort o1 = new ParallelMergeSort(firstHalf);
            ParallelMergeSort o2 = new ParallelMergeSort(secondHalf);

            Thread th1 = new Thread(o1);
            Thread th2 = new Thread(o2);

            th1.start();
            th2.start();

            th1.join();
            th2.join();

        } else {

            if (array.length > 1) {

                firstHalf = Arrays.copyOf(array, array.length / 2);

                sort(firstHalf);

                secondHalf = Arrays.copyOfRange(array, array.length / 2, array.length);

                sort(secondHalf);

                merge(firstHalf, secondHalf, array);

            }

        }

        merge(firstHalf, secondHalf, array);

    }

    /**
     * merge two sorted arrays: O(N)
     *
     * @param part1 a sorted array
     * @param part2 a sorted array
     * @param dest destination, length must be >= part1.length + part2.length
     */
    public static void merge(int[] part1, int[] part2, int dest[]) {
        int part1Index = 0, part2Index = 0, destIndex = 0;
        while (part1Index < part1.length && part2Index < part2.length) {
            if (part1[part1Index] < part2[part2Index]) {
                dest[destIndex++] = part1[part1Index++];
            } else {
                dest[destIndex++] = part2[part2Index++];
            }
        }
        // copy elements when at most one of the parts contains elements
        while (part1Index < part1.length) {
            dest[destIndex++] = part1[part1Index++];
        }
        while (part2Index < part2.length) {
            dest[destIndex++] = part2[part2Index++];
        }
    }

    /**
     * simple check to see if array is nondecreasing
     *
     * @param array
     * @return array is sorted
     */
    public static boolean isSorted(int[] array) {
        int current = array[0];
        for (int i : array) {
            if (i < current) {
                return false;
            } else {
                current = i;
            }
        }
        return true;
    }

    @Override
    public void run() {
        int[] curr = this.sub;
        try {

            sort(curr);

        } catch (InterruptedException ex) {
            Logger.getLogger(ParallelMergeSort.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
