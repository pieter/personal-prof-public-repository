package taxi;

import static taxi.Simulation.*;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Trains bring a number of passengers to a station in the Taxi simulation
 */
public class Train implements Runnable {

	private int nrOfPassengers;
	private final Station station;
	private int nrOfTrips = 0;

	private final Lock lock = new ReentrantLock();
	private final Condition passengersAtStation = lock.newCondition();

	public Train(Station station) {
		this.station = station;
		this.nrOfPassengers = 0;
		System.out.println("Train created");
	}

	/**
	 * Populate this train with number nrOfPassengers
	 *
	 * @param number the number of passengers of this train
	 */
	public void loadPassengers(int number) {
		nrOfPassengers = number;
	}

	/**
	 * empties this train and augment the number of Passengers at the station with
	 * this nrOfPassenegers
	 */
	public void unloadPassengers() throws InterruptedException {
		nrOfTrips += 1;
		station.enterStation(nrOfPassengers);
	}

	public void closeStation() {
		station.noMoreTrains();
	}

	public int getNrOfTrips() {
		return nrOfTrips;
	}

	@Override
	public void run() {
		System.out.println("Train started");
		try {
			for (nrOfTrips = 0; nrOfTrips < TRAIN_TRIPS; nrOfTrips++) {
				Thread.sleep(Util.getRandomNumber(TRAIN_WAIT_TIME - 200, TRAIN_WAIT_TIME + 200));
				loadPassengers(Util.getRandomNumber(MIN_TRAVELLERS, MAX_TRAVELLERS));
				unloadPassengers();
			}
			station.noMoreTrains();
		} catch (InterruptedException e) {
			System.out.println("Train got interrupted.");
		}
	}
}
