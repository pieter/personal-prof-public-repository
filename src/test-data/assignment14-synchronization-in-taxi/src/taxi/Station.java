package taxi;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class that holds the number of persons arriving by train at the station and
 * waiting for a taxi
 */
public class Station {

	private int nrOfPassengersAtStation = 0;
	private int totalNrOfPassengers = 0;
	private boolean noMoreTrains = false;
	private boolean isClosed = false;

	private final Lock lock = new ReentrantLock();
	private final Condition passengersAtStation = lock.newCondition();
	private final Condition allPassengersGone = lock.newCondition();

	public void enterStation(int nrOfPassengers) throws InterruptedException {
		lock.lock();
		try {
			while (nrOfPassengersAtStation > 0) {
				System.out.println("Waiting for all passengers to leave.");
				allPassengersGone.await();
			}

			nrOfPassengersAtStation += nrOfPassengers;
			totalNrOfPassengers += nrOfPassengers;
			passengersAtStation.signalAll();
			System.out.println(nrOfPassengers + " passengers arrived at station");
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Ask for nrOfPassengers Passengers to leave the station
	 *
	 * @param requestedNrOfPassengers
	 * @return number of passengers actually leaving
	 */
	public int leaveStation(int requestedNrOfPassengers) throws InterruptedException {
		lock.lock();
		try {
			while (nrOfPassengersAtStation == 0 && !isClosed) {
				System.out.println("Waiting for passengers to arrive.");
				passengersAtStation.await();
			}

			int actuallyLeaving = Math.min(requestedNrOfPassengers, nrOfPassengersAtStation);

			nrOfPassengersAtStation -= actuallyLeaving;

			if (noMoreTrains && nrOfPassengersAtStation <= 0) {
				isClosed = true;
				passengersAtStation.signalAll();
			}

			if (nrOfPassengersAtStation == 0) {
				allPassengersGone.signalAll();
			}

			return actuallyLeaving;
		} finally {
			lock.unlock();
		}
	}

	public int waitingPassengers() {
		lock.lock();
		try {
			return nrOfPassengersAtStation;
		} finally {
			lock.unlock();
		}
	}

	public void noMoreTrains() {
		lock.lock();
		try {
			noMoreTrains = true;
		} finally {
			lock.unlock();
		}
	}

	public boolean isClosed() {
		lock.lock();
		try {
			return isClosed;
		} finally {
			lock.unlock();
		}
	}

	public int getTotalNrOfPassengers() {
		lock.lock();
		try {
			return totalNrOfPassengers;
		} finally {
			lock.unlock();
		}
	}
}
