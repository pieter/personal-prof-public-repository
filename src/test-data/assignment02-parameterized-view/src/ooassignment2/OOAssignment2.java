/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooassignment2;

import java.util.Scanner;

/**
 *
 * @author Max Jongmans (s1026392)
 * @author Mario Tsatsev (s1028415)
 */
public class OOAssignment2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        View View = new View (input);
        View.pickWord();
        View.guess();
    }
    
}
