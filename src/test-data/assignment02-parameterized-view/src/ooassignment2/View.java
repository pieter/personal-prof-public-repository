/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooassignment2;

import java.util.Scanner;

/**
 *
 * @author Max Jongmans (s1026392)
 * @author Mario Tsatsev (s1028415)
 */
public class View {
    private Scanner input;
    private Gallows hangman;
            
    public View (Scanner input) {
        this.input = input;
    }
    
    public void pickWord () {
        System.out.println ("Welcome to Hangman!\n"
                + "Please enter a word or press Enter to pick a random one: ");
        String pickedWord = this.input.nextLine();
        StringBuilder start = new StringBuilder ("");
        if (pickedWord.length () == 0) {
            this.hangman = new Gallows (10, start, start);
        }
        else {
            this.hangman = new Gallows (pickedWord, 10, start, start);
        }
    }
    
    public void guess () {
        int counter = 0;
        boolean done = false;
        while (done == false) {   
            System.out.println ("Guess: ");
            String guess = this.input.next ();
            if (hangman.guessRight (guess)) {
                System.out.println (guess + " is in the word.");
            }
            else {
                System.out.println (guess + " is not in the word.");
            }
            int mistakes = hangman.mistakesLeft(guess);
            System.out.println ("Remaining mistakes: " + mistakes);
            StringBuilder guesses = new StringBuilder(hangman.addGuess(guess));
            System.out.println ("Guessed letters: [" + guesses + "]");
            StringBuilder displayedWord = new StringBuilder ("");
            displayedWord = hangman.wordDisplay (displayedWord);
            System.out.println ("Word: " + displayedWord);    
            done = hangman.done ();
        }
        if (hangman.winOrLose () == 0) {
            System.out.println ("You won!");
        }
        else {
            System.out.println ("You lost...");
        }
    }
}
