/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ooassignment2;

/**
 *
 * @author Max Jongmans (s1026392)
 * @author Mario Tsatsev (s1028415)
 */
public class Gallows {
    private String word;
    private int mistakes;
    private StringBuilder guesses;
    private StringBuilder displayedWord;

    public Gallows (int mistakes, StringBuilder guesses, StringBuilder displayedWord) {
        WordReader RndmWord = new WordReader ("words.txt");
        this.word = RndmWord.getWord ();
        this.mistakes = mistakes;
        this.guesses = guesses;
        this.displayedWord = displayedWord;
    }   
    
    public Gallows (String word, int mistakes, StringBuilder guesses, StringBuilder displayedWord) {
        this.word = word;
        this.mistakes = mistakes;
        this.guesses = guesses;
        this.displayedWord = displayedWord;
    } 
    
    public boolean guessRight (String guess) {
        boolean right = false;
        for (int i = 0; i < this.word.length (); i++) {
            if (this.word.charAt (i) == guess.charAt (0)) {
                right = true;
            }
        }
        return right;
    }
            
    public int mistakesLeft (String guess) {
        if (guessRight (guess) == false) {
            this.mistakes = this.mistakes - 1;
        }
        return this.mistakes;
    }
    
    public StringBuilder addGuess (String guess) {
        this.guesses.append(guess);
        return this.guesses;
    }
    
    public int winOrLose () {
        int counter = 0;
        for (int i = 0; i < displayedWord.length(); i++) {
            if (displayedWord.charAt (i) == '.') {
                counter++;
            }
        }
        return counter;
    }
    
    public boolean done () {
        int counter = winOrLose ();
        if (this.mistakes == 0 || counter == 0) {
            return true;
        }
        return false;
    }
    
    public StringBuilder wordDisplay (StringBuilder displayedWord) {
        for (int i = 0; i < this.word.length(); i++) {
            displayedWord.append (".");
        }
        for (int i = 0; i < displayedWord.length(); i++) {
            for (int j = 0; j < this.guesses.length(); j++) {
                if (this.word.charAt (i) == this.guesses.charAt (j)) {
                    displayedWord.setCharAt (i, guesses.charAt (j));
                }
            }
        }
        this.displayedWord = displayedWord;
        return displayedWord;
    }
}
