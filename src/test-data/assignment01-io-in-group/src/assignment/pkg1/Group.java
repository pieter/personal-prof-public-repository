package assignment.pkg1;

import java.util.Scanner;

public class Group{
    private int size;
    private int index;
    private Student student;
    private Student[] students;
    
    public Group () {
        this.size = size();
        this.student = student;
        this.students = new Student [size]; // Klopt dit?
    }
    public int size(){
        System.out.println ("Hello, how big should the group of students be?\n");
        Scanner scanner = new Scanner (System.in);
        int answer = scanner.nextInt();
        return answer;
    }
    public void fillGroup(){
        for (int index = 0; index < size; index++){
            Student new_student = new Student("a","a",0);
            new_student.requestInfo("a","a",0);
            fillArray(students, new_student, index);
        }
        print();
    }
    public void fillArray (Student[] students, Student student, int index) {
        students [index] = student;
    }  
    public void print (){
        System.out.println("\nThe group now contains:");
        for (int x = 0; x < size; x++){
            System.out.println(students[x].toString());   
        }
    }
    public void update (){
        int number = 0;
        while (number >= 0)
        {
            System.out.println("\nPlease insert the studentnumber of a stundent whose name you want to alter.");
            System.out.println("Enter a negative integer to terminate the program.");
            Scanner scanner = new Scanner (System.in);
            number = scanner.nextInt();
            if (number < 0)
            {
                System.out.println("Goodbye");
                return;
            }
            String given_name, fam_name;
            System.out.println("Now insert the (new) first name.");
            given_name = scanner.next();
            System.out.println("Insert the (new) family name.");
            fam_name = scanner.next();
            for (int x = 0; x < size; x++)
               {
                    if (number == students[x].getStudentNumber())
                    {
                        students[x].setFirstName(given_name);
                        students[x].setFamilyName(fam_name);
                        print();
                    }
               }
            
        }
    }
}
    
