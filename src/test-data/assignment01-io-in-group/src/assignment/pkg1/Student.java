package assignment.pkg1;

import java.util.Scanner;

public class Student {
    private String first_name;
    private String family_name;
    private int student_number;
    
    public Student (String first_name, String family_name, int student_number){
        this.first_name = first_name;
        this.family_name = family_name;
        this.student_number = student_number;
    }
    public void changeFirstName(String new_first_name){
        this.first_name = new_first_name; 
    }
    public void changeFamilyName(String new_family_name){
        this.family_name = new_family_name; 
    }
    public String requestInfo(String first_name, String family_name, int student_number){
        System.out.println ("Would you please insert the student's first name.\n");
        Scanner scanner = new Scanner (System.in);
        first_name = scanner.nextLine();
        System.out.println ("Thank you, now please insert the student's family name.\n");
        family_name = scanner.nextLine();
        System.out.println ("Thank you, now please insert the student's number.\n");
        student_number = scanner.nextInt();
        this.first_name = first_name;
        this.family_name = family_name;
        this.student_number = student_number;
        return first_name + " " + family_name + " " + student_number;
    }
    public String toString(){
        return first_name + " " + family_name + " s" + student_number;// + "\n";
    }
    public int getStudentNumber(){
        return student_number;
    }
    public void setFirstName(String given_name){
        this.first_name = given_name;
    }
    public void setFamilyName(String fam_name){
        this.family_name = fam_name;
    }
}
