package geometric;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class GeometricArray {

	private ArrayList<Geometric> objects;

	public GeometricArray() {
		objects = new ArrayList<>();
	}

	public void addCircle(Circle circle) {
		objects.add(circle);
	}

	public void addRectangle(Rectangle rectangle) {
		objects.add(rectangle);
	}

	public int getSize() {
		return objects.size();
	}

	public Geometric getElement(int index) {
		return objects.get(index);
	}

	public void move(int index, double dx, double dy) {
		objects.get(index).moveObject(dx, dy);
	}

	public void remove(int index) {
		objects.remove(index);
	}

	public ArrayList<Geometric> getObjects() {
		return this.objects;
	}

	public void sortByArea() {
		Collections.sort(objects);
	}

	public void sortByX() {
		Xcomparator xComp = new Xcomparator();
		Collections.sort(objects, xComp);
	}

	public void sortByY() {
		Ycomparator yComp = new Ycomparator();
		Collections.sort(objects, yComp);
	}
}
