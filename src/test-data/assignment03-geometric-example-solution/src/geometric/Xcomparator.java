package geometric;

import java.util.Comparator;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Xcomparator implements Comparator<Geometric> {

	@Override
	public int compare(Geometric o1, Geometric o2) {
		if (o1.leftBorder() == o2.leftBorder()) {
			return 0;
		}
		if (o1.leftBorder() < o2.leftBorder()) {
			return -1;
		} else {
			return 1;
		}
	}
}
