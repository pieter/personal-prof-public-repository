package geometric;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Circle implements Geometric {

	private double x, y, radius;

	public Circle(double x, double y, double radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	@Override
	public double leftBorder() {
		return x - radius;
	}

	@Override
	public double rightBorder() {
		return x + radius;
	}

	@Override
	public double bottomBorder() {
		return y - radius;
	}

	@Override
	public double topBorder() {
		return y + radius;
	}

	@Override
	public double areaObject() {
		return Math.PI * radius * radius;
	}

	@Override
	public void moveObject(double dx, double dy) {
		this.x = x + dx;
		this.y = y + dy;
	}

	@Override
	public String toString() {
		String dx = String.valueOf(x);
		String dy = String.valueOf(y);
		String dradius = String.valueOf(radius);
		return "circle: x coordinate: " + dx + ", y coordinate: " + dy + ", radius: " + dradius;
	}

	public double getArea() {
		return areaObject();
	}
}
