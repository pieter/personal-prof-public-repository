package assignment06;

import java.util.*;

/**
 * @author Pieter Koopman, Sjaak Smetsers
 * @version 08-03-2012
 */
public class BreadthFirstSolver {

	protected Queue<Configuration> toExamine;
	protected Collection<Configuration> encountered;

	public BreadthFirstSolver(Configuration g) {
		toExamine = new LinkedList<Configuration>();
		encountered = new HashSet<Configuration>();
		toExamine.add(g);
		encountered.add(g);
	}

	public String solve() {
		while (!toExamine.isEmpty()) {
			Configuration next = toExamine.remove();
			if (next.isSolution()) {
				return printSolutions(next);
			} else {
				for (Configuration succ : next.successors()) {
					if (!encountered.contains(succ)) {
						toExamine.add(succ);
						encountered.add(succ);
					}
				}
			}
		}
		return "No solution found";
	}

	private String printSolutions(Configuration c) {
		StringBuilder b = new StringBuilder();
		for (Configuration conf : c.pathFromRoot()) {
			b.append(conf).append("\n");
		}
		return b.toString();
	}

}
