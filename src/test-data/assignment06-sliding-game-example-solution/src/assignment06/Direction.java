package assignment06;

/**
 * @author Sjaak
 */
public enum Direction {
	NORTH(0, -1), EAST(1, 0), SOUTH(0, 1), WEST(-1, 0);

	private final int dx, dy;

	Direction(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	public int getDX() {
		return dx;
	}

	public int getDY() {
		return dy;
	}
}
