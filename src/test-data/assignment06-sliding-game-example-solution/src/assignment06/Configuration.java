package assignment06;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public interface Configuration extends Comparable<Configuration> {
	abstract Configuration getParent();

	Collection<Configuration> successors();

	boolean isSolution();

	default List<Configuration> pathFromRoot() {
		List<Configuration> result = new LinkedList<>();
		Configuration node = this;
		while( node != null )
		{
			result.add(0, node);
			node = node.getParent();
		}
		return result;
	}
}
