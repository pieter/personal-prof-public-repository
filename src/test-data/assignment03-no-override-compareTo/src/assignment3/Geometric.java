package assignment3;

public interface Geometric extends Comparable <Geometric> {
    double getLeftBorder();
    double getRightBorder();
    double getBottomBorder();
    double getTopBorder();
    double getArea();
    void moveGeometric (double dx, double dy);
    
    @Override
    public String toString ();
}
