package assignment3;
import java.util.Comparator;

public class XComparator implements Comparator<Geometric> {

    @Override
    public int compare (Geometric shape1, Geometric shape2) {
        double x1 = shape1.getLeftBorder();
        double x2 = shape2.getLeftBorder();
        if (x1 < x2) { 
            return -1;
        }
        else if (x1 == x2) {
            return 0;
        }
        else if (x1 > x2) {
            return 1;
        }
        return 0;
    }
}
