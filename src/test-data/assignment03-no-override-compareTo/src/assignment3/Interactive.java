package assignment3;
import java.util.Arrays;

public class Interactive {
    private static final int MAX_SHAPES = 10;
    private boolean quit;
    private IO io;
    private Geometric shapes[];
    private int nrOfShapes;
    private XComparator xComp;
    private YComparator yComp;
    
    public Interactive () {
        this.quit = false;
        this.io = new IO();
        this.shapes = new Geometric[MAX_SHAPES];
        this.nrOfShapes = 0;
        this.xComp = new XComparator ();
        this.yComp = new YComparator ();
    }
    
    public void run() {
    while (!quit) {
        String input[] = io.getInput().split(" ");
        switch (input[0]) {
            case "quit": io.showStopMessage(); quit = true; break;
            case "show": showShapes(); break;
            case "circle": addCircle (input); break;
            case "rectangle": addRectangle (input); break;
            case "move": moveShape (input); break;
            case "remove": removeShape (input); break;
            case "sort": sortShapes (input); break;
            default: io.showUnknownCommandMessage();
            }
        }
    }
    
    private void showShapes() {
        for (int i = 0; i < nrOfShapes; i++) {
            io.showShape(i, shapes[i]);
        }
    }
    
    private void addCircle (String input[]) {
        if (nrOfShapes < MAX_SHAPES) {
            double xCoord = Double.parseDouble(input[1]);
            double yCoord = Double.parseDouble(input[2]);
            double radius = Double.parseDouble(input[3]);
            Circle circle = new Circle (xCoord, yCoord, radius);
            io.showAddedShape (nrOfShapes, circle);
            shapes[nrOfShapes++] = circle;
        }
        else {
            io.showArrayFullMessage();
        }
    }
    
    private void addRectangle (String input[]) {
        if (nrOfShapes < MAX_SHAPES) {
            double xCoord = Double.parseDouble(input[1]);
            double yCoord = Double.parseDouble(input[2]);
            double height = Double.parseDouble(input[3]);
            double width = Double.parseDouble(input[4]);
            Rectangle rect = new Rectangle (xCoord, yCoord, height, width);
            io.showAddedShape (nrOfShapes, rect);
            shapes[nrOfShapes++] = rect;
        }
        else {
            io.showArrayFullMessage();
        }
    }
    
    private void moveShape (String input[]) {
        int index = Integer.parseInt(input[1]);
        double dx = Double.parseDouble(input[2]);
        double dy = Double.parseDouble(input[3]);
        if (index >= 0 && index < nrOfShapes) {
            shapes[index].moveGeometric(dx, dy);
            io.showShape(index, shapes[index]);
        }
    }
    
    private void removeShape (String input[]) {
        int index = Integer.parseInt(input[1]);
        if (index >= 0) {
            for (int i = index; i < nrOfShapes; i++) {
                    shapes[i] = shapes[i+1];
                }
            nrOfShapes--;
        }
        showShapes();
    }
    
    private void sortShapes (String input[]) {
        if (input.length == 1) {
            Arrays.sort (shapes, 0, nrOfShapes);
        }
        else if (input[1].charAt(0) == 'x') {
            Arrays.sort (shapes, 0, nrOfShapes, xComp);
        }
        else if (input[1].charAt(0) == 'y') {
            Arrays.sort (shapes, 0, nrOfShapes, yComp);
        }
        else {
            Arrays.sort (shapes, 0, nrOfShapes);
        }
        showShapes();
    }
}
