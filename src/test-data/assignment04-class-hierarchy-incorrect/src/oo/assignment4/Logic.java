package oo.assignment4;

import java.util.*;

public class Logic {
    private List<Question> questions;
    private List<Question> answeredWrong;
    
    private int totalScore;
	
	IO inOut = new IO();
    
  
    public Logic(){
        this.questions = new LinkedList<>();
        this.answeredWrong = new LinkedList<>();
        this.totalScore = 0;
    }

    private void storeQuestions() {
    	questions.add(new OpenQuestion("What is the  complexity  of a binary  search?", "O(log N)", 2));
    	questions.add(new OpenQuestion("What is the  minimal  amount of  constructors  for a Java  class?", "0"));
    	questions.add(new MultipleChoiceQuestion("What is the  best  achievable  complexity  of in situ  sorting?", new  String [] {"O(N^2)", "O(N log N)", "O(N)", "O(log N)"}, 1, 4));
        questions.add(new ThisThatQuestion("Is Java the same as Javascript?", "Yes", "No", "No"));
    }
    
    public void startQuiz() {
        storeQuestions();
    	inOut.start();	
    }
    
    public void runQuiz(){
        
        for (Question q: questions){
            inOut.printQuestion(q);
            String answer = inOut.askAnswer();
            
            if (q.isCorrect(answer)){
                inOut.printCorrect();
                totalScore += q.score;
            }
            else{
            	answeredWrong.add(q);
                inOut.printIncorrect();
            }
        }
        inOut.printScore1(totalScore);
        if (answeredWrong.size() > 0) {
        	while (answeredWrong.size() != 0) {
        		inOut.printQuestion(answeredWrong.get(0));
        		String answer = inOut.askAnswer();
            
        		if (answeredWrong.get(0).isCorrect(answer)){
        			inOut.printCorrect();
        			totalScore += answeredWrong.get(0).score;
        		}
        		else{
        			answeredWrong.add(answeredWrong.get(0));
        			inOut.printIncorrect();
        		}
        		answeredWrong.remove(0);
        	}
        	inOut.printScore2(totalScore);
        }
    }
}
            
            
        
        
  
