import java.util.Scanner;

public class Main {

    // static final is constant
    public static final int NUM_FOOBARS = 3;

    public static void main(String[] args) {
       System.out.print ("Enter a group size.\n");

       // invalid variable name
       Scanner Scan = new Scanner(System.in);
       int group_size = Scan.nextInt();

       Group group = new Group(group_size);
       Student student = null;

       do {
           System.out.println("Please enter a student.");
           int student_number = Scan.nextInt();
           String firstname = Scan.next();
           String surname = Scan.next();
           student = new Student(student_number, firstname, surname);
        } while (group.AddMember(student) < group_size);

       System.out.print (group.toString());

       int s_number = 0;

       while (s_number >= 0) {
          System.out.print ("Student number and new given/family name.\n");
          s_number = Scan.nextInt();
          if (s_number < 0) break;
          String firstname = Scan.next();
          String surname = Scan.next();
          if (!group.changeMemberName(s_number, firstname, surname)){
             System.out.print ("Student number does not exist.");
         } else {
            System.out.print (group.toString());
         }
      }
      System.out.print ("Bye!");
    }
}
