public class Group {
    private int size;
    private int Currentindex = 0;
    private Student[] members;

    public Group(int size) {
        this.size = size;
        this.members = new Student[size];
    }

    // invalid parameter name
    public int AddMember(Student member) {
        if (this.Currentindex + 1 <= size) {
            this.members[this.Currentindex++] = member;
            return this.Currentindex;
        } else {
            return -1;
        }
    }

    public int hasMember(int student_number) {
        for (int i = 0; i < this.size; i++) {
            if (this.members[i].getStudent_number() == student_number) {
                return i;
            }
        }
        return -1;
    }

    public boolean changeMemberName(int student_number, String New_firstname, String new_surname) {
        int index = this.hasMember(student_number);
        if (index >= 0) {
            this.members[index].setFirst_Name(New_firstname);
            this.members[index].setSur_name(new_surname);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("The group now contains:\n");
        for (int i = 0; i < Currentindex; i++) {
            sb.append(this.members[i].toString());
        }
        return sb.toString();
    }
}

