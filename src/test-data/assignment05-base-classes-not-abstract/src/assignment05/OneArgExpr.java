package assignment05;

public class OneArgExpr extends Expression {

	private final Expression x;

	public OneArgExpr(Expression x) {
		this.x = x;
	}

	public Expression getX() {
		return x;
	}
}
