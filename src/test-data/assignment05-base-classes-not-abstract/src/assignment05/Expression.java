package assignment05;

import java.util.HashMap;
import java.util.Map;

public class Expression {

	public String toString() { return null; }

	public double eval(Map<String, Double> env) { return 0.0; }

	public Expression partialEval() { return null; }

	protected static final Map<String, Double> EMPTY_ENV = new HashMap<>();
}
