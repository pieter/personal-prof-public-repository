package assignment05;

import java.util.Map;

public class Negate extends OneArgExpr {

	public Negate(Expression x) {
		super(x);
	}

	@Override
	public String toString() {
		return "-" + getX();
	}

	@Override
	public double eval(Map<String, Double> env) {
		return -getX().eval(env);
	}

	@Override
	public Expression partialEval() {
		Expression first = getX().partialEval();
		if (first instanceof Constant) {
			return new Constant(-(first.eval(EMPTY_ENV)));
		}

		return new Negate(first);
	}
}
