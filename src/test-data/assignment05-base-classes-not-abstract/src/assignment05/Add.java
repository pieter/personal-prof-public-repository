package assignment05;

import java.util.Map;

public class Add extends TwoArgExpr {

	public Add(Expression x, Expression y) {
		super(x, y);
	}

	@Override
	public String toString() {
		return "(" + getX() + "+" + getY() + ")";
	}

	@Override
	public double eval(Map<String, Double> env) {
		return getX().eval(env) + getY().eval(env);
	}

	@Override
	public Expression partialEval() {
		Expression first = getX().partialEval();
		Expression second = getY().partialEval();
		if (first instanceof Constant && second instanceof Constant) {
			return new Constant(first.eval(EMPTY_ENV) + second.eval(EMPTY_ENV));
		}
		if (first instanceof Constant && first.eval(EMPTY_ENV) == 0) {
			return second;
		}
		if (second instanceof Constant && second.eval(EMPTY_ENV) == 0) {
			return first;
		}

		return new Add(first, second);
	}
}
