package assignment05;

import java.util.Map;

public class Constant extends NoArgExpr {

	Double value;

	public Constant(Double cons) {
		this.value = cons;
	}

	@Override
	public String toString() {
		return "" + value;
	}

	@Override
	public double eval(Map<String, Double> env) {
		return value;
	}

	@Override
	public Expression partialEval() {
		return new Constant(value);
	}
}
