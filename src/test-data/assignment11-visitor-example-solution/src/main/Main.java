package main;

import ast.BinaryOperator;

import ast.Atom;
import ast.Constant;
import ast.BinOp;
import static ast.BinOp.*;
import static ast.FormulaFactory.*;
import ast.Formula;
import ast.Not;
import visitor.EvaluateVisitor;
import visitor.PrintVisitor;

import java.util.HashMap;

public class Main {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		Formula formula = form3();
		printFormula(formula);

		HashMap<String, Boolean> env = new HashMap<>();
		env.put("P", false);
		env.put("Q", true);
		evalFormula(formula, env);

		printFormula(form3());
		printFormula(form4());

		evalFormula(form3(), env);
		evalFormula(form4(), env);
	}

	private static void evalFormula(Formula form, HashMap<String, Boolean> env) {
		boolean result = form.accept(new EvaluateVisitor(env), null);
		System.out.println(result);
	}

	private static void printFormula(Formula form) {
		PrintVisitor pfv = new PrintVisitor();
		form.accept(pfv, MAX_PRIO);
		System.out.println();
	}

	private static Formula form1() {
		return implies(and(not(atom("Q")), implies(atom("P"), atom("Q"))),
				not(atom("P")));
	}

	private static Formula form2() {
		return new BinaryOperator(
				new BinaryOperator(new Not(TRUE),
						new BinaryOperator(new Atom("P"), new Atom("Q"), IMPLIES), AND),
				new Not(new Atom("P")), IMPLIES);
	}

	private static Formula form3() {
		return new Not(new BinaryOperator(new Atom("P"), new Atom("Q"), BinOp.IMPLIES));
	}

	private static Formula form4() {
		return new BinaryOperator(new Not(new Atom("P")), new Atom("Q"), BinOp.IMPLIES);
	}

}
