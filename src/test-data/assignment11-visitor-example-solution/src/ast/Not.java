package ast;

import visitor.FormulaVisitor;

public class Not implements Formula {
	private Formula operand;

	public Not(Formula oper) {
		this.operand = oper;
	}

	public Formula getOperand() {
		return operand;
	}

	public <R, A> R accept(FormulaVisitor<R, A> v, A arg) {
		return v.visit(this, arg);
	}
}
