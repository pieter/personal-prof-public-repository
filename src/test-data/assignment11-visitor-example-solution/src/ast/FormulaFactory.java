package ast;

import visitor.EvaluateVisitor;
import visitor.PrintVisitor;
import static ast.BinOp.*;

import java.util.Map;

public class FormulaFactory {

	public static Formula atom(String atomId) {
		return new Atom(atomId);
	}

	public static Formula and(Formula leftOp, Formula rightOp) {
		return new BinaryOperator(leftOp, rightOp, BinOp.AND);
	}

	public static Formula or(Formula leftOp, Formula rightOp) {
		return new BinaryOperator(leftOp, rightOp, BinOp.OR);
	}

	public static Formula implies(Formula leftOp, Formula rightOp) {
		return new BinaryOperator(leftOp, rightOp, BinOp.IMPLIES);
	}

	public static Formula not(Formula notOp) {
		return new Not(notOp);
	}

	public static final Formula TRUE = Constant.True;

	public static final Formula FALSE = Constant.False;

	public static String prettyPrint(Formula f) {
		PrintVisitor v = new PrintVisitor();
		f.accept(v, MAX_PRIO);
		return v.getResult();
	}

	public static Boolean evaluate(Formula f, Map<String,Boolean> env) {
		EvaluateVisitor v = new EvaluateVisitor(env);
		return f.accept(v, null);
	}
}
