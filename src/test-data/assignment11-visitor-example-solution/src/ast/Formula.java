package ast;

import visitor.FormulaVisitor;

public interface Formula {
	<R, A> R accept(FormulaVisitor<R, A> visitor, A arg);
}
