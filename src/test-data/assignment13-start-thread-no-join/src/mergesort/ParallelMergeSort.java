/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mergesort;

import java.util.Arrays;

public class ParallelMergeSort extends MergeSort {
	private static final int BOUND = 5000;

	/**
	 * sort the given array
	 * 
	 * @param array
	 */
	public static void sort(int[] array) {
		Sorter s = new Sorter(array);
		s.run();
	}

	private static class Sorter implements Runnable {
		private final int[] array;

		public Sorter(int[] array) {
			this.array = array;
		}

		@Override
		public void run() {
			if (array.length > BOUND) {
				int[] firstHalf = Arrays.copyOf(array, array.length / 2);
				Sorter s1 = new Sorter(firstHalf);
				Thread t1 = new Thread(s1);
				t1.start();
				int[] secondHalf = Arrays.copyOfRange(array, array.length / 2, array.length);
				Sorter s2 = new Sorter(secondHalf);
				s2.run();
				merge(firstHalf, secondHalf, array);
			} else {
				MergeSort.sort(array);
			}
		}
	}
}
