package filefinder;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class FileFinderTest {

	@Test
	public void findNeedleInHaystack() throws IOException {
		String goal = "needle.txt";
		String root = "haystack";
		FileFinder ff = new FileFinder(root);
		ff.findFile(goal);
	}

}
