package piechartsimple;

import javafx.application.Application;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class PieChartSimple extends Application {
    private GridPane root = new GridPane();

  @Override
  public void start(Stage primaryStage) {
    root.setAlignment(Pos.CENTER);
    root.setHgap(20);
    root.setVgap(10);
    IntegerProperty sum = new SimpleIntegerProperty(0);
    IntegerProperty previousSum = new SimpleIntegerProperty(0);
    for (int i = 0; i < 4; i += 1) {
      previousSum = add(previousSum, sum, i);
    }
    sum.bind(previousSum);
    Scene scene = new Scene(root, 500, 300);
    primaryStage.setTitle("Pie");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  private IntegerProperty add (IntegerProperty previousSum, IntegerProperty sum, int row) {
    IntegerProperty inputProperty = new SimpleIntegerProperty(1);
    IntegerProperty newSum = new SimpleIntegerProperty(0);
    newSum.bind(previousSum.add(inputProperty));
    DoubleProperty outputProperty = new SimpleDoubleProperty(0);
    outputProperty.bind(inputProperty.add(0f).divide(sum));
    addText(inputProperty, row);
    addOutput(outputProperty, row);
    return newSum;
  }

  private void addText(IntegerProperty value, int row) {
    TextField textField = new TextField();
    textField.textProperty().addListener(
        (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if ( ! newValue.matches( "[1-9]\\d{0,3}" ) ) {
                textField.setText(oldValue);
            }
        });
    textField.textProperty().bindBidirectional(value, new NumberStringConverter());
    root.add(textField, 0, row);
  }

  private void addOutput(DoubleProperty outputProperty, int row) {
      Label label = new Label();
      label.textProperty().bind(outputProperty.asString("%.4f"));
      root.add(label, 1, row);
  }

  public static void main(String[] args) {
    launch(args);
  }
}
