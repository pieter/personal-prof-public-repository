package assignment07;

import java.io.Writer;

public interface QuadTreeNode {

	public void fillBitmap(int x, int y, int width, Bitmap bitmap);

	public void writeNode(Writer out);

	public boolean sameLeaf(QuadTreeNode other_node);

}
