package assignment07;

import java.io.IOException;
import java.io.Writer;

public class GreyNode implements QuadTreeNode {

	private final QuadTreeNode[] children;

	public GreyNode(QuadTreeNode[] children) {
		this.children = children;
	}

	@Override
	public boolean sameLeaf(QuadTreeNode node) {
		return false;
	}

	@Override
	public void fillBitmap(int x, int y, int width, Bitmap bitmap) {
		width /= 2;
		children[0].fillBitmap(x, y, width, bitmap);
		children[1].fillBitmap(x + width, y, width, bitmap);
		children[2].fillBitmap(x + width, y + width, width, bitmap);
		children[3].fillBitmap(x, y + width, width, bitmap);
	}

	@Override
	public void writeNode(Writer out) {
		try {
			out.append('1');
		} catch (IOException ex) {
		}
		for (QuadTreeNode child : children) {
			child.writeNode(out);
		}
	}

}
