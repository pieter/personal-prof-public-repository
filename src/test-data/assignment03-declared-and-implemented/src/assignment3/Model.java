/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import java.util.Comparator;
import java.util.Scanner;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public class Model {

    Controller controller;
    UserInterface ui;
    ObjectList objects;
    Scanner scanner;
    
    Comparator<Geometric> x = (Geometric o1, Geometric o2) -> {
        if (o1.leftBorder() > o2.leftBorder()) {
            return 1;
        } else if (o1.leftBorder() < o2.leftBorder()) {
            return -1;
        } else {
            return 0;
        }
    };
    
    Comparator<Geometric> y = (Geometric o1, Geometric o2) -> {
        if (o1.bottomBorder() > o2.bottomBorder()) {
            return 1;
        } else if (o1.bottomBorder() < o2.bottomBorder()) {
            return -1;
        } else {
            return 0;
        }
    };

    public Model(Scanner scanner, Controller controller, UserInterface ui, ObjectList objects) {
        this.controller = controller;
        this.ui = ui;
        this.objects = objects;
        this.scanner=scanner;
    }

    public void execute() {
        boolean quit=false;
        while (!quit){
        String input = controller.input(scanner, ui);
        switch (input) {
            case "quit":
                quit= true;
                break;
            case "show":
                show();
                break;
            case "circle":
                circle();
                break;
            case "rectangle":
                rectangle();
                break;
            case "move":
                move();
                break;
            case "remove":
                remove();
                break;
            case "sort":
                sort(scanner);
                break;
            default:
                ui.invalidInput(input);
        }
        }
    }

    public boolean quit() {
        return true;
    }

    public void show() {
        ui.printObjects(objects);
    }

    public void circle() {
        Circle circle = new Circle(scanner.nextDouble(),scanner.nextDouble(),scanner.nextDouble());
        objects.add(circle);
    }

    public void rectangle() {
        Rectangle rectangle= new Rectangle(scanner.nextDouble(),scanner.nextDouble(),scanner.nextDouble(),scanner.nextDouble());
        objects.add(rectangle);
    }

    public void move() {
        objects.get(scanner.nextInt()-1).move(scanner.nextDouble(),scanner.nextDouble());
    }

    public void remove() {
        objects.remove(scanner.nextInt());
    }

    public void sort(Scanner scanner) {
        String input="";
            input = scanner.nextLine();
            switch (input){
                    case " x":
                        objects.sortList(x);
                        break;
                    case " y":
                        objects.sortList(y);
                    default:
                        objects.sortList();
                            break;
            }
    }

}
