/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public class UserInterface {
    public void askForInput(){
        System.out.println("Your order, sir?");
    }

    void invalidInput(String input) {
        System.out.println("I'm afraid i cant do'"+ input+"'.");
    }

    void printObjects(ObjectList objects) {
        Geometric object;
        if (objects.getObjectCount()==0)
            System.out.println("No objects to display");
        for (int i=0;i<objects.getObjectCount();i++)
        {
            object=objects.get(i);
            System.out.println(i+1+": "+object.toString());
        }
    }
}
