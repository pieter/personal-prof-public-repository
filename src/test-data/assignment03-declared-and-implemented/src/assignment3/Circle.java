/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public class Circle implements Geometric {
    private double radius;
    private double xCoord;
    private double yCoord;

    public Circle(double xCoord, double yCoord,double radius) {
        this.radius = radius;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    @Override
    public double topBorder() {
        return this.yCoord + this.radius;
    }

    @Override
    public double bottomBorder() {
        return this.yCoord - this.radius;
    }

    @Override
    public double leftBorder() {
        return this.xCoord - this.radius;
    }

    @Override
    public double rightBorder() {
        return this.xCoord + this.radius;
    }

    @Override
    public double area() {
        return this.radius * this.radius * Math.PI;
    }

    @Override
    public int compareTo(Geometric thing) {
        if (this.area() > thing.area()) {
            return 1;
        } else if (this.area() < thing.area()) {
            return -1;
        } else {
            return 0;
        }
    }
    
    @Override
    public void move(double x, double y) {
        this.xCoord += x;
        this.yCoord += y;
    }
    
    public String toString() {
        return "Circle (x,y,radius): " + " " + this.xCoord + " " + this.yCoord + " " + this.radius ;
    }
}
