/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import java.util.Scanner;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
class Controller {
    public String input(Scanner scanner,UserInterface ui){
        ui.askForInput();
        while (true){
            if (scanner.hasNext())
                return scanner.next();
        }
    }
    
}
