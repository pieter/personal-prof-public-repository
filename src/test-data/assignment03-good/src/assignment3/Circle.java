package assignment3;

public class Circle implements Geometric {
    private static final double PI = 3.14159265359;
    private double xCoord;
    private double yCoord;
    private double radius;
    
    public Circle (double x, double y, double r) {
        this.xCoord = x;
        this.yCoord = y;
        this.radius = r;
    }
    
    @Override
    public double getLeftBorder() {
        return xCoord - radius;
    }
    
    @Override
    public double getRightBorder() {
        return xCoord + radius;
    }
    
    @Override
    public double getBottomBorder() {
        return yCoord - radius;
    }
    
    @Override
    public double getTopBorder() {
        return yCoord + radius;
    }
    
    @Override
    public double getArea() {
        return PI * radius * radius;
    }
    
    @Override
    public void moveGeometric (double dx, double dy) {
        this.xCoord += dx;
        this.yCoord += dy;
    }
    
    @Override
    public String toString() {
        return ("Circle with center point at (" + xCoord + ", " + yCoord + "), with radius " + radius);
    }
}
