package assignment3;
import java.util.Comparator;

public class YComparator implements Comparator<Geometric> {

    @Override
    public int compare (Geometric shape1, Geometric shape2) {
        double y1 = shape1.getBottomBorder();
        double y2 = shape2.getBottomBorder();
        if (y1 < y2) { 
            return -1;
        }
        else if (y1 == y2) {
            return 0;
        }
        else if (y1 > y2) {
            return 1;
        }
        return 0;
    }
}
