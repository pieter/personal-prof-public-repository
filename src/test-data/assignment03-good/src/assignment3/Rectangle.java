package assignment3;

public class Rectangle implements Geometric {
    private double xCoord;
    private double yCoord;
    private double height;
    private double width;
    
    public Rectangle (double x, double y, double h, double w) {
        this.xCoord = x;
        this.yCoord = y;
        this.height = h;
        this.width = w;
    }
    
    @Override
    public double getLeftBorder() {
        return xCoord;
    }
    
    @Override
    public double getRightBorder() {
        return xCoord + width;
    }
    
    @Override
    public double getBottomBorder() {
        return yCoord;
    }
    
    @Override
    public double getTopBorder() {
        return yCoord + height;
    }
    
    @Override
    public double getArea() {
        return width * height;
    }
    
    @Override
    public void moveGeometric (double dx, double dy) {
        this.xCoord += dx;
        this.yCoord += dy;
    }
    
    @Override
    public String toString() {
        return ("Rectangle with bottom left corner at (" + xCoord + ", " + yCoord + "), with height " + height + " and width " + width);
    }
}
