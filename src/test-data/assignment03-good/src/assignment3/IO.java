package assignment3;
import java.util.Scanner;

public class IO {
    private Scanner scan;
    
    public IO () {
        this.scan  = new Scanner(System.in);
    }
    
    public String getInput() {
        System.out.println ("Give a command:");
        return scan.nextLine();
    }
    
    public void showStopMessage() {
        System.out.println ("Program terminated.");
    }
    
    public void showShape (int shapeNr, Geometric shape) {
        System.out.println ("Index " + shapeNr + ": " + shape.toString());
    }
    
    public void showAddedShape (int shapeNr, Geometric shape) {
        System.out.print ("Added => ");
        showShape (shapeNr, shape);
    }
    
    public void showUnknownCommandMessage() {
        System.out.println ("Unknown command. Make sure you typed the command correctly.");
    }
    
    public void showArrayFullMessage() {
        System.out.println ("Array of shapes is full, please remove a shape first.");
    }
}
