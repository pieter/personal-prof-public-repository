package polynomials;

/**
 *
 * @author Sjaak Smetsers
 */
public class Main {

    public static void main(String[] args) {
        tests();
    }
    private static void tests(){
        Polynomial p1 = new Polynomial("3.0 1 2 3");
        Polynomial p2 = new Polynomial("4 0 5 3");
        Polynomial p3 = new Polynomial("4 0 -5 3");
        Polynomial p4 = new Polynomial("6 1");
        Polynomial p5 = new Polynomial(p2);
        Polynomial p6 = new Polynomial(p1);
        Polynomial p7 = new Polynomial(p1);
        Polynomial p8 = new Polynomial("-4 0 -2 2 1 3");
        Polynomial p9 = new Polynomial("-3 0 1 1");
//        Polynomial p10 = new Polynomial("-3 2 1 1");

        System.out.println("p1:" + p1);
        System.out.println("p2:" + p2);
        System.out.println("p3:" + p3);
        System.out.println("p4:" + p4);
        System.out.println("p8:" + p8);
        System.out.println("p9:" + p9);

//        System.out.println(p2.equals(p5));

        p1.plus(p1);

        p2.times(p3);
        System.out.println("p2:" + p2);

        System.out.println("p1:" + p1);
        System.out.println("p2:" + p2);
        p6.times(p6);
        p7.times(p1);
        System.out.println("p6:" + p6);
        System.out.println("p7:" + p7);
        Polynomial pr = p8.divide(p9);
        System.out.println("p8:" + p8);
        System.out.println("pr:" + pr);
        Polynomial pr5 = p5.divide(p5);
        System.out.println("p5:" + p5);
        System.out.println("pr5:" + pr5);
        
        
    }
    
    private static void testDivide(){
        Polynomial p1 = new Polynomial("-1 0 4 1 2 3");
        Polynomial p2 = new Polynomial("-2 0 2 1");
        System.out.println("p1:" + p1);
        System.out.println("p2:" + p2);
        Polynomial pd = p1.divide(p2);
        System.out.println("p1:" + p1);
        System.out.println("pd:" + pd);
    } 

}

