package supermarket;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

public class Customer implements Callable<Integer> {

	private final Store store;
	private final int customerNumber;
	private final int numberOfItems;
	private final Random generator;

	public Customer(int number, Store store) {
		this.generator = new Random();
		this.store = store;
		customerNumber = number;
		numberOfItems = generator.nextInt(20) + 1;
		System.out.println("Customer " + customerNumber + " has " + numberOfItems + " items.");
	}

	@Override
	public Integer call() {
		try {
			List<Item> items = store.getItems(numberOfItems);
			int checkoutNr = generator.nextInt(Store.NUMBER_OF_CHECKOUTS);
			Register checkout = store.claimRegister(checkoutNr);
			
			synchronized(this) {
				System.out.println("hello world");
			}

			System.out.println("Customer " + customerNumber + " starts putting items on belt");
			items.stream().forEach(checkout::putOnBelt);
			checkout.putOnBelt(null);
			System.out.println("Customer " + customerNumber + " finished putting items on belt");

			while (checkout.removeFromBin() != null) {
			}
			System.out.println("Customer " + customerNumber + " finished taking items");

			store.freeRegister(checkoutNr);
			return numberOfItems;
		} catch (InterruptedException e) {
			return numberOfItems;
		}
	}
}
