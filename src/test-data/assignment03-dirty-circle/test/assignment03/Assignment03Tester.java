package assignment03;

public class Assignment03Tester {

	GeometricArray objects;

	public Assignment03Tester() {
		objects = new GeometricArray();
	}

	public void createCircle(double x, double y, double r) {
		objects.addCircle(new Circle(x, y, r));
	}

	public void createRectangle(double x, double y, double width, double height) {
		objects.addRectangle(new Rectangle(x, y, width, height));
	}

	public double topBorder(int index) {
		return objects.getElement(index).topBorder();
	}

	public double rightBorder(int index) {
		return objects.getElement(index).rightBorder();
	}

	public double bottomBorder(int index) {
		return objects.getElement(index).bottomBorder();
	}

	public double leftBorder(int index) {
		return objects.getElement(index).leftBorder();
	}

	public double area(int index) {
		return objects.getElement(index).areaObject();
	}

	public void move(int index, double dx, double dy) {
		objects.getElement(index).moveObject(dx, dy);
	}

	public void sortByArea() {
		objects.sortByArea();
	}

	public void sortByX() {
		objects.sortByX();
	}

	public void sortByY() {
		objects.sortByY();
	}
}
