public class student {
    private int student_number;
    private String first_name;
    private String sur_name;

    public Student (int student_number, String firstname, String surname){
        this.student_number = student_number;
        this.first_name = firstname;
        this.sur_name = surname;
    }

    public int getStudent_number() {
        return student_number;
    }

    public String getFirst_Name() {
        return first_name;
    }

    public void setFirst_Name(String name) {
        this.first_name = name;
    }

    public String getSur_name() {
        return sur_name;
    }

    public void setSur_name(String sur_name) {
        this.sur_name = sur_name;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(first_name);
        sb.append(" " + sur_name);
        sb.append(", s" + student_number + "\n");

        return sb.toString();
    }
}
