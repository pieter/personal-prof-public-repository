/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mergesort;

import java.util.Arrays;

public class ParallelMergeSort extends MergeSort implements Runnable {
	private static final int BOUND = 10000;
	private final int[] array;

	/**
	 * sort the given array
	 * 
	 * @param array
	 */
	public static void sort(int[] array) {
		ParallelMergeSort s = new ParallelMergeSort(array);
		s.run();
	}

	private ParallelMergeSort(int[] array) {
		this.array = array;
	}

	@Override
	public void run() {
		if (array.length > BOUND) {
			int[] firstHalf = Arrays.copyOf(array, array.length / 2);
			ParallelMergeSort s1 = new ParallelMergeSort(firstHalf);
			Thread t1 = new Thread(s1);
			t1.start();
			int[] secondHalf = Arrays.copyOfRange(array, array.length / 2, array.length);
			ParallelMergeSort s2 = new ParallelMergeSort(secondHalf);
			s2.run();
			try {
				t1.join();
			} catch (InterruptedException e) {
				System.out.println("thread was interrupted.");
				e.printStackTrace();
			}
			MergeSort.merge(firstHalf, secondHalf, array);
		} else {
			MergeSort.sort(array);
		}
	}
}
