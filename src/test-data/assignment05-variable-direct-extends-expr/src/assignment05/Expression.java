package assignment05;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public abstract interface Expression {

	public abstract String toString();

	public abstract double eval(Map<String, Double> env);

	public abstract Expression partialEval();
}
