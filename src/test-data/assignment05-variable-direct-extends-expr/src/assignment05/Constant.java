package assignment05;

import java.util.Map;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public class Constant implements Expression {

	Double value;

	public Constant(Double cons) {
		this.value = cons;
	}

	@Override
	public String toString() {
		return "" + value;
	}

	@Override
	public double eval(Map<String, Double> env) {
		return value;
	}

	@Override
	public Expression partialEval() {
		return new Constant(value);
	}
}
