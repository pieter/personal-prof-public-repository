package assignment05;

/**
 *
 * @author Dylan Stoutjesdijk (s1000303)
 * @author Misja Zippelius (s1019965)
 */
public abstract class OneArgExpr implements Expression {

	private final Expression x;

	public OneArgExpr(Expression x) {
		this.x = x;
	}

	public Expression getX() {
		return x;
	}
}
