package exercise3oo;

public class Rectangle implements Geometric, Comparable<Geometric> {

    private double xLowerLeft, yLowerLeft, width, height;

    public Rectangle(double x, double y, double height, double width) {
        this.xLowerLeft = x;
        this.yLowerLeft = y;
        this.height = height;
        this.width = width;
    }

    @Override
    public double leftBorder() {
        return xLowerLeft;
    }

    @Override
    public double rightBorder() {
        return xLowerLeft + width;
    }

    @Override
    public double bottomBorder() {
        return yLowerLeft;
    }

    @Override
    public double topBorder() {
        return yLowerLeft + height;
    }

    @Override
    public double areaObject() {
        return height * width;
    }

    @Override
    public void moveObject(double dx, double dy) {
        this.xLowerLeft = xLowerLeft + dx;
        this.yLowerLeft = yLowerLeft + dy;
    }

    @Override
    public String toString() {
        String dx = String.valueOf(xLowerLeft);
        String dy = String.valueOf(yLowerLeft);
        String dwidth = String.valueOf(width);
        String dheight = String.valueOf(height);
        return "rectangle: " + "x coordinate lower left corner: " + dx + ", y coordinate lower left corner: " + dy + ", height rectangle: " + dheight + ", width rectangle: " + dwidth;

    }

    public double getArea() {
        return areaObject();
    }
}
