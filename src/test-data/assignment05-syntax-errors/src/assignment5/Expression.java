/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;
import java.util.Map;

/**
 *
 * @author lucvr
 */
public interface Expression {
@Override public String toString();
public double eval(env:Map<String, Double>);
public Expression optimize();
public boolean isConstant(); 
public double getValue();
}

