  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;
import java.util.Map;

/**
 *
 * @author lucvr
 */
public class Addition extends doubleArgument{
    public Addition(Expression x, Expression y) {
        super(x, y);
   }
@Override
    public String toString() {
        return "(" + super.xToString() + " + " + super.yToString() + ")";
    }

    @Override
    public double eval(env:Map<String, Double> map) {
        return super.getX(map) + super.getY(map);
    }
    

    @Override
    public Expression optimize() {
        if (super.optimizeX().isConstant() && super.optimizeY().isConstant()) {
            return new Constant(super.optimizeX().getValue() + super.optimizeY().getValue());
        }
        else if (super.optimizeX().getValue() == 0) {
            return super.optimizeY();
        }
        else if (super.optimizeY().getValue() == 0) {
            return super.optimizeX();
        }
        else {
            return new Addition(this.optimizeX(), this.optimizeY());
        }
    }    

    @Override
    public boolean isConstant() {
        return false;
    }

    @Override
    public double getValue() {
        return -1;
    }
}

