/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;
import java.util.Map;
/**
 *
 * @author lucvr
 */
public class Constant extends NoArgument {
    private double Value;
    public Constant(double GivenValue){
        Value = GivenValue;
    }
    @Override
    public double getValue(){
        return Value;
    }
    @Override
    public String toString(){
    return "" + Value;
}
    @Override
    public double eval(Map<String, Double> map) {
        return Value;
    }
    @Override
    public Expression optimize() {
        return this;
    }
    @Override
    public boolean isConstant() {
        return true;
    }
}
