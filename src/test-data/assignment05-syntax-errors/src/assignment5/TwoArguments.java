/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;
import java.util.Map;

/**
 *
 * @author lucvr
 */
public abstract class TwoArguments implements Expression{
private final Expression x;
private final Expression y;

public TwoArguments (Expression x, Expression y){
this.x = x;
this.y = y;
}
public double getX(Map<String, Double> map) {
return x.eval(map);
    }
public String xToString() {
 return x.toString();
}

public String yToString() {
   return y.toString();
}

public Expression optimizeX() {
    return this.x.optimize();
}

public Expression optimizeY() {
    return this.y.optimize();
}

}
