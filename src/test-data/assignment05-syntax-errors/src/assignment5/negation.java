/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

/**
 *
 * @author lucvr
 */
public class negation extends OneArgument{
    public Negate(Expression exp){
        super(exp);
    }
    @Override
    public String toString(){
        return "-" + super.toString();
    }
    @Override
    public double eval (env:Map<String, Double> map) {
        return super.eval(map) * -1;
    }
    @Override
    public Expression optimize(){
        if (super.optimize().isConstant()){
            return new Constant (super.optimize().getValue() * -1)
        }
        else{
            return new negation(super.optimize());
        }
    }
}
