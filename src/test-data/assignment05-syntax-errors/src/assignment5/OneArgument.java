/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

/**
 *
 * @author lucvr
 */
import java.util.Map;
public abstract class OneArgument implements Expression {
    private final Expression exp;
    
public OneArgument (Expression exp) {
     this.exp = exp;
}
        }@Override
        public String toString() {
return "(" + exp.toString() + ")";
}
@Override
public double eval(Map<String, Double> map) {
return exp.eval(map);
}
@override
public boolean isConstant() {
return exp.isConstant();
}
@Override
public double getValue() {
return exp.getValue();
}
@Override
public Expression optimize() {
if (this.exp.optimize().isConstant()) {
return new Constant (exp.optimize().getValue());
}
else{
return this.exp.optimize();
}
}