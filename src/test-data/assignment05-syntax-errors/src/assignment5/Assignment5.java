/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;
import java.util.Map ;
import java.util.HashMap ;

/**
 *
 * @author lucvr
 */
public class Assignment5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
Map<String , Double> store = new HashMap < >() ;
store.put ("pi", 3.1415) ;
store.put ("x", 5d) ;
store.put ("y", 3d) ;
store.put ("e", 2.71);
Expression e1 = Addition(Multi(con(2.), Constant(3.)), Variable("x"));
Expression e2 = Multi(con(3.), Variable("a"));
System.out.println("E1: " + e1.toString() + " - Result: " + e1.eval(map) + " - Optimized: " + e1.optimize().toString());
System.out.println("E2: " + e2.toString() + " - Result: " + e2.eval(map) + " - Optimized: " + e2.optimize().toString());
    }
    

    
}
