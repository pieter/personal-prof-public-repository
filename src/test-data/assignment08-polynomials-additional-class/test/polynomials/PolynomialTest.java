package polynomials;

import java.util.InputMismatchException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Sjaak Smetsers
 */
public class PolynomialTest {

    public PolynomialTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(expected = InputMismatchException.class)
    public void testConstructor() {
        Polynomial p10 = new Polynomial("-3 2 1 1");
    }

    @Test(expected = InputMismatchException.class)
    public void testZero() {
        Polynomial p10 = new Polynomial("0 0");
    }

    /**
     * Test of plus method, of class Polynomial.
     */
    @Test
    public void testPlus() {
        Polynomial p1 = new Polynomial("5 0 3 1 7 2 5 3");
        Polynomial p2 = new Polynomial("1 0 1 1 1 2     8 4");
        Polynomial ex = new Polynomial("6 0 4 1 8 2 5 3 8 4");
        p1.plus(p2);
        assertEquals(p1, ex);
    }

    @Test
    public void testPlusTermVanishes() {
        Polynomial p1 = new Polynomial("1 0  2 1 3 2");
        Polynomial p2 = new Polynomial("1 0 -2 1 3 2");
        Polynomial ex = new Polynomial("2 0      6 2");
        p1.plus(p2);
        assertEquals(p1, ex);
    }

    /**
     * Test of minus method, of class Polynomial.
     */
    @Test
    public void testMinus() {
        Polynomial p1 = new Polynomial("5 0 3 1 7 2 5 3");
        Polynomial p2 = new Polynomial("1 0 1 1 1 2     8 4");
        Polynomial ex = new Polynomial("4 0 2 1 6 2 5 3 -8 4");
        p1.minus(p2);
        assertEquals(p1, ex);
    }

    @Test
    public void testMinusTermVanishes() {
        Polynomial p1 = new Polynomial("1 0  2 1 3 2");
        Polynomial p2 = new Polynomial("1 0 -2 1 3 2");
        Polynomial ex = new Polynomial("     4 1");
        p1.minus(p2);
        assertEquals(p1, ex);
    }

    @Test
    public void testPlusAssociative() {
        Polynomial a = new Polynomial("1 0 2 1 3 2");
        Polynomial b = new Polynomial("2 0 3 1 4 2");
        Polynomial c = new Polynomial("3 0 4 1 5 2");
        Polynomial a2 = new Polynomial(a);
        Polynomial b2 = new Polynomial(b);
        Polynomial c2 = new Polynomial(c);
        a.plus(b);
        a.plus(c);
        b2.plus(c2);
        a2.plus(b2);
        assertEquals("(a + b) + c == a + (b + c)", a, a2);
    }

    @Test
    public void testPlusCommutative() {
        Polynomial a = new Polynomial("1 0 2 1 3 2");
        Polynomial b = new Polynomial("2 0 3 1 4 2");
        Polynomial a2 = new Polynomial(a);
        Polynomial b2 = new Polynomial(b);
        a.plus(b);
        b2.plus(a2);
        assertEquals("a + b = b + a", a, b2);
    }

    /**
     * Test of times method, of class Polynomial.
     */
    @Test
    public void testTimes() {
        Polynomial p1 = new Polynomial("2 0 1 1");
        Polynomial p1Copy = new Polynomial(p1);
        Polynomial p2 = new Polynomial("2 0 1 1");
        p1.times(p2);
        assertEquals(String.format("%s times %s:", p1Copy, p2), new Polynomial("4 0 4 1 1 2"), p1);
    }

    @Test
    public void testTimesSelf() {
        Polynomial p1 = new Polynomial("2 0 1 1");
        Polynomial p1Copy = new Polynomial(p1);
        p1.times(p1);
        assertEquals(String.format("%s times %s:", p1Copy, p1Copy), new Polynomial("4 0 4 1 1 2"), p1);
    }

    @Test
    public void testPlusTimesDistributive() {
        Polynomial a = new Polynomial("1 0 2 1 3 2");
        Polynomial b = new Polynomial("2 0 3 1 4 2");
        Polynomial c = new Polynomial("-100 0 42.24 1 1337.70 70");
        Polynomial a2 = new Polynomial(a);
        Polynomial b2 = new Polynomial(b);
        Polynomial c2 = new Polynomial(c);

        b.plus(c);
        a.times(b);

        b2.times(a2);
        c2.times(a2);
        b2.plus(c2);

        assertEquals("a * (b + c) == (a * b) + (a * c)", a, b2);
    }

    @Test
    public void testMinusIsNegatedPlus() {
        Polynomial a = new Polynomial("1 0 2 1 3 2");
        Polynomial b = new Polynomial("2 0 3 1 4 2");
        Polynomial a2 = new Polynomial(a);
        Polynomial b2 = new Polynomial(b);

        a.minus(b);

        b2.times(new Polynomial("-1 0"));
        a2.plus(b2);

        assertEquals("a - b == a + (-1 * b)", a, a2);
    }

    /**
     * Test of divide method, of class Polynomial.
     */
    @Test
    public void testDivide() {
        Polynomial p1 = new Polynomial("-1 0 4 1 2 3");
        Polynomial p1Copy = new Polynomial(p1);
        Polynomial p2 = new Polynomial("-2 0 2 1");
        Polynomial pd = p1.divide(p2);

        assertEquals(String.format("%s div %s:", p1Copy, p2), new Polynomial("3 0 1 1 1 2"), p1);
        assertEquals(String.format("%s mod %s:", p1Copy, p2), new Polynomial("5 0"), pd);

    }

    @Test
    public void testDivideSelf() {
        Polynomial p1 = new Polynomial("1 0");
        Polynomial p1Copy = new Polynomial(p1);
        Polynomial pd = p1.divide(p1);

        assertEquals(String.format("%s div %s:", p1Copy, p1Copy), new Polynomial("1 0"), p1);
        assertEquals(String.format("%s mod %s:", p1Copy, p1Copy), new Polynomial(), pd);

    }

    @Test(expected = ArithmeticException.class)
    public void testDivide_divisionByZero() {
        Polynomial p1 = new Polynomial("1 0");
        Polynomial p2 = new Polynomial();
        p1.divide(p2);
    }

}
