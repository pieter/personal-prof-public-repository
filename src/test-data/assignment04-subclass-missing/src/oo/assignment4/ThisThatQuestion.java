package oo.assignment4;

public class ThisThatQuestion extends Question {

    private String answer1;
    private String answer2;
    private String correctAnswer;

    public ThisThatQuestion(String question, String answer1, String answer2, String correctAnswer, int score) {
        super(question, score);
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.correctAnswer = correctAnswer;
    }

    public ThisThatQuestion(String question, String answer1, String answer2, String correctAnswer) {
        super(question, 3);
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.correctAnswer = correctAnswer;
    }

    @Override
    public String toString() {
        return answer1 + " or " + answer2 + ": " + question;
    }

    @Override
    public String correctAnswer() {
        return correctAnswer;
    }

}
