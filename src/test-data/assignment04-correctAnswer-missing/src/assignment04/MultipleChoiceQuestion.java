package assignment04;

public class MultipleChoiceQuestion extends Question {

	protected int correctAnswer;
	protected String question;
	protected String[] answers;
	private String correctAnswerStr;

	public MultipleChoiceQuestion(String question, String[] answers, int correctAnswer, int score) {
		this.question = question;
		this.answers = answers;
		setCorrectAnswer(correctAnswer);
		setScore(score);
	}

	public MultipleChoiceQuestion(String question, String[] answers, int correctAnswer) {
		this.question = question;
		this.answers = answers;
		setCorrectAnswer(correctAnswer);
		setScore(3);
	}

	private void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
		correctAnswerStr = "" + (char) ('a' + correctAnswer);
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		result.append(question);
		result.append("\n");

		char curChar = 'a';
		for (String answer : answers) {
			result.append(curChar + ") ");
			result.append(answer);
			result.append("\n");
			curChar++;
		}

		return result.toString();
	}

	@Override
	public boolean isCorrect(String answer) {
		return answer.equalsIgnoreCase(correctAnswerStr);
	}
}
