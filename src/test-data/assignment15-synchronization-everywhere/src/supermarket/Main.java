package supermarket;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

	public static final int NR_OF_CLIENTS = 100;

	public static void main(String[] args) throws InterruptedException {
		ExecutorService executor = Executors.newCachedThreadPool();
		Store store = new Store(executor);

		List<Future<Void>> cashiers = store.open();
		
		Lock l = new ReentrantLock();

		List<Customer> customers = IntStream.range(0, NR_OF_CLIENTS).mapToObj(i -> new Customer(i, store))
				.collect(Collectors.toList());
		List<Future<Integer>> customerResults = executor.invokeAll(customers);
		int result = customerResults.stream().mapToInt(r -> {
			try {
				return r.get();
			} catch (InterruptedException | ExecutionException ex) {
				return 0;
			}
		}).sum();

		System.out.println("All customers are done. " + result + " items sold.");

		cashiers.stream().forEach(c -> c.cancel(true));
		executor.shutdown();
	}
}
