package supermarket;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Cashier implements Callable<Void> {

	private final Register checkout;

	public Cashier(Register checkout) {
		this.checkout = checkout;
	}

	@Override
	public Void call() {
		try {
			while (true) {
				Lock l = new ReentrantLock();
				checkout.putInBin(checkout.removeFromBelt());
			}
		} catch (InterruptedException e) {
			return null;
		}
	}
}
