package snake;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.shape.Circle;

public class Food extends Circle {

    private final IntegerProperty x = new SimpleIntegerProperty(), y = new SimpleIntegerProperty();

    public void moveTo(int x, int y) {
        this.x.set(x);
        this.y.set(y);
    }

    public int getFoodX() {
        return x.get();
    }

    public int getFoodY() {
        return y.get();
    }

    public IntegerProperty getXProperty() {
        return x;
    }

    public IntegerProperty getYProperty() {
        return y;
    }
}
