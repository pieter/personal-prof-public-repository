package piechart;

import java.util.LinkedList;
import java.util.List;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

/**
 *
 * @author pieterkoopman
 */
public class PieChartGenerator extends Application {
    private final IntegerProperty sum = new SimpleIntegerProperty(0);
    private final IntegerProperty beginAngle = new SimpleIntegerProperty(90); // 12 o'clock
    private final IntegerProperty x = new SimpleIntegerProperty();
    private final IntegerProperty y = new SimpleIntegerProperty();
    private final IntegerProperty r = new SimpleIntegerProperty();
    private final VBox textPane = new VBox();
    private final Pane piePane = new Pane();
  
  @Override
  public void start(Stage primaryStage) {
    BorderPane root = new BorderPane();
    root.setLeft(textPane);
    root.setCenter(piePane);
    x.bind(piePane.widthProperty().divide(2));
    y.bind(piePane.heightProperty().divide(2));
    r.bind(y.subtract(5));
    FlowPane flowPane = new FlowPane();
    flowPane.setHgap(10);
    root.setBottom(flowPane);
    Button minButton = new Button("-");
    Button plusButton = new Button("+");
    TextField sumText = new TextField();
    sumText.textProperty().bind(sum.asString());
    flowPane.getChildren().addAll(minButton, plusButton, sumText);
    plusButton.setOnAction(e -> add());
    minButton.setOnAction(e -> remove());
    add(); // the first textfield
    Scene scene = new Scene(root, 500, 300);
    primaryStage.setTitle("Pie");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
  
  private void add () {
    IntegerProperty ip = new SimpleIntegerProperty(10);
    sum.set(sum.get() + ip.get());
    IntegerProperty start = beginAngle;
    if (! piePane.getChildren().isEmpty()) {
      Arc last = (Arc) piePane.getChildren().get(piePane.getChildren().size() - 1);
      start = new SimpleIntegerProperty();
      start.bind(last.startAngleProperty().add(last.lengthProperty()));
    }
    addText(ip);
    addArc(start, ip);
  }
  
  private void addText(IntegerProperty v) {
    TextField text = new TextField(v.getValue().toString());
    text.textProperty().addListener(new ChangeListener<String>() {
      @Override
      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (!newValue.matches("[1-9]\\d{0,3}")) {
          text.setText(oldValue);
        } else {
          int old = v.get();
          int cur = Integer.parseInt(newValue);
          v.set(cur);
          sum.set(sum.get() + cur - old);
        }
      }
    });
    textPane.getChildren().add(text);
  }
  
  private void addArc (IntegerProperty start, IntegerProperty v) {
    Arc arc = new Arc();
    arc.setFill(Color.DARKGREY);
    arc.setStroke(Color.BLUE);
    arc.setType(ArcType.ROUND);
    arc.centerXProperty().bind(x);
    arc.centerYProperty().bind(y);
    arc.radiusXProperty().bind(r);
    arc.radiusYProperty().bind(r);
    arc.startAngleProperty().bind(start);
    IntegerProperty l = new SimpleIntegerProperty();
    l.bind(v.multiply(-360).divide(sum)); // clockwise
    arc.lengthProperty().bind(l);
    piePane.getChildren().add(arc);
  }
  
  private void remove () {
    List<Node> textFields = textPane.getChildren();
    int len = textFields.size();
    if (len > 1) {
      TextField last = (TextField) textFields.get(len - 1);
      sum.set(sum.get() - Integer.parseInt(last.getText()));
      textFields.remove(len - 1);
      piePane.getChildren().remove(len - 1);
    }
  }

  public static void main(String[] args) {
    launch(args);
  }  
}
