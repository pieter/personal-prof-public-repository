package assignment04;

public class ThisThatQuestion extends MultipleChoiceQuestion {

	public ThisThatQuestion(String question, String answer1, String answer2, int correctAnswer, int score) {
		super(question, new String[] { answer1, answer2 }, correctAnswer, score);
	}

	public ThisThatQuestion(String question, String answer1, String answer2, int correctAnswer) {
		super(question, new String[] { answer1, answer2 }, correctAnswer);
	}

	@Override
	public boolean isCorrect(String userAnswer) {
		return answers[correctAnswer].equalsIgnoreCase(userAnswer);
	}

	@Override
	public String correctAnswer() {
		return answers[correctAnswer];
	}
}
