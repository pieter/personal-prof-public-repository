package assignment04;

public class OpenQuestion extends Question {

	private String question, answer;

	public OpenQuestion(String question, String answer, int score) {
		this.question = question;
		this.answer = answer;
		setScore(score);

	}

	public OpenQuestion(String question, String answer) {
		this.question = question;
		this.answer = answer;
		setScore(3);
	}

	@Override
	public String toString() {
		return question;
	}

	@Override
	public boolean isCorrect(String userAnswer) {
		return answer.equalsIgnoreCase(userAnswer);
	}

	@Override
	public String correctAnswer() {
		return answer;
	}
}
