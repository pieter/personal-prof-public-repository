package assignment04;

public abstract class Question {

	private int score;

	public abstract String toString();

	public abstract boolean isCorrect(String answer);

	public abstract String correctAnswer();

	public void setScore(int s) {
		if (s < 1 || s > 5) {
			score = 3;
		} else {
			score = s;
		}
	}

	public int getScore() {
		return score;
	}
}
