package assignment04;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Game {

	private Scanner scanner;
	private int firstRoundScore, secondRoundScore;
	private List<Question> questions;
	private List<Question> roundTwoQuestions;

	public Game() {
		scanner = new Scanner(System.in);
		questions = new LinkedList<>();
		roundTwoQuestions = new LinkedList<>();
		initializeQuestions();
	}

	private void initializeQuestions() {
		questions.add(new OpenQuestion("What is the big O complexity of binary search?", "O(log N)", 2));
		questions.add(new OpenQuestion("What is the minimal amount of constructors for a Java class?", "0"));
		questions.add(new OpenQuestion("Is there a difference between an interface and an abstract class?", "Yes", 5));
		questions.add(
				new OpenQuestion("How would you read an integer i from scanner s in Java?", "i = s.nextInt();", 2));
		questions.add(new MultipleChoiceQuestion("How do you read a non -empty word using scanner s?",
				new String[] { "s.nextline()", "s.next(\"\\S+\")", "s.next(\"\\a*\")", "s.next(\"\\S*\")",
						"s.next(\"\\\\s+\")", "s.next(\"\\s+\")", "s.nextString(\"\\s*\")", "s.next(\"\\\\S+\")",
						"s.nextString()" },
				7, 1));
		questions.add(new MultipleChoiceQuestion("What is the best achievable complexity of in situ sorting?",
				new String[] { "O(N^2)", "O(N log N)", "O(N)", "O(log N)" }, 1, 4));
		questions.add(new ThisThatQuestion("Is there a difference between abstract classes and interfaces in Java?",
				"yes", "no", 0, 2));
		questions.add(new ThisThatQuestion("Each class definition must have a constructor?", "right", "wrong", 1));
	}

	public void play() {
		round1();
		round2();
		printResult();
	}

	private void round1() {
		for (Question q : questions) {
			System.out.println(q);
			String answer = scanner.nextLine();
			if (q.isCorrect(answer)) {
				firstRoundScore += q.getScore();
				System.out.println("Correct!");
			} else {
				roundTwoQuestions.add(q);
				System.out.println("Not correct. The correct answer is:");
				System.out.println(q.correctAnswer());
			}
		}
	}

	private void round2() {
		for (Question q : roundTwoQuestions) {
			System.out.println(q);
			String answer = scanner.nextLine();
			if (q.isCorrect(answer)) {
				secondRoundScore += q.getScore();
				System.out.println("Correct!");
			} else {
				System.out.println("The correct answer would have been:");
				System.out.println(q.correctAnswer());
			}
		}
	}

	private void printResult() {
		System.out.println("Score first round: " + firstRoundScore);
		System.out.println("Score second round: " + secondRoundScore);
	}
}