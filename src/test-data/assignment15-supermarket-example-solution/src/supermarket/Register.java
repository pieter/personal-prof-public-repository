package supermarket;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Register {

	private static final int CONVEYER_SIZE = 10, BIN_SIZE = 10;
	private final ConveyorBelt<Item> belt = new ConveyorBelt<>(CONVEYER_SIZE);
	private final ConveyorBelt<Item> bin = new ConveyorBelt<>(BIN_SIZE);

	private final Lock lock = new ReentrantLock();

	public void putOnBelt(Item article) {
		try {
			belt.putIn(article);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public Item removeFromBelt() throws InterruptedException {
		return belt.removeFrom();
	}

	public void putInBin(Item article) throws InterruptedException {
		bin.putIn(article);
	}

	public Item removeFromBin() throws InterruptedException {
		return bin.removeFrom();
	}

	public void claim() {
		lock.lock();
	}

	public void free() {
		lock.unlock();
	}
}
