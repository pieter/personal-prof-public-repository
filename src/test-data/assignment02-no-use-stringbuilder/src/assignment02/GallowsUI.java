package assignment02;

import java.util.*;

public class GallowsUI {

	private static Scanner scanner;
	private static Gallows gallows;

	public GallowsUI() {
		scanner = new Scanner(System.in);
		welcomeMessage();
		chooseWord();
		playGallows();
		showResult();
	}

//   Greetings to user
	public void welcomeMessage() {
		System.out.println("Welcome to Hangman!");
		System.out.println("");
		System.out.println("");
	}

//    Choose for an own word or an automated word.
	public void chooseWord() {
		System.out.println("Please enter a word or press <Enter> to randomly pick one");
		System.out.print("> ");
		String path = scanner.nextLine();
		if (path.isEmpty()) {
			gallows = new Gallows();
		} else {
			gallows = new Gallows(path);
		}
	}

//   Tells if the letter is in the word; 
//    shows guessed letters; how many attempts left;
//    shows where the right guessed letters are in the word.
	public void playGallows() {
		while (gallows.getGuessesLeft() > 0 && !gallows.isWordGuessed()) {
			System.out.println("");
			System.out.println("Guessed letters: [" + gallows.getGuessedLetters() + "]");
			System.out.println(gallows.getWordSoFar());
			System.out.println("Remaining guesses: " + gallows.getGuessesLeft());
			System.out.println("Enter a letter you want to guess: ");
			System.out.print("> ");
			String input = scanner.next();
			if (input.length() == 1) {
				gallows.guessLetter(input.charAt(0));
			} else {
				System.out.println("Please enter one letter.");
			}
		}
	}

//  Checks if you guessed the word within 10 attempts and shows the word that
//   had to be guessed.     
	public void showResult() {
		if (gallows.isWordGuessed()) {
			System.out.println("Good job, you got it!!!");
			System.out.println("The word was: " + gallows.getWordToGuess());
		} else {
			System.out.println("Too bad, you did not get the word");
			System.out.println("The word was: " + gallows.getWordToGuess());
		}
	}
}
