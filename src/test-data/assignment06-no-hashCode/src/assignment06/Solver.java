package assignment06;

import java.util.*;

/**
 * @author Pieter Koopman, Sjaak Smetsers
 * @version 08-03-2012
 */
public class Solver {

	Queue<Configuration> toExamine;
	Collection<Configuration> encountered;

	public Solver(Configuration g) {
		toExamine = new PriorityQueue<>();
		encountered = new HashSet<Configuration>();
		toExamine.add(g);
		encountered.add(g);
	}

	public String solve() {
		while (!toExamine.isEmpty()) {
			Configuration next = toExamine.remove();
			if (next.isSolution()) {
				return printSolutions(next);
			} else {
				for (Configuration succ : next.successors()) {
					if (!encountered.contains(succ)) {
						toExamine.add(succ);
						encountered.add(succ);
					}
				}
			}
		}
		return "No solution found";
	}

	private String printSolutions(Configuration c) {
		StringBuilder b = new StringBuilder();
		for (Configuration conf : c.pathFromRoot()) {
			b.append(conf);
		}
		return b.toString();
	}

}
