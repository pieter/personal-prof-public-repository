package assignment06;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Pieter Koopman
 */
public class SlidingGame implements Configuration {

	public static final int N = 3, SIZE = N * N, HOLE = SIZE;
	private int[][] board;
	private int holeX, holeY;
	private int manhattanDist;
	Configuration parent;

	public SlidingGame(int[] start) {
		board = new int[N][N];
		parent = null;

		assert start.length == N * N : "Length of specified board incorrect";

		for (int p = 0; p < start.length; p++) {
			board[p % N][p / N] = start[p];
			if (start[p] == HOLE) {
				holeX = p % N;
				holeY = p / N;
			} else {
				manhattanDist += manhattanDist(p % N, p / N, start[p]);
			}
		}
	}

	private SlidingGame(SlidingGame orig) {
		board = new int[N][N];
		parent = orig;
		for (int x = 0; x < N; x++) {
			for (int y = 0; y < N; y++) {
				board[x][y] = orig.board[x][y];
			}
		}
		holeX = orig.holeX;
		holeY = orig.holeY;
		manhattanDist = orig.manhattanDist;
	}

	public Configuration getParent() {
		return parent;
	}

	public boolean canMove(Direction dir) {
		return holeX + dir.GetDX() >= 0 && holeX + dir.GetDX() < N && holeY + dir.GetDY() >= 0
				&& holeY + dir.GetDY() < N;
	}

	@Override
	public Collection<Configuration> successors() {
		List<Configuration> succs = new LinkedList<>();
		for (Direction dir : Direction.values()) {
			if (canMove(dir)) {
				SlidingGame s = new SlidingGame(this);
				s.holeX += dir.GetDX();
				s.holeY += dir.GetDY();
				s.board[holeX][holeY] = board[s.holeX][s.holeY];
				s.board[s.holeX][s.holeY] = HOLE;
				s.manhattanDist += manhattanDist(holeX, holeY, board[s.holeX][s.holeY])
						- manhattanDist(s.holeX, s.holeY, board[s.holeX][s.holeY]);
				succs.add(s);
			}
		}
		return succs;
	}

	private static int manhattanDist(int x, int y, int v) {
		return Math.abs((v - 1) % N - x) + Math.abs((v - 1) / N - y);
	}

	public int getManhattanDistance() {
		return manhattanDist;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < N; i += 1) {
			for (int j = 0; j < N; j += 1) {
				int p = board[j][i];
				buf.append(p == HOLE ? "  " : p + " ");
			}
			buf.append("\n");
		}
		return buf.toString();
	}

	@Override
	public boolean isSolution() {
		for (int i = 0; i < N; i += 1) {
			for (int j = 0; j < N; j += 1) {
				if (board[i][j] != (j * N + i + 1)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != getClass()) {
			return false;
		} else {
			SlidingGame p = (SlidingGame) o;
			for (int x = 0; x < N; x += 1) {
				for (int y = 0; y < N; y += 1) {
					if (board[x][y] != p.board[x][y]) {
						return false;
					}
				}
			}
			return true;
		}
	}

	@Override
	public int compareTo(Configuration g) {
		return manhattanDist - ((SlidingGame) g).manhattanDist;
	}

}
