package oo.assignment4;

public class ThisThatQuestion extends MultipleChoiceQuestion {

    public ThisThatQuestion(String question, String answer1, String answer2, int correctAnswer, int score) {
        super(question, null, correctAnswer, score);
    }

    public ThisThatQuestion(String question, String answer1, String answer2, int correctAnswer) {
        super(question, null, correctAnswer);
    }

    @Override
    public String toString() {
        return answers[0] + " or " + answers[1] + ": " + question;
    }

}
