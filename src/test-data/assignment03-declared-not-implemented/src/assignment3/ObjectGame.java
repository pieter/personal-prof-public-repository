/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import java.util.Scanner;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public class ObjectGame {
    public void run(){
        Scanner scanner = new Scanner(System.in);
        ObjectList objects=new ObjectList(10);
        Controller controller=new Controller();
        UserInterface ui=new UserInterface();
        Model model = new Model(scanner,controller,ui,objects);
        
        model.execute();
    }
}
