/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public class Rectangle implements Geometric {
    private double width;
    private double height;
    private double xCoord;
    private double yCoord;

    public Rectangle(double xCoord, double yCoord,double height, double width) {
        this.height = height;
        this.width = width;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    @Override
    public double topBorder() {

        return this.yCoord + this.height;
    }

    @Override
    public double bottomBorder() {
        return this.yCoord;
    }

    @Override
    public double leftBorder() {
        return this.xCoord;
    }

    @Override
    public double rightBorder() {
        return this.xCoord + this.width;
    }

    @Override
    public double area() {
        return this.height * this.width;
    }

    @Override
    public int compareTo(Geometric thing) {
        if (this.area() > thing.area()) {
            return 1;
        } else if (this.area() < thing.area()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public void move(double x, double y) {
        this.xCoord += x;
        this.yCoord += y;
    }
    
    public String toString() {
        return "Rectangle (x,y,width,height,): " + this.xCoord + " " + this.yCoord + " " + this.width + "," + this.height + " ";
    }
}
