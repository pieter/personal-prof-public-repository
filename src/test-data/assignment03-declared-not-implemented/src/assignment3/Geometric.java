/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import java.util.Comparator;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public interface Geometric extends Comparable<Geometric>{
    public double topBorder();
    public double bottomBorder();
    public double leftBorder();
    public double rightBorder();
    public double area();
    public void move(double x,double y);
    public int compareTo(Geometric thing);
}
