/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import static java.util.Arrays.sort;
import java.util.Comparator;

/**
 *
 * @author Benjamin Gerritsen s1019202
 * @author Christian Bloks s1019760
 */
public class ObjectList {

    private Geometric[] objects;
    private int objectCount;

    public ObjectList(int size) {
        objects = new Geometric[size];
        objectCount = 0;
    }

    public int getObjectCount() {
        return objectCount;
    }

    public void add(Geometric object) {
        objects[objectCount] = object;
        objectCount++;
    }

    public void set(int index, Geometric object) {
        objects[index] = object;
    }

    public Geometric get() {
        return objects[objectCount + 1];

    }

    public Geometric get(int index) {
        return objects[index];
    }

    public void remove() {
        objects[objectCount] = null;
        objectCount--;
    }

    public void remove(int index) {
        for (int i = index + 1; i <= objectCount; i++) {
            objects[i - 1] = objects[i];
        }
        objects[objectCount] = null;
        objectCount--;
    }

    public void sortList(Comparator<Geometric> c) {
        sort(objects, 0, objectCount, c);
    }
    public void sortList() {
        sort(objects, 0, objectCount);
    }
}
