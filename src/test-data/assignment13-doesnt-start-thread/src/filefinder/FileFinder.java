package filefinder;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class FileFinder implements Runnable {

	private final File rootDir;
	private String fileName;

	public FileFinder(String dirPath) throws IOException {
		rootDir = new File(dirPath);
		if (!(rootDir.exists() && rootDir.isDirectory())) {
			throw new IOException(dirPath + ": Not a directory");
		}
	}

	private FileFinder(File dir, String fileName) {
		this.rootDir = dir;
		this.fileName = fileName;
	}

	public void findFile(String fileName) {
		this.fileName = fileName;
		run();
	}

	public void run() {
		File[] files = rootDir.listFiles();
		if (files != null) {
			for (File file : rootDir.listFiles()) {
				if (file.isDirectory()) {
					FileFinder ff = new FileFinder(file, fileName);
					ff.run();
				} else if (fileName.equals(file.getName())) {
					System.out.println(file.getAbsolutePath());
				}
			}
		}
	}
}