package exercise3oo;

import java.util.ArrayList;

public class GeometryArray {

    private ArrayList<Geometric> objects;

    public GeometryArray() {
        objects = new ArrayList<>();
    }

    public void addCircle(Circle circle) {
        objects.add(circle);
    }

    public int getSize() {
        return objects.size();
    }

    public Geometric getElement(int index) {
        return objects.get(index);
    }

    public void move(double dx, double dy, int index) {
        objects.get(index).moveObject(dx, dy);
    }

    public void remove(int index) {
        objects.remove(index);
    }

    public ArrayList<Geometric> getObjects() {
        return this.objects;
    }
}
