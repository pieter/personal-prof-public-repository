module AssignmentSnakeRules

import Message;
import lang::java::m3::Core;

import ClassHierarchy;
import StandardLocations;

import Util;

set[Message] allAssignmentSnakeRules(M3 model)
  = aSnake_model_separated(model)
  + aSnake_view_separated(model)
  + aSnake_controller_separated(model)
  ;

// Check if World contains the timeline, in which case most model logic is most likely there too
set[Message] aSnake_model_separated(M3 model) = classDependsOn(model, "World", javafxAnimationTimeline);

set[Message] aSnake_view_separated(M3 model)
{
	// Check if no classes other than SnakeGame contain JavaFX shapes
	try
    {
	    set[loc] shapeClasses = 
	      { javafxSceneShapeCircle
	      , javafxSceneShapeRectangle
	      , javafxSceneShapePolygon
	      , javafxSceneShapeEllipse
	      , javafxSceneShapeLine
	      , javafxSceneShapeArc
	      };
	    set[loc] permittedClasses = { findClass(model, "SnakeGame") };
	    set[loc] allPermittedClasses = { *(model.containment*)[permitted] | permitted <- permittedClasses};
	 	return { error("<getName(model, shapeClass)>: only SnakeGame should use shapes", class)
	      | class <- classes(model) - allPermittedClasses
	      , shapeClass <- shapeClasses
	      , (shapeClass in model.extends[class] || shapeClass in allTypeDependenciesLoc(model, class))
	      };
    }
    catch e:error(_,_):
    {
    	return { e };
    }
}

set[Message] aSnake_controller_separated(M3 model)
{
	// Check if InputHandler is the only class that uses JavaFX EventHandler
	try
    {
		set[loc] eventClasses = 
	      { javafxSceneInputKeyEvent
	      , javafxSceneInputMouseEvent
	      };
    	set[loc] permittedClasses =
    	  { findClass(model, "Main")
	      , findClass(model, "InputHandler")
	      };
	    set[loc] allPermittedClasses = { *(model.containment*)[permitted] | permitted <- permittedClasses};
	    return { error("<getName(model, eventClass)>: only InputHandler should handle input events", class)
	      | class <- classes(model) - allPermittedClasses
	      , eventClass <- eventClasses
	      , eventClass in allTypeDependenciesLoc(model, class)
	      };
    }
    catch e:error(_,_):
    {
    	return { e };
    }
}
