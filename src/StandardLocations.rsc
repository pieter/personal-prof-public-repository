module StandardLocations

public loc javaLangComparable = |java+interface:///java/lang/Comparable|;
public loc compareTo = |java+method:///java/lang/Comparable/compareTo(T)|;

public loc javaLangStringBuilder = |java+class:///java/lang/StringBuilder|;

public loc javaIoPrintStream = |java+class:///java/io/PrintStream|;

public loc javaUtilListIterator = |java+interface:///java/util/ListIterator|;
public loc javaUtilListSize = |java+method:///java/util/List/size()|;
public loc javaUtilListGet = |java+method:///java/util/List/get(int)|;

public loc orgJunitTest = |java+interface:///org/junit/Test|;

public loc javaUtilCollection = |java+interface:///java/util/Collection|;
public loc javaUtilQueue = |java+interface:///java/util/Queue|;
public loc javaUtilHashSet = |java+class:///java/util/HashSet|;
public loc javaUtilPriorityQueue = |java+class:///java/util/PriorityQueue|;

public loc javaLangHashCode = |java+method:///java/lang/Object/hashCode()|;
public loc javaLangToString = |java+method:///java/lang/Object/toString()|;

public loc javaUtilStream = |java+interface:///java/util/stream/Stream|;
public loc javaUtilIntStream = |java+interface:///java/util/stream/IntStream|;

public loc javaLangThread = |java+class:///java/lang/Thread|;
public loc threadJoin = |java+method:///java/lang/Thread/join()|;
public loc threadStart = |java+method:///java/lang/Thread/start()|;
public loc javaLangRunnable = |java+interface:///java/lang/Runnable|;
public loc javaUtilExecutorService = |java+interface:///java/util/concurrent/ExecutorService|;

public loc javaUtilLock = |java+interface:///java/util/concurrent/locks/Lock|;
public loc javaUtilCondition = |java+interface:///java/util/concurrent/locks/Condition|;

public loc javafxAnimationTimeline = |java+class:///javafx/animation/Timeline|;

public loc javafxSceneInputKeyEvent = |java+class:///javafx/scene/input/KeyEvent|;
public loc javafxSceneInputMouseEvent = |java+class:///javafx/scene/input/MouseEvent|;

public loc javafxSceneShapeCircle = |java+class:///javafx/scene/shape/Circle|;
public loc javafxSceneShapeRectangle = |java+class:///javafx/scene/shape/Rectangle|;
public loc javafxSceneShapePolygon = |java+class:///javafx/scene/shape/Polygon|;
public loc javafxSceneShapeEllipse = |java+class:///javafx/scene/shape/Ellipse|;
public loc javafxSceneShapeLine = |java+class:///javafx/scene/shape/Line|;
public loc javafxSceneShapeArc = |java+class:///javafx/scene/shape/Arc|;
