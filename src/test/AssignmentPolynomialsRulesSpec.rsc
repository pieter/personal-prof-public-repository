module \test::AssignmentPolynomialsRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentPolynomialsRules;
import CodingRules;
import Util;

test bool aPolynomialsexampleSolution()
{
  M3 model = loadTestProject("assignment08-polynomials-example-solution");
  errors = allAssignmentPolynomialsRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aPolynomialsstartProject()
{
  M3 model = loadTestProject("assignment08-polynomials-start");
  errors = allAssignmentPolynomialsRules(model);
  return
    { "Polynomial should use |java+interface:///java/util/ListIterator|"
    } <= messages(errors);
}

test bool aPolynomialsadditionalClasses()
{
  M3 model = loadTestProject("assignment08-polynomials-additional-class");
  errors = allAssignmentPolynomialsRules(model);
  return
    size(errors) == 2 &&
    { "You should not write additional classes in this assignment"
    } == messages(errors);
}

test bool aPolynomialsnumTestCases()
{
  M3 model = loadTestProject("assignment08-polynomials-start");
  errors = allAssignmentPolynomialsRules(model);
  return
    { "Please write at least 11 test cases, as described in the assignment text"
    } <= messages(errors);
}