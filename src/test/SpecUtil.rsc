module \test::SpecUtil

import lang::java::m3::Core;
import lang::java::m3::AST;

import Configuration;

M3 loadTestProject(str projectName)
{
  // classPath needs my patched version
  // https://github.com/mklinik/rascal/commit/6ade3236504e9e102ddfb8db0d42b9d2f0df32a1
  M3 model = createM3FromDirectory(testDataDir + projectName, javaVersion=defaultJavaVersion, classPath=defaultClassPath);
  return model;
}

set[Declaration] loadTestAST(str projectName)
{
  return createAstsFromDirectory(testDataDir + projectName, true, javaVersion=defaultJavaVersion);
}

tuple[M3 model, set[Declaration] ast] loadTestBoth(str projectName)
{
  return
    < loadTestProject(projectName)
    , loadTestAST(projectName)
    >;
}