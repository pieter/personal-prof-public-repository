module \test::AssignmentGeometricRulesSpec

import lang::java::m3::Core;

import Set;
import \test::SpecUtil;
import Util;

import AssignmentGeometricRules;
import CodingRules;

test bool aGeometricexampleSolution()
{
  M3 model = loadTestProject("assignment03-geometric-example-solution");
  errors = allAssignmentGeometricRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aGeometricprojectWithGeometricInterface()
{
  return isEmpty(aGeometric_geometric_is_comparable(loadTestProject("assignment03-good")));
}

test bool aGeometricprojectWithoutGeometricInterface()
{
  errors = aGeometric_geometric_is_comparable(loadTestProject("assignment01-good"));
  return "There should be an interface called Geometric" in messages(errors);
}

test bool aGeometricgeometricShouldExtendComparable()
{
  errors = aGeometric_geometric_is_comparable(loadTestProject("assignment03-geometric-doesnt-extend-comparable"));
  return "Geometric should extend Comparable" in messages(errors);
}

test bool aGeometricrectangleAndCircleImplementGeometric()
{
  errors = aGeometric_shapes_implement_comparable(loadTestProject("assignment03-good"));
  return isEmpty(errors);
}

test bool aGeometricrectangleIsMissing()
{
  errors = aGeometric_shapes_implement_comparable(loadTestProject("assignment03-rectangle-missing"));
  return
    { "There should be two implementations of Geometric: Rectangle and Circle" }
    <= messages(errors);
}

test bool aGeometricshapesImplementComparableDirectly()
{
  errors = aGeometric_shapes_implement_comparable(loadTestProject("assignment03-shapes-implement-too-much"));
  return
    { "Circle should implement Geometric and nothing else"
    , "Rectangle should implement Geometric and nothing else"
    }
    <= messages(errors);
}

test bool aGeometricgeometricDoesntOverrideCompareTo()
{
  errors = aGeometric_shapes_implement_comparable(loadTestProject("assignment03-no-override-compareTo"));
  return
    { "Circle should override Comparable.compareTo"
    , "Rectangle should override Comparable.compareTo"
    }
    <= messages(errors);
}

// Geometric declares compareTo, and the shapes implement it.
// This test should pass.
test bool aGeometricgeometricDeclaresShapeImplements()
{
  errors = aGeometric_shapes_implement_comparable(loadTestProject("assignment03-declared-and-implemented"));
  return isEmpty(errors);
}


// Geometric declares compareTo, but Circle does not implement it.
// This should generate an error. It seems impossible to distinguish this
// case from a default implementation in Geometric with the current M3.
// For now we don't generate an error.
// This is okay, also according to Pieter, because students get a compile error anyway.
test bool aGeometricgeometricDeclaresButShapeDoesntImplement()
{
/*
  M3 model = loadTestProject("assignment03-declared-not-implemented");
  errors = a03_shapes_implement_comparable(model);
  return
    { "Circle should override Comparable.compareTo"
    } <= messages(errors);
*/
  return true;
}


test bool aGeometricdirtyCircle()
{
  errors = allAssignmentGeometricRules(loadTestProject("assignment03-dirty-circle"));
  return "Circle should not perform I/O" in messages(errors);
}
