module \test::AssignmentSupermarketRulesSpec

import Set;

import \test::SpecUtil;
import AssignmentSupermarketRules;
import CodingRules;
import Util;

test bool aSupermarketExampleSolution()
{
  <model,ast> = loadTestBoth("assignment15-supermarket-example-solution");
  errors = allAssignmentSupermarketRules(model,ast) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aSupermarketSynchronized()
{
  <model,ast> = loadTestBoth("assignment15-supermarket-synchronized");
  errors = allAssignmentSupermarketRules(model,ast) + allCodingRules(model);
  return size(errors) == 2 &&
    { "synchronized should not be used in this assignment"
    } == messages(errors);
}

test bool aSupermarketSynchronizationEverywhere()
{
  <model,ast> = loadTestBoth("assignment15-synchronization-everywhere");
  errors = allAssignmentSupermarketRules(model,ast) + allCodingRules(model);
  return
    { "class Main should not have synchronization code"
    , "class Customer should not have synchronization code"
    , "class Cashier should not have synchronization code"
    } == messages(errors);
}

test bool aSupermarketStart()
{
  <model,ast> = loadTestBoth("assignment15-supermarket-start");
  errors = allAssignmentSupermarketRules(model,ast) + allCodingRules(model);
  return
    { "ConveyorBelt should use |java+interface:///java/util/concurrent/locks/Lock|"
    , "ConveyorBelt should use |java+interface:///java/util/concurrent/locks/Condition|"
    , "Register should use |java+interface:///java/util/concurrent/locks/Lock|"
    } == messages(errors);
}