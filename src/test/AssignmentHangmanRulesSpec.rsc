module \test::AssignmentHangmanRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentHangmanRules;
import CodingRules;
import Util;

test bool aHangmanexampleSolution()
{
  M3 model = loadTestProject("assignment02-hangman-example-solution");
  errors = allAssignmentHangmanRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aHangmanwrongProject()
{
  errors = allAssignmentHangmanRules(loadTestProject("assignment01-good"));
  return "There should be a class called Gallows" in messages(errors);
}

test bool aHangmanmainHasIO()
{
  errors = allAssignmentHangmanRules(loadTestProject("assignment02-main-has-io"));
  return "All I/O should happen in the view class" in messages(errors);
}

test bool aHangmannoUseStringBuilder()
{
  errors = allAssignmentHangmanRules(loadTestProject("assignment02-no-use-stringbuilder"));
  return "Gallows should use StringBuilder" in messages(errors);
}

// Main class creates a Scanner and passes it on to the View class. This should be allowed.
test bool aHangmanparameterizedView()
{
  errors = allAssignmentHangmanRules(loadTestProject("assignment02-parameterized-view"));
  return isEmpty(errors);
}


// Some students handed in a doubly zipped submission, where personal prof could
// not find a Main class. This led to an uncaught exception. This test case runs
// the hangman rules on their submission.
test bool aHangmanDoubleZip()
{
  errors = allAssignmentHangmanRules(loadTestProject("assignment02-double-zip"));
  return
    { "There should be a Main class"
    , "There should be a class called Gallows"
    } == messages(errors);
}
