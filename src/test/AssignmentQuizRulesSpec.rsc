module \test::AssignmentQuizRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentQuizRules;
import CodingRules;
import Util;

test bool aQuizabstractClassQuestionExists()
{
  M3 model = loadTestProject("assignment04-good");
  return isEmpty(aQuiz_question_exists(model));
}

test bool aQuizexampleSolution()
{
  M3 model = loadTestProject("assignment04-quiz-example-solution");
  errors = allAssignmentQuizRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aQuizquestionIsNotAbstract()
{
  M3 model = loadTestProject("assignment04-question-is-not-abstract");
  errors = aQuiz_question_exists(model);
  return { "Class Question should be abstract." } <= messages(errors);
}

test bool aQuizclassHierarchyIncorrect()
{
  M3 model = loadTestProject("assignment04-class-hierarchy-incorrect");
  errors = aQuiz_question_types(model);
  return {"ThisThatQuestion should extend MultipleChoiceQuestion"} <= messages(errors);
}

test bool aQuizallSubclassesExist()
{
  M3 model = loadTestProject("assignment04-good");
  errors = aQuiz_question_types(model);
  return isEmpty(errors);
}

test bool aQuizoneSubclassMissing()
{
  M3 model = loadTestProject("assignment04-subclass-missing");
  errors = aQuiz_question_types(model);
  return { "There should be a class called MultipleChoiceQuestion" } <= messages(errors);
}

test bool aQuiztoStringMissingFromSubclass()
{
  M3 model = loadTestProject("assignment04-toString-missing-from-subclass");
  errors = allAssignmentQuizRules(model);
  return "ThisThatQuestion should implement |java+method:///java/lang/Object/toString()|" in messages(errors);
}

test bool aQuizcorrectAnswerMissing()
{
  M3 model = loadTestProject("assignment04-correctAnswer-missing");
  errors = allAssignmentQuizRules(model);
  return "There should be a method Question.correctAnswer" in messages(errors);
}