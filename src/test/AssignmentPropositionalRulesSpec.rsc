module \test::AssignmentPropositionalRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentPropositionalRules;
import CodingRules;
import Util;

test bool aPropositionalexampleSolution()
{
  M3 model = loadTestProject("assignment11-visitor-example-solution");
  errors = allAssignmentPropositionalRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aPropositionalstartProjectCodingRules()
{
  M3 model = loadTestProject("assignment11-visitor-start");
  errors = allCodingRules(model);
  return isEmpty(errors);
}

test bool aPropositionalstartProjectErrors()
{
  M3 model = loadTestProject("assignment11-visitor-start");
  errors = allAssignmentPropositionalRules(model) + allCodingRules(model);
  return
    { "There should be a class called PrintVisitor"
    , "There should be a class called EvaluateVisitor"
    , "There should be an enum called Constant"
    , "There should be a class called Atom"
    , "There should be a class called BinaryOperator"
    , "There should be a class called Not"
    , "There should be an enum called BinOp"
    } == messages(errors);
}

test bool aPropositionalbinaryOperatorDoesntUseStrategy()
{
  M3 model = loadTestProject("assignment11-no-use-strategy");
  errors = allAssignmentPropositionalRules(model) + allCodingRules(model);
  return
    { "class BinaryOperator should use enum BinOp as strategy"
    } == messages(errors);
}