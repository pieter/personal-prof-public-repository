module \test::AssignmentExpressionsRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentExpressionsRules;
import CodingRules;
import Util;

test bool aExpressionsexampleSolution()
{
  M3 model = loadTestProject("assignment05-expressions-example-solution");
  errors = allAssignmentExpressionsRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aExpressionsbaseClassesNotAbstract()
{
  M3 model = loadTestProject("assignment05-base-classes-not-abstract");
  errors = allAssignmentExpressionsRules(model);
  return
    { "There should be an interface called Expression"
    , "Class NoArgExpr should be abstract."
    , "Class OneArgExpr should be abstract."
    , "Class TwoArgExpr should be abstract."
    } == messages(errors);
}

test bool aExpressionsvariableNotExtendsOneArgExpr()
{
  M3 model = loadTestProject("assignment05-variable-direct-extends-expr");
  errors = allAssignmentExpressionsRules(model);
  return
    { "Variable should extend NoArgExpr"
    , "Constant should extend NoArgExpr"
    } == messages(errors);
}