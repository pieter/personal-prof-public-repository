module \test::AssignmentTaxiRulesSpec

import IO;
import Set;

import \test::SpecUtil;
import AssignmentTaxiRules;
import CodingRules;
import Util;

test bool aTaxiexampleSolution()
{
  <model,ast> = loadTestBoth("assignment14-taxi-example-solution");
  errors = allAssignmentTaxiRules(model,ast) + allCodingRules(model);
  for( e <- errors ) println(e);
  return isEmpty(errors);
}

test bool aTaxistartProjectCodingRules()
{
  <model,ast> = loadTestBoth("assignment14-taxi-start");
  errors = allCodingRules(model);
  return isEmpty(errors);
}

test bool aTaxistart()
{
  <model,ast> = loadTestBoth("assignment14-taxi-start");
  errors = allAssignmentTaxiRules(model,ast);
  return
    { "Station should use |java+interface:///java/util/concurrent/locks/Lock|"
    , "Station should use |java+interface:///java/util/concurrent/locks/Condition|"
    , "Simulation should use |java+interface:///java/util/concurrent/ExecutorService|"
    , "|java+class:///taxi/Taxi| should implement |java+interface:///java/lang/Runnable|"
    , "|java+class:///taxi/Train| should implement |java+interface:///java/lang/Runnable|"
    } == messages(errors);
}


test bool aTaxisynchronizationInTaxi()
{
  <model,ast> = loadTestBoth("assignment14-synchronization-in-taxi");
  errors = allAssignmentTaxiRules(model,ast);
  return size(errors) == 2 &&
    { "Only Station should have synchronization"
    } <= messages(errors);
}

test bool aTaxisynchronizedMethod()
{
  <model,ast> = loadTestBoth("assignment14-synchronized");
  errors = allAssignmentTaxiRules(model,ast);
  return size(errors) == 1 &&
    { "synchronized should not be used in this assignment"
    } == messages(errors);
}

test bool aTaxisynchronizedBlock()
{
  <model,ast> = loadTestBoth("assignment14-synchronized-block");
  errors = allAssignmentTaxiRules(model,ast);
  return size(errors) == 1 &&
    { "synchronized should not be used in this assignment"
    } == messages(errors);
}