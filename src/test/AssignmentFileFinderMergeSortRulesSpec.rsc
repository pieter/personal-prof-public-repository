module \test::AssignmentFileFinderMergeSortRulesSpec

import Set;

import \test::SpecUtil;
import AssignmentFileFinderMergeSortRules;
import CodingRules;
import Util;

test bool aFileFinderMergeSortexampleSolution()
{
  <model,ast> = loadTestBoth("assignment13-finder-sort-example-solution");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aFileFinderMergeSortalternativeSolution()
{
  <model,ast> = loadTestBoth("assignment13-parallel-is-runnable");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aFileFinderMergeSortstartProjectCodingRules()
{
  <model,ast> = loadTestBoth("assignment13-finder-sort-start");
  errors = allCodingRules(model);
  return isEmpty(errors);
}

test bool aFileFinderMergeSortstartProjectAllRules()
{
  <model,ast> = loadTestBoth("assignment13-finder-sort-start");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return
    { "|java+class:///filefinder/FileFinder| should implement |java+interface:///java/lang/Runnable|"
    , "FileFinder should start new threads"
    , "There should be a class called ParallelMergeSort"
    } == messages(errors);
}

test bool aFileFinderMergeSortdoesntStartThread()
{
  <model,ast> = loadTestBoth("assignment13-doesnt-start-thread");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return
    { "FileFinder should start new threads"
    } == messages(errors);
}

test bool aFileFinderMergeSortffCallsJoin()
{
  <model,ast> = loadTestBoth("assignment13-ff-calls-join");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return
    { "FileFinder should not call Thread.join"
    } <= messages(errors);
}

test bool aFileFinderMergeSortParallelMergeSortNotParallel()
{
  <model,ast> = loadTestBoth("assignment13-parallel-sort-not-actually-parallel");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return
    { "ParallelMergeSort should start new threads"
    } == messages(errors);
}

test bool aFileFinderMergeSortStartThreadNoJoin()
{
  <model,ast> = loadTestBoth("assignment13-start-thread-no-join");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return
    { "ParallelMergeSort should call Thread.join"
    } == messages(errors);
}

test bool aFileFinderMergeSortSortTwoThreads()
{
  <model,ast> = loadTestBoth("assignment13-sort-two-threads");
  errors = allAssignmentFileFinderMergeSortRules(model,ast) + allCodingRules(model);
  return
    { "ParallelMergeSort should start only one new thread"
    } == messages(errors);
}