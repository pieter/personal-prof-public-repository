module \test::AssignmentSlidingGameRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentSlidingGameRules;
import CodingRules;
import Util;

test bool aSlidingGameexampleSolution()
{
  M3 model = loadTestProject("assignment06-sliding-game-example-solution");
  errors = allAssignmentSlidingGameRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aSlidingGamenotUsingInterfaces()
{
  M3 model = loadTestProject("assignment06-not-using-interfaces");
  errors = allAssignmentSlidingGameRules(model);
  // It looks like enhanced for loops cause the collection interface to be a dependency
  return
    { "Solver should use |java+interface:///java/util/Queue|"
    // , "Solver should use |java+interface:///java/util/Collection|"
    , "Solver should use |java+class:///java/util/HashSet|"
    , "Solver should use |java+class:///java/util/PriorityQueue|"
    }
    <= messages(errors);
}

test bool aSlidingGamenoHashCode()
{
  M3 model = loadTestProject("assignment06-no-hashCode");
  errors = allAssignmentSlidingGameRules(model);
  return
    { "SlidingGame should implement hashCode"
    }
    <= messages(errors);
}