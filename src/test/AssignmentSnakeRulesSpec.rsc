module \test::AssignmentSnakeRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentSnakeRules;
import CodingRules;
import Util;
import IO;

test bool aSnakeexampleSolution()
{
  M3 model = loadTestProject("assignment-snake-example-solution");
  errors = allAssignmentSnakeRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aSnakeGraphicsInInnerClass()
{
  M3 model = loadTestProject("assignment-snake-graphics-in-inner-class");
  errors = allAssignmentSnakeRules(model) + allCodingRules(model);
  for( error <- errors ) println(error);
  return isEmpty(errors);
}

test bool aSnakegraphicsNotInView()
{
  M3 model = loadTestProject("assignment-snake-graphics-not-in-view");
  errors = allAssignmentSnakeRules(model);
  return
    { "Rectangle: only SnakeGame should use shapes"
    , "Circle: only SnakeGame should use shapes"
    } == messages(errors);
}

test bool aSnakeInputInInnerClass()
{
  M3 model = loadTestProject("assignment-snake-input-in-inner-class");
  errors = allAssignmentSnakeRules(model) + allCodingRules(model);
  for( error <- errors ) println(error);
  return isEmpty(errors);
}

test bool aSnakeinputNotInController()
{
  M3 model = loadTestProject("assignment-snake-input-not-in-controller");
  errors = allAssignmentSnakeRules(model);
  return
    { "KeyEvent: only InputHandler should handle input events"
    , "MouseEvent: only InputHandler should handle input events"
    } == messages(errors);
}