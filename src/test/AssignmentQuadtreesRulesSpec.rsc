module \test::AssignmentQuadtreesRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentQuadtreesRules;
import CodingRules;
import Util;

test bool aQuadtreesExampleSolution()
{
  M3 model = loadTestProject("assignment07-quadtrees-example-solution");
  errors = allAssignmentQuadtreesRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aQuadtreesstartProject()
{
  M3 model = loadTestProject("assignment07-quadtrees-start");
  errors = allAssignmentQuadtreesRules(model);
  return
    { "There should be a class called WhiteLeaf"
    , "There should be a class called BlackLeaf"
    , "There should be a class called GreyNode"
    } <= messages(errors);
}

test bool aQuadtreesmainUsesNodesDirectly()
{
  M3 model = loadTestProject("assignment07-main-uses-nodes-directly");
  errors = allAssignmentQuadtreesRules(model);
  return
    { "Main should not use BlackLeaf directly"
    } <= messages(errors);
}