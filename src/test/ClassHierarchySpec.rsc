module \test::ClassHierarchySpec

import Set;
import lang::java::m3::Core;
import IO;
import Message;

import ClassHierarchy;
import \test::SpecUtil;
import Util;

test bool findInterfaceThatExists()
{
  M3 model = loadTestProject("assignment03-good");
  return findInterface(model, "Geometric") == |java+interface:///assignment3/Geometric|;
}

test bool findInterfaceThatDoesntExist()
{
  try
  {
    M3 model = loadTestProject("assignment03-good");
    loc blah = findInterface(model, "Geometricccc");
    // call to findInterface should throw an error
    return false;
  }
  catch error(message,_):
  {
    return message == "There should be an interface called Geometricccc";
  }
}


test bool findClassThatExists()
{
  M3 model = loadTestProject("assignment03-good");
  return findClass(model, "Circle") == |java+class:///assignment3/Circle|;
}

test bool findClassThatDoesntExist()
{
  try
  {
    M3 model = loadTestProject("assignment03-good");
    loc blah = findClass(model, "Triangle");
    // call to findInterface should throw an error
    return false;
  }
  catch error(message,_):
  {
    return message == "There should be a class called Triangle";
  }
}

test bool interfaceExtendsInterface()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  return isEmpty(extendsInterface(model, "IntermediateInterface", "MyInterface"));
}

test bool interfaceExtendsInterfaceUnknownInterface()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  errors = extendsInterface(model, "Foobar", "MyInterface");
  return
    { "There should be an interface called Foobar" }
    <= messages(errors);
}

test bool interfaceDoesntExtendInterface()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  errors = extendsInterface(model, "MyOtherInterface", "MyInterface");
  return
    { "MyOtherInterface should extend MyInterface" }
    <= messages(errors);
}

test bool classExtendsClass()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  return isEmpty(extendsClass(model, "B", "A"));
}

test bool unknownClass()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  errors = extendsClass(model, "Baa", "A");
  return
    { "There should be a class called Baa" }
    <= messages(errors);
}

test bool classDoesntExtendClass()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  errors = extendsClass(model, "C", "A");
  return
    { "C should extend A" }
    <= messages(errors);
}

test bool classImplementsInterface()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  return isEmpty(implementsInterface(model, "A", "IntermediateInterface"));
}

test bool classDoesntImplementInterface()
{
  M3 model = loadTestProject("ClassHierarchy-test-data/interface-extends-interface");
  errors = implementsInterface(model, "A", "MyInterface");
  return
    { "|java+class:///A| should implement |java+interface:///MyInterface|" }
    <= messages(errors);
}


test bool implementsInterfaceDirectly()
{
  M3 model = loadTestProject("ImplementsInterface-test-data/directly-implements-interface");
  return implementsAbstractMethod(model, |java+class:///A|, |java+method:///MyInterface/myAbstractFunction()|);
}

test bool implementsInterfaceIndirectly()
{
  M3 model = loadTestProject("ImplementsInterface-test-data/indirectly-implements-interface");
  return implementsAbstractMethod(model, |java+class:///B|, |java+method:///MyInterface/myAbstractFunction()|);
}

test bool doesntImplementInterface()
{
  M3 model = loadTestProject("ImplementsInterface-test-data/doesnt-implement-interface");
  return !implementsAbstractMethod(model, |java+class:///B|, |java+method:///MyInterface/myAbstractFunction()|);
}

// How to detect that the interface itself has an implementation?
// This is currently a false positive.
test bool defaultImplementationInInterface()
{
  return true;
  /*
  M3 model = loadTestProject("ImplementsInterface-test-data/default-implementation-in-interface");
  return implementsAbstractMethod(model, |java+class:///B|, |java+method:///MyInterface/myAbstractFunction()|);
  */
}

// The problem here is that methodOverrides states that IntermediateInterface overrides
// MyInterface/myAbstractFunction, even though it just declares it. In my opinion this
// is a bug. But: because this produces a compile error, I would argue that we don't need
// to generate an error message here.
test bool intermediateInterfaceDeclaresFunction()
{
  return true;
  /*
  M3 model = loadTestProject("ImplementsInterface-test-data/intermediate-interface-declares-function");
  return !implementsAbstractMethod(model, |java+class:///B|, |java+method:///MyInterface/myAbstractFunction()|);
  */
}

test bool intermediateInterfaceDefaultImplementsFunction()
{
  M3 model = loadTestProject("ImplementsInterface-test-data/intermediate-interface-default-implements-function");
  return implementsAbstractMethod(model, |java+class:///B|, |java+method:///MyInterface/myAbstractFunction()|);
}