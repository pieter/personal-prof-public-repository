module \test::AssignmentStudentRulesSpec

import Set;
import lang::java::m3::Core;

import \test::SpecUtil;
import AssignmentStudentRules;
import CodingRules;
import Util;

test bool aStudentgood()
{
  errors = allAssignmentStudentRules(loadTestProject("assignment01-good"));
  return isEmpty(errors);
}

test bool aStudentexampleSolution()
{
  M3 model = loadTestProject("assignment01-student-example-solution");
  errors = allAssignmentStudentRules(model) + allCodingRules(model);
  return isEmpty(errors);
}

test bool a01ioOnlyInMainClass()
{
  errors = aStudent_io(loadTestProject("assignment01-io-in-group"));
  return "Only the main or view class should contain I/O" in messages(errors);
}
