module \test::CodingRulesSpec

import List;
import Set;
import Message;

import \test::SpecUtil;
import CodingRules;
import Util;

test bool projectWithProperCapitalizedClassNames()
{
  return isEmpty(ps_class_names_uppercase(loadTestProject("assignment01-good")));
}

test bool projectWithLowerCaseClassNames()
{
  classes = {c.file | error(_, c) <- ps_class_names_uppercase(loadTestProject("assignment01-lowercase-class-names"))};
  return classes == {"student", "main"};
}

test bool projectWithProperCapitalizedMethodAndAttributeNames()
{
  identifiers = ps_attribute_names_lowercase(loadTestProject("assignment01-good"));
  return isEmpty(identifiers);
}

test bool projectWithUpperCaseNames()
{
  identifiers = {i.file | error(_, i) <- ps_attribute_names_lowercase(loadTestProject("assignment01-uppercase-method-names"))};
  return identifiers ==
    { "Scan" // local variable
    , "AddMember(Student)" // method
    , "Currentindex" // field
    , "New_firstname" // parameter
    };
}

test bool projectWithAllCapsConstant()
{
  return isEmpty(ps_all_caps_constant_names(loadTestProject("assignment01-good")));
}

test bool projectWithImproperConstantName()
{
  constants = {c.file | error(_, c) <- ps_all_caps_constant_names(loadTestProject("assignment01-wrong-constant-name"))};
  return constants == {"FOO_bar_BAZ"};
}

test bool explicitAccessModifiersGood()
{
  return isEmpty(ps_explicit_access_modifiers(loadTestProject("assignment01-good")));
}

test bool explicitAccessModifiersMissing()
{
  errors = ps_explicit_access_modifiers(loadTestProject("assignment01-missing-access-modifier"));
  return
    size(errors) == 2 &&
    { "Explicitly specify access modifiers: public, protected, or private" } <=
    messages(errors);
}

test bool enumInterfaceAccessModifiersGood()
{
  return isEmpty(ps_explicit_access_modifiers(loadTestProject("implicit-enum-interface-access-modifiers")));
}

test bool enumAccessModifiersExplicit()
{
  errors = ps_no_enum_modifier(loadTestProject("explicit-enum-interface-access-modifiers"));
  return { "Private access modifiers for enum constructors are redundant" } == messages(errors);
}

test bool interfaceAccessModifiersExplicit()
{
  errors = ps_no_interface_modifier(loadTestProject("explicit-enum-interface-access-modifiers"));
  return { "Public access modifiers for interface methods are redundant" } == messages(errors);
}

test bool nonFinalPublicAttribute()
{
  errors = ps_public_attributes(loadTestProject("assignment01-non-final-public"));
  return
    { "Only final attributes may be public" } <=
    messages(errors);
}

test bool allCapsAttributeNames()
{
  errors = allCodingRules(loadTestProject("assignment01-all-caps-attribute-name"));
  return
    size(errors) == 3 &&
    { "Only constants (static final) should be ALL_CAPS"
    , "Attributes, methods, and parameter names should start with a lowercase letter"
    } <= messages(errors);
}