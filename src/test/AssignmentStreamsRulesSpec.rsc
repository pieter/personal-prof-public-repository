module \test::AssignmentStreamsRulesSpec

import Set;
import lang::java::m3::Core;
import lang::java::m3::AST;

import \test::SpecUtil;
import AssignmentStreamsRules;
import CodingRules;
import Util;

test bool aStreamsexampleSolution()
{
  str project = "assignment12-streams-example-solution";
  M3 model = loadTestProject(project);
  set[Declaration] ast = loadTestAST(project);
  errors = allAssignmentStreamsRules(model, ast) + allCodingRules(model);
  return isEmpty(errors);
}

test bool aStreamsstartCodingRules()
{
  str project = "assignment12-streams-start";
  M3 model = loadTestProject(project);
  errors = allCodingRules(model);
  return isEmpty(errors);
}

test bool aStreamsstartAll()
{
  str project = "assignment12-streams-start";
  M3 model = loadTestProject(project);
  set[Declaration] ast = loadTestAST(project);
  errors = allAssignmentStreamsRules(model, ast) + allCodingRules(model);
  return
    { "countEvenNumbers should use |java+interface:///java/util/stream/Stream|"
    , "sumOddNumbers should use |java+interface:///java/util/stream/Stream|"
    , "multiplyNumbers should use |java+interface:///java/util/stream/Stream|"
    , "calculateTax should use |java+interface:///java/util/stream/Stream|"
    , "sumStringIntegers should use |java+interface:///java/util/stream/Stream|"
    , "streamOfStreams should use |java+interface:///java/util/stream/Stream|"
    , "everySecondElement should use |java+interface:///java/util/stream/IntStream|"
    , "philosophers should use |java+interface:///java/util/stream/Stream|"
    } == messages(errors);
}

test bool aStreamsforeachLoop()
{
  str project = "assignment12-foreach-loop";
  M3 model = loadTestProject(project);
  set[Declaration] ast = loadTestAST(project);
  errors = allAssignmentStreamsRules(model, ast);
  return size(errors) == 4 &&
    { "No loops are allowed in this assignment"
    , "sumStringIntegers should use |java+interface:///java/util/stream/Stream|"
    } == messages(errors);
}