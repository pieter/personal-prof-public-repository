module AssignmentExpressionsRules

import Message;
import lang::java::m3::Core;
import lang::java::m3::AST;

import ClassHierarchy;
import Util;

set[Message] allAssignmentExpressionsRules(M3 model)
  = aExpressions_base_classes_exist(model)
  + aExpressions_concrete_classes_exist(model)
  ;

set[Message] aExpressions_base_classes_exist(M3 model)
{
  set[Message] result = {};
  try
  {
    // Just make sure it exists and is an interface. Throws an exception if it doesn't.
    result += assertExists(findInterface, model, "Expression");

    set[loc] baseClasses =
      { findClass(model, "NoArgExpr")
      , findClass(model, "OneArgExpr")
      , findClass(model, "TwoArgExpr")
      };
    result +=
      { error("Class <getName(model,c)> should be abstract.", c)
      | c <- baseClasses
      , ! (abstract() in model.modifiers[c])
      };
  }
  catch e:error(_,_):
  {
    return { e };
  }

  return result;
}

set[Message] aExpressions_concrete_classes_exist(M3 model)
  = extendsClass(model, "Constant", "NoArgExpr")
  + extendsClass(model, "Variable", "NoArgExpr")
  + extendsClass(model, "Add", "TwoArgExpr")
  + extendsClass(model, "Multiply", "TwoArgExpr")
  + extendsClass(model, "Negate", "OneArgExpr")
  ;
