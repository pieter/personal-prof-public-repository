 
module CodingRules

import Relation;
import lang::java::m3::Core;
import lang::java::m3::AST;
import Message;
import Set;
import List;

import Util;

// Runs all coding rule checkers on the given model
set[Message] allCodingRules(M3 model)
  = ps_class_names_uppercase(model)
  + ps_attribute_names_lowercase(model)
  + ps_all_caps_constant_names(model)
  + ps_explicit_access_modifiers(model)
  + ps_no_enum_modifier(model)
  + ps_no_interface_modifier(model)
  + ps_public_attributes(model)
  // Redundant for Main, but useful for the tests
  + modelErrors(model)
  ;

// Given an M3 model, returns all classes whose name does not start with an uppercase letter
set[Message] ps_class_names_uppercase(M3 model) =
  { error("Class name should start with an uppercase letter", c)
  | c <- classes(model)
  , ! (/^[A-Z]/ := getName(model,c))
  };

// Given an M3 model, return all attributes, methods, and parameters whose names do not
// start with a lowercase letter
set[Message] ps_attribute_names_lowercase(M3 model)
{
  set[loc] constants = { e | <e, _> <- model.declarations, isField(e), {\static(), \final()} <= model.modifiers[e] };
  set[Message] result = {};
  for( identifier <- methods(model) + parameters(model) + variables(model) + ( fields(model) - constants ) )
  {
    if(/^[A-Z_]+$/ := getName(model,identifier))
    {
      result += error("Only constants (static final) should be ALL_CAPS", identifier);
    }
    else if(! isConstructor(identifier) && ! (/^[a-z_]/ := getName(model,identifier)))
    {
      result += error("Attributes, methods, and parameter names should start with a lowercase letter", identifier);
    }
  }
  return result;
}

// Given a model, returns all constants that are not all caps
set[Message] ps_all_caps_constant_names(M3 model)
{
  set[loc] constants = range(declaredFields(model, checkModifiers={\static(),\final()}));
  return
    { error("Names of constants (static final) should be ALL_CAPS", constant)
    | constant <- constants
    , ! (/^[A-Z_]+$/ := getName(model,constant))
    };
}


set[Message] ps_explicit_access_modifiers(M3 model)
{
  set[loc] interfaceMethods = { method | interface <- interfaces(model), method <- methods(model, interface) };
  set[loc] enumConstructors = { constructor | enum <- enums(model), constructor <- constructors(model, enum) };
  set[loc] artifactsOfInterest = range(declaredMethods(model)) + range(declaredFields(model)) - interfaceMethods - enumConstructors;
  return
    { error("Explicitly specify access modifiers: public, protected, or private", thing)
    | thing <- artifactsOfInterest
      // & is set intersection
    , isEmpty(model.modifiers[thing] & {\public(), \protected(), \private()})
    };
}

set[Message] ps_no_enum_modifier(M3 model)
{
  set[loc] artifactsOfInterest = { constructor | enum <- enums(model), constructor <- constructors(model, enum) };
  return
    { error("Private access modifiers for enum constructors are redundant", thing)
    | thing <- artifactsOfInterest
    , \private() in model.modifiers[thing]
    };
}

set[Message] ps_no_interface_modifier(M3 model)
{
  set[loc] artifactsOfInterest = { method | interface <- interfaces(model), method <- methods(model, interface) };
  return
    { error("Public access modifiers for interface methods are redundant", thing)
    | thing <- artifactsOfInterest
    , \public() in model.modifiers[thing]
    };
}

// If an attribute is public, it must be final
set[Message] ps_public_attributes(M3 model) =
  { error("Only final attributes may be public", field)
  | field <- range(declaredFields(model))
  , \public() in model.modifiers[field] && !(\final() in model.modifiers[field])
  };
