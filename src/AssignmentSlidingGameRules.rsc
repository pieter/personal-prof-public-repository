module AssignmentSlidingGameRules

import Message;
import lang::java::m3::Core;

import ClassHierarchy;
import StandardLocations;

set[Message] allAssignmentSlidingGameRules(M3 model)
  = aSlidingGame_solver_uses_collection(model)
  + aSlidingGame_implement_hashCode(model)
  ;

set[Message] aSlidingGame_solver_uses_collection(M3 model)
{
  return {}
    + classDependsOn(model, "Solver", javaUtilCollection)
    + classDependsOn(model, "Solver", javaUtilQueue)
    + classDependsOn(model, "Solver", javaUtilHashSet)
    + classDependsOn(model, "Solver", javaUtilPriorityQueue)
    ;
}

set[Message] aSlidingGame_implement_hashCode(M3 model)
{
  set[Message] result = {};
  try
  {
    loc slidingGame = findClass(model, "SlidingGame");

    if( !(javaLangHashCode in model.methodOverrides[methods(model, slidingGame)]) )
    {
      return { error("SlidingGame should implement hashCode", slidingGame) };
    }
  }
  catch e:error(_,_):
  {
    return { e };
  }
  return result;
}