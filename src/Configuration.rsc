module Configuration

private list[str] externalJars =
  [ "junit.jar"
  , "javafx.base.jar"
  , "javafx.controls.jar"
  , "javafx.fxml.jar"
  , "javafx.graphics.jar"
  , "javafx.media.jar"
  , "javafx.swing.jar"
  , "javafx.web.jar"
  ];
  
public list[loc] defaultClassPath =
  [ |cwd:///lib/| + jarFile
  | jarFile <- externalJars
  ];

// For running tests within eclipse, you have to start eclipse with the src/
// subdirectory as CWD
public loc testDataDir = |cwd:///test-data|;

// The java parser supports old versions of java. We want it to parse a newer one.
public str defaultJavaVersion = "11";