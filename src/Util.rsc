module Util

import lang::java::m3::Core;
import lang::java::m3::AST;
import Relation;
import Set;
import Message;

import StandardLocations;

// Filter all messages that start with "syntax error"
set[Message] syntaxErrors(M3 model) =
  { e
  | e:error(msg, _) <- model.messages
  , /^syntax error/i := msg
  };

// All error messages that occurred during loading of the model
set[Message] modelErrors(M3 model) =
  { e
  | e:error(_, _) <- model.messages
  };

// Assumption: the given identifier has exactly one name.
str getName(M3 model, loc identifier)
{
  set[str] candidateNames = invert(model.names)[identifier];
  if(size(candidateNames) < 1)
  {
    return "<identifier>";
  }
  return getOneFrom(candidateNames);
}

// A main method is public static and called main. We don't check for arguments.
// Checking argument types is complicated, and probably this catches all cases anyway.
set[loc] getMainMethods(M3 model)
{
  publicStatics = domain(invert(declaredMethods(model, checkModifiers = {\public(), \static()})));
  mains = { func | func <- publicStatics, getName(model, func) == "main" };
  return mains;
}

// Assumes that there is exactly one Main class in the project
// Throws an error if there is no Main class
loc getMainClass(M3 model)
{
  mainClasses = range(domainR(invert(model.containment), getMainMethods(model)));
  if(size(mainClasses) < 1)
  {
    throw error("There should be a Main class", |file:///|);
  }
  return getOneFrom(mainClasses);
}

// extract only the messages of a set of errors, discarding the locations
set[str] messages(set[Message] errors) = { message | error(message, _) <- errors };

// All classes that perform I/O
// Performing I/O is defined as using PrintStream, which is System.out
set[loc] ioClasses(M3 model)
{
    // find all methods that use System
    set[loc] ioMethods =
      { m
      | m <- invert(model.typeDependency)[javaIoPrintStream]
      , isMethod(m)
      };
    // find the classes to which these methods belong
    return range(domainR(invert(model.containment), ioMethods));
}