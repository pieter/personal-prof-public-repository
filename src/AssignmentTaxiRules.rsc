module AssignmentTaxiRules

import Message;
import Relation;
import lang::java::m3::Core;
import lang::java::m3::AST;

import ClassHierarchy;
import StandardLocations;

set[Message] allAssignmentTaxiRules(M3 model, set[Declaration] ast)
  = aTaxi_use_runnable(model)
  + aTaxi_simulation_exists(model)
  + aTaxi_synchronization_exists(model)
  + aTaxi_synchronization_only_in_station(model)
  + aTaxi_no_synchronized_modifier(model)
  + aTaxi_no_synchronized_block(model,ast)
  ;

// Taxi and Train must implement Runnable
set[Message] aTaxi_use_runnable(M3 model)
  = implementsInterfaceGeneric
      ( model
      , loc (M3 model){return findClass(model, "Taxi");}
      , loc (M3 _){return javaLangRunnable;}
      )
  + implementsInterfaceGeneric
      ( model
      , loc (M3 model){return findClass(model, "Train");}
      , loc (M3 _){return javaLangRunnable;}
      )
  ;

set[Message] aTaxi_simulation_exists(M3 model)
{
  try
  {
    return
        classDependsOn(model, "Simulation", javaUtilExecutorService)
      + classDependsOn(model, "Simulation", findClass(model, "Taxi"))
      + classDependsOn(model, "Simulation", findClass(model, "Train"))
      ;
  }
  catch e:error(_,_):
  {
    return { e };
  }
}

set[Message] aTaxi_synchronization_exists(M3 model)
  = classDependsOn(model, "Station", javaUtilLock)
  + classDependsOn(model, "Station", javaUtilCondition)
  ;

set[Message] aTaxi_synchronization_only_in_station(M3 model)
{
  try
  {
    // All classes except Station that use Lock or Condition
    set[loc] synchronizationClasses =
      { c
      | c <- classes(model)
      ,    javaUtilLock      in allTypeDependenciesLoc(model, c)
        || javaUtilCondition in allTypeDependenciesLoc(model, c)
      };
    synchronizationClasses -= findClass(model, "Station");
    return
      { error("Only Station should have synchronization", c)
      | c <- synchronizationClasses
      };
  }
  catch e:error(_,_):
  {
    return { e };
  }
}

// No methods with the synchronized modifier
set[Message] aTaxi_no_synchronized_modifier(M3 model) =
  { error("synchronized should not be used in this assignment", synchronizedThing)
  | synchronizedThing <- invert(model.modifiers)[synchronized()]
  };

set[Message] aTaxi_no_synchronized_block(M3 _, set[Declaration] ast) =
  { error("synchronized should not be used in this assignment", m.decl)
  | /m:method(_,_,_,_,/synchronizedStatement(_,_)) <- ast
  };